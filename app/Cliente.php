<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Cliente extends Scope
{
  use Notifiable;
  protected $table = 'sog_clientes';
  protected $fillable = ['cliente', 'cnpj','cpf','telefone','ie_rg','email',
                        'razao','endereco_id','celular','empresa_id','status'];
  protected $primaryKey = 'id_cliente';

  public function endereco(){
    return $this->belongsTo('Soged\Endereco', 'endereco_id');
  }
}
