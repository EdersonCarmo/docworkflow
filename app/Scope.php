<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;

class Scope extends Model
{
  public function scopeActive($query){
    return $query->where('status', '>',0);
  }
  public function scopeEmpId($query){
    return $query->where('empresa_id', \Auth::user()->empresa_id);
  }
  public function scopeEmpAct($query){
    return $query->where([['empresa_id', \Auth::user()->empresa_id],['status', '>',0]]);
  }
  public function scopeUserAct($query){ 
    return $query->where([['user_id', \Auth::id()],['status', '>',0]]);
  }

}
