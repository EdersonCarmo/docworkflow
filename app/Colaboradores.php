<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Colaboradores extends Model
{
  use Notifiable;
  protected $connection = 'mysql2';
  protected $table = 'sog_grupos';
  protected $fillable = ['nome_grupo','empresa_id'];
  protected $primaryKey = 'id_grupo';
}
