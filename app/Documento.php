<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Documento extends Scope
{
  use Notifiable;
  protected $table = 'sog_documentos';
  protected $fillable = ['documento','texto','status','template_id','cliente_id','pasta_id'];
  protected $primaryKey = 'id_documento';

  public function fluxo(){
    return $this->hasMany('Soged\Fluxo', 'documento_id');
  }
}
