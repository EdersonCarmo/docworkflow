<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Usuario extends Scope
{
  use Notifiable;
  protected $table = 'users';
  protected $fillable = [
    'empresa_id','name', 'email', 'password','foto'
    ,'telefone', 'endereco_id', 'celular','grupo_id'
  ];
  protected $primaryKey = 'id';

  public function grupo(){
    return $this->belongsTo('Soged\Grupo', 'grupo_id');
  }

  public $rules = [
    'name'        => 'required|min:3|max:100',
    'empresa_id'  => 'required|max:11|numeric',
    'grupo_id'    => 'required|max:11|numeric',
    'endereco_id' => 'max:11|numeric',
    'telefone'    => 'max:15|numeric',
    'celular'     => 'max:15|numeric',
    'email'       => 'required|email|min:5|max:255|unique:users'
  ];

  public $messages = [
    'name.min'    => 'deve ter no mínimo 3 caracteres.',
    'name.max'    => 'nome deve ter no máximo 100 caracteres.',
  ];
}
