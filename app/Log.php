<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Log extends Scope
{
  use Notifiable;
  protected $table = 'sog_logs';
  protected $fillable = ['id_empresa','id_user','documento_id','pasta_id',
            'template_id', 'desc', 'grupo_id','permissao_id', 'endereco_id',
            'cliente_id', 'arquivo_id', 'empresa_id', 'user_id'];
  protected $primaryKey = 'id_log';
}
