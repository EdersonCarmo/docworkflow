<?php

function rteSafe($strText) {
  //returns safe code for preloading in the RTE
  $tmpString = $strText;
  //convert all types of single quotes
  $tmpString = str_replace(chr(145), chr(39), $tmpString);
  $tmpString = str_replace(chr(146), chr(39), $tmpString);
  $tmpString = str_replace("'", "'", $tmpString);
  //convert all types of double quotes
  $tmpString = str_replace(chr(147), chr(34), $tmpString);
  $tmpString = str_replace(chr(148), chr(34), $tmpString);
  //replace carriage returns & line feeds
  $tmpString = str_replace(chr(10), " ", $tmpString);
  $tmpString = str_replace(chr(13), " ", $tmpString);
  return $tmpString;
}

if(! function_exists('ReplaceStr')){
  function ReplaceStr($str, $a, $b = 0){
    if(isset($str['tem']) and $str['tem'] == 1)
      return rteSafe($str['texto']);

    if(!isset($b->id_cliente)){
      $ac = array(
        '@nome_a','@cnpj_a','@razao_a','@ie_a','@uf_a','@cidade_a',
        '@bairro_a','@rua_a','@numero_a','@cep_a','@comp_a'
      );

      $dc = array(
        $a->empresa, $a->cnpj, $a->razao, $a->ie, $a->endereco->uf,
        $a->endereco->cidade, $a->endereco->bairro, $a->endereco->logradouro,
        $a->endereco->numero, $a->endereco->cep, $a->endereco->complemento
      );
    }else {
      $ac = array(
        '@nome_a','@cnpj_a','@razao_a','@ie_a','@uf_a','@cidade_a',
        '@bairro_a','@rua_a','@numero_a','@cep_a','@comp_a',
        '@nome_b','@cnpj_b','@razao_b','@ie_b','@uf_b','@cidade_b',
        '@bairro_b','@rua_b','@numero_b','@cep_b','@comp_b'
      );
      $dc = array(
        $a->nome, $a->cnpj, $a->razao, $a->ie, $a->endereco->uf,
        $a->endereco->cidade, $a->endereco->bairro, $a->endereco->logradouro,
        $a->endereco->numero, $a->endereco->cep, $a->endereco->complemento,
        $b->nome, $b->cnpj, $b->razao, $b->ie, $b->endereco->uf,
        $b->endereco->cidade, $b->endereco->bairro, $b->endereco->logradouro,
        $b->endereco->numero, $b->endereco->cep, $b->endereco->complemento
      );
    }

    return str_replace($ac, $dc, rteSafe($str['texto']));
  }
}

if(! function_exists('ReplaceDoc')){
  function ReplaceDoc($str, $a, $b=0){
    if($b == 0){
      $ac = array('@nome','@email','');
      $dc = array($a->name, $a->email);
    }else {
      $ac = array(
        '@nome_a','@email_a','@CNPJ_a','@razao_a','@IE_a','@UF_a',
        '@cidade_a','@bairro_a','@rua_a','@numero_a','@CEP_a','@COMP_a',

        '@nome_b','@email_b','@CNPJ_b','@razao_b','@IE_b','@UF_b',
        '@cidade_b','@bairro_b','@rua_b','@numero_b','@CEP_b','@COMP_b'
      );
      $dc = array(
        $a->name, $a->email, $a->cnpj, $a->razao, $a->ie, $a->endereco->uf,
        $a->endereco->cidade, $a->endereco->bairro, $a->endereco->logradouro,
        $a->endereco->numero, $a->endereco->cep, $a->endereco->complemento,

        $b->nome, $b->email, $b->cnpj, $b->razao, $b->ie, $b->endereco->uf,
        $b->endereco->cidade, $b->endereco->bairro, $b->endereco->logradouro,
        $b->endereco->numero, $b->endereco->cep, $b->endereco->complemento
      );
    }

    return str_replace($ac, $dc, rteSafe($str));
  }
}

if(! function_exists('ReplaceTem')){
  function ReplaceTem($str, $u){
    $ac = array('@nome','@email');
    $dc = array($u->name, $u->email);

    if(isset($str->template_id) > 0) return rteSafe($str->texto);

    return str_replace($ac, $dc, rteSafe($str['texto']));
  }
}
