<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Fluxo extends Scope
{
  use Notifiable;
  protected $table = 'sog_fluxos';
  protected $fillable = ['fluxo', 'ordem','user_id','tipo','documento_id','email'];
  protected $primaryKey = 'id_fluxo';

  public function usuario(){
    return $this->belongsTo('Soged\Usuario', 'user_id');
  }

  public function documento(){
    return $this->belongsTo('Soged\Documento', 'documento_id');
  }
}
