<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Alert extends Scope
{
  use Notifiable;
  protected $table = 'sog_alerts';
  protected $fillable = ['alerta', 'desc','user_id','status','fluxo_id','tipo'];
  protected $primaryKey = 'id_alert';
}
