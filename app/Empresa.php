<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Empresa extends Scope
{
	use Notifiable;
	protected $table = 'sog_empresas';
	protected $fillable = ['empresa', 'cnpj','telefone','ie','razao','endereco_id','celular','cod_vip'];
	// ['empresa','razao','cnpj','ie','telefone','celular','cep','logradouro','numero','complemento','bairro','cidade','uf'];
	protected $primaryKey = 'id_empresa';

	public function endereco(){
		return $this->belongsTo('Soged\Endereco', 'endereco_id');
	}
}
