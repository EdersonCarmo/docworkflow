<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Template extends Scope
{
  use Notifiable;
  protected $table = 'sog_templates';
  protected $fillable = ['template','status','empresa_id'];
  protected $primaryKey = 'id_template';
}
