<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Permissao extends Model
{
  use Notifiable;

  protected $table = 'sog_permissao';
  protected $fillable = ['crud', 'updown', 'user_id','pasta_id',
  'template_id','documento_id','grupo_id','arquivo_id','ref_pai','sts'];
  protected $primaryKey = 'id_permissao';


  public function pasta(){
    return $this->belongsTo('Soged\Pasta', 'pasta_id');
  }
  public function arquivo(){
    return $this->belongsTo('Soged\Arquivo', 'arquivo_id');
  }
  public function usuario(){
    return $this->belongsTo('Soged\Usuario', 'user_id');
  }
}
