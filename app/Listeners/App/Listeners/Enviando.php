<?php

namespace Soged\Listeners\App\Listeners;

use Soged\Events\App\Events\TesteEnvio;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Enviando
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TesteEnvio  $event
     * @return void
     */
    public function handle(TesteEnvio $event)
    {
        //
    }
}
