<?php

namespace Soged\Listeners\App\Listeners;

use Soged\Events\App\Events\EnviarEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EnviarEmail  $event
     * @return void
     */
    public function handle(EnviarEmail $event)
    {
        //
    }
}
