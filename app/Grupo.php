<?php namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Grupo extends Scope
{
  use Notifiable;
  protected $table = 'sog_grupos';
  protected $fillable = ['grupo','empresa_id'];
  protected $primaryKey = 'id_grupo';
}
