<?php

namespace Soged\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUsuario extends FormRequest
{
  public function authorize()
  {
    return true;
  }
  public function rules()
  {
    return [
      'name'        => 'required|min:3|max:100',
      'telefone'    => 'max:15|numeric',
      'celular'     => 'max:15|numeric',
      'email'       => 'required|email|min:5|max:255|unique:users'
    ];
  }
}
