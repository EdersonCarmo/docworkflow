<?php

namespace Soged\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCliente extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'cliente'     => 'required|min:3|max:190',
      'razao'       => 'required|min:3|max:190',
      'cnpj'        => 'cnpj',
      //'telefone'    => 'telefone_com_ddd',
      'ie_rg'       => 'required',
      //'celular'     => 'celular_com_ddd',
      'cpf'         => 'cpf',

      'logradouro'  => 'required|min:3|max:190',
      'bairro'      => 'required|min:3|max:190',
      'cidade'      => 'required|min:3|max:190',
      'uf'          => 'required',
      'numero'      => 'required',
      'cep'         => 'required|numeric',
    ];
  }
}
