<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Events\EnviarEmail;
use Soged\Alert;
use Soged\SessionController;
use Soged\User;

class HomeController extends Controller
{
  public function __construct(Request $request){
    $this->middleware('auth');
  }

  public function index(Request $request){
    return view('home');
  }

  public function teste(){
    EnviarEmail::listen(new EnviarEmail());
  }

  public function usuario(){
    return view('listUsers');
  }

  public function perfil_emp(){
  }

  public function perfil_usu(){
  }
}
