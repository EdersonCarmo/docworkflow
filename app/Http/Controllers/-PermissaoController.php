<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Permissao;
use Soged\Usuario;
use Soged\Grupo;
use Soged\Pasta;
use Soged\Arquivo;
use Soged\Documento;

class PermissaoController extends Controller
{
  // RETORNA VALOR DO CRUD
  public function crud($crud){ $soma = 0;
    if(isset($crud['crud'])) {
      if($crud['crud'] < 100) return $crud['crud'];
      elseif($crud['crud'] < 200) return $crud['crud'] - 100;
      elseif($crud['crud'] < 300) return $crud['crud'] - 200;
      elseif($crud['crud'] < 400) return $crud['crud'] - 300;
    }

    if(isset($crud['c'])) $soma = $soma + $crud['c'];
    if(isset($crud['r'])) $soma = $soma + $crud['r'];
    if(isset($crud['u'])) $soma = $soma + $crud['u'];
    if(isset($crud['d'])) $soma = $soma + $crud['d'];
    return $soma;
  }
  // RETORNA VALOR DO UPDN
  public function updn($updn){ $soma = 0;
    if(isset($crud['crud'])) {
      if($crud['crud'] < 100) return 0;
      elseif($crud['crud'] < 200) return 1;
      elseif($crud['crud'] < 300) return 2;
      elseif($crud['crud'] < 400) return 3;
    }

    if(isset($updn['dn'])) $soma = $soma + $updn['dn'];
    if(isset($updn['up'])) $soma = $soma + $updn['up'];
    if(isset($updn['p']))  $soma = $soma + $updn['p'];
    return $soma;
  }
  // RETORNA USUÁRIOS SELECIONADOS
  public function usuariosP($dados){
    $data   = (object) $dados;
    $idUser = array();

    if(!isset($data->grupo_id) AND !isset($data->user_id))
    return back()->withInput()->with(['danger' => 'Selecione um usuário ou grupo.']);

    if(isset($data->grupo_id))
    foreach ($data->grupo_id as $key){
      $idG  = Usuario::select('id')
      ->where('grupo_id','=', $key, 'or')
      ->get();

      foreach ($idG as $item)
      array_push($idUser, $item->id);
    }

    if(isset($data->user_id))
    foreach ($data->user_id as $item)
    array_push($idUser, $item);
    return $idUser;
  }
  // RETORNA NOME DA PASTA RAIZ
  public function pastaRaiz($id){
    $nm     = \Auth::user()->empresa_id.'p'.$id;

    if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0)
    return false;

    $idP    = Pasta::select('id_pasta')->where('pasta',$nm)->get();
    return $idP[0]->id_pasta;
  }
  // CRIA PERMISSÃO PARA ARQUIVO
  public function permiteArq($dados){
    $data   = (object) $dados;
    $idUser = $this->usuariosP($dados);

    foreach ($idUser as $key) {
      $del  = Permissao::where('user_id', $key)
      ->where('arquivo_id', $data->arquivo_id)
      ->delete();

      $nm     = \Auth::user()->empresa_id.'p'.$key;
      if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0)
      return redirect()->route('pasta.index')->with(['danger' => 'Usuário não possui pasta.']);

      $idP    = Pasta::select('id_pasta')->where('pasta',$nm)->get();

      $idPer  = Permissao::create([
        'arquivo_id'=> $data->arquivo_id,
        'pasta_id'  => $idP[0]->id_pasta,
        'crud'      => $this->crud($dados),
        'updown'    => $this->updn($dados),
        'user_id'   => $key,
        'sts'       => 2
        ])->id_permissao;
        $permissao  = Permissao::find($idPer);
    }
  }
  // CRIA PERMISSÃO PARA DOCUMENTO
  public function permiteDoc($dados){
    $data   = (object) $dados;
    $idUser = $this->usuariosP($dados);

    foreach ($idUser as $key) {
      $del  = Permissao::where('user_id', $key)
      ->where('documento_id', $data->documento_id)
      ->delete();

      $nm     = \Auth::user()->empresa_id.'p'.$key;
      if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0)
      return redirect()->route('pasta.index')->with(['danger' => 'Usuário não possui pasta.']);

      $idP    = Pasta::select('id_pasta')->where('pasta',$nm)->get();

      $idPer  = Permissao::create([
        'documento_id'=> $data->documento_id,
        'pasta_id'  => $idP[0]->id_pasta,
        'crud'      => $this->crud($dados),
        'updown'    => $this->updn($dados),
        'user_id'   => $key,
        'sts'       => 2
        ])->id_permissao;
        $permissao  = Permissao::find($idPer);
    }
  }

  // ===================================================================
  public $pastaB = array();
  public $arquiB = array();
  public $documB = array();

  // BUSCA PASTAS E ARQUIVO EM ARVORE
  public function buscaPasta($id){
    if(count($this->pastaB) == 0){ // ADD id RAIZ
      array_push($this->pastaB, ['id' => $id, 'ref_pai' => null]);
      // BUSCA ARQUIVOS NA PASTA RAIZ
      $a = Arquivo::Active()->where('pasta_id',$id)->get();
      if(count($a) > 0)
      foreach ($a as $item)
      array_push($this->arquiB, ['id' => $item->id_arquivo, 'pasta_id' => $item->pasta_id]);
      // BUSCA ARQUIVOS NA PASTA RAIZ
      // BUSCA DOCUMENTOS NA PASTA RAIZ
      $d = Documento::select()
      ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
      ->where('P.user_id', \Auth::id())
      ->where('P.pasta_id',$id)
      ->get();

      if(count($d) > 0)
      foreach ($d as $item)
      array_push($this->documB, ['id' => $item->id_documento, 'pasta_id' => $item->pasta_id]);
      // BUSCA DOCUMENTOS NA PASTA RAIZ
    }
    // BUSCA SUBPASTAS
    $p = Pasta::Active()->where('ref_pai',$id)->get();
    foreach ($p as $key) {
      array_push($this->pastaB, ['id' => $key->id_pasta, 'ref_pai' => $key->ref_pai]);
      // BUSCA SUBPASTAS
      // BUSCA ARQUIVOS SUBPASTA
      $a = Arquivo::Active()->where('pasta_id',$key->id_pasta)->get();
      if(count($a) > 0)
      foreach ($a as $item)
      array_push($this->arquiB, ['id' => $item->id_arquivo, 'pasta_id' => $item->pasta_id]);
      // BUSCA ARQUIVOS SUBPASTA
      // BUSCA DOCUMENTOS SUBPASTA
      $d = Documento::select()
      ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
      ->where('P.user_id', \Auth::id())
      ->where('pasta_id',$key->id_pasta)
      ->get();

      if(count($d) > 0)
      foreach ($d as $item)
      array_push($this->documB, ['id' => $item->id_documento, 'pasta_id' => $item->pasta_id]);
      // BUSCA DOCUMENTOS SUBPASTA

      $this->buscaPasta($key->id_pasta);
    }
  }
  // BUSCA PASTAS E ARQUIVO EM ARVORE
  // APAGA PERMISOES PASTAS E ARQUIVOS
  public function apagaPermissao($dados){
    $sts = 1; $volta = 0;
    if(isset($dados['user_id'])){
      $data   = (object) $dados;
      $idUser = $this->usuariosP($dados);
      $this->buscaPasta($data->pasta_id);
    }
    else {
      $volta = 1;
      $id     = Permissao::where('pasta_id', $dados)
      ->where('arquivo_id', NULL)
      ->where('ref_pai','!=', NULL, 'and')
      ->where('user_id', \Auth::id(), 'and')
      ->get();

      $idUser = array($id[0]->user_id);

      if($id[0]->sts == 1){
        $id     = Permissao::where('pasta_id', $dados)
        ->where('arquivo_id', NULL)
        ->where('ref_pai','!=', NULL, 'and')
        ->get();

        $sts = 0;
        $idUser = array();
        foreach ($id as $key)
        array_push($idUser, $key->user_id);
      }

      $this->buscaPasta($dados);
    }


    foreach ($idUser as $keyU) {
      // DEL PERMISSAO PASTA
      foreach ($this->pastaB as $keyP) {
        $del = Permissao::where('user_id', $keyU)
        ->where('pasta_id', $keyP['id'])
        ->where('ref_pai','!=', NULL, 'and')
        ->where('sts','>',$sts)
        ->delete();
      }
      // DEL PERMISSAO ARQUIVO
      foreach ($this->arquiB as $keyA) {
        $del = Permissao::where('user_id', $keyU)
        ->where('arquivo_id', $keyA['id'])
        ->where('sts','>',$sts)
        ->delete();
      }
      // DEL PERMISSAO DOCUMENTO
      foreach ($this->documB as $keyD) {
        $del = Permissao::where('user_id', $keyU)
        ->where('documento_id', $keyD['id'])
        ->where('sts','>',$sts)
        ->delete();
      }
    }

    // if($volta == 1) return redirect()->route('pasta.index');
  }
  // APAGA PERMISOES PASTAS E ARQUIVOS


  // CRIA PERMISOES PASTAS E ARQUIVOS
  public function permitePasta($dados){
    $this->apagaPermissao($dados); // FUNC DEL PERMISSÕES PASTA/ARQ/DOC
    $idUser = $this->usuariosP($dados); // FUNC LIST ID-USUARIOS
    $up = $this->updn($dados); // FUNC CONVERTE UPDN
    $cr = $this->crud($dados); // FUNC CONVERTE CRUD

    if($up + $cr == 0)
    return back()->withInput()->with(['success' => 'Retirado o acesso do(s) usuário(s).']);


    foreach ($idUser as $keyU) {

      foreach ($this->pastaB as $keyP) { // PERMISSAO DE PASTA
        if($this->pastaRaiz($keyU) == false) dd('STOP');
        else{

          if($keyP['id'] == $dados['pasta_id']) // PASTA RAIZ
          $ref_pai = $this->pastaRaiz($keyU);
          else                                  // SUBPASTAS
          $ref_pai = $keyP['ref_pai'];

          $p = Permissao::create([
            'pasta_id'  => $keyP['id'],
            'crud'      => $this->crud($dados),
            'updown'    => $this->updn($dados),
            'ref_pai'   => $ref_pai,
            'user_id'   => $keyU,
            'sts'       => 2
          ]);
        }
      }

      foreach ($this->arquiB as $keyA) { // PERMISSAO DE ARQUIVOS
        Permissao::create([
          'arquivo_id'  => $keyA['id'],
          'pasta_id'    => $keyA['pasta_id'],
          'crud'        => $this->crud($dados),
          'updown'      => $this->updn($dados),
          'user_id'     => $keyU,
          'sts'         => 2
        ]);
      }

      foreach ($this->documB as $keyD) { // PERMISSAO DE DOCUMENTOS
        Permissao::create([
          'documento_id'=> $keyD['id'],
          'pasta_id'    => $keyD['pasta_id'],
          'crud'        => $this->crud($dados),
          'updown'      => $this->updn($dados),
          'user_id'     => $keyU,
          'sts'         => 2
        ]);
      }
    }

    return back()->withInput()->with(['success' => 'Compartilhado com sucesso.']);
  }
  // CRIA PERMISOES PASTAS E ARQUIVOS
  // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  public function index(){
    //
  }

  public function create(){
    //
  }

  public function store(Request $request){
    //
  }

  public function show($id){
    $permissao  = Permissao::find($id);
    $grupos     = Grupo::EmpAct()->get();

    if($permissao->arquivo_id != NULL){
      $usuarios  = Usuario::EmpAct()
      ->join('sog_permissao as P', 'id', '=', 'P.user_id')
      ->where('P.arquivo_id',$permissao->arquivo_id)
      ->distinct('users.name')
      ->get();
    }elseif($permissao->documento_id != NULL){
      $usuarios  = Usuario::EmpAct()
      ->join('sog_permissao as P', 'id', '=', 'P.user_id')
      ->where('P.pasta_id',$permissao->pasta_id)
      ->where('P.arquivo_id',NULL)
      ->where('P.ref_pai',NULL)
      ->get();
    }else{
      $usuarios  = Usuario::EmpAct()
      ->join('sog_permissao as P', 'id', '=', 'P.user_id')
      ->where('P.pasta_id',$permissao->pasta_id)
      ->where('P.arquivo_id',NULL)
      ->where('P.documento_id',NULL)
      ->get();
    }

    return view('doc.permissao', compact('permissao','grupos','usuarios'));
  }

  public function mostrar(Request $request){
    $data   = $request->all();
    if ($data['arquivo_id'] == NULL)
    $permi  = Permissao::where('pasta_id',$data['pasta_id'])->where('user_id',$data['user_id'])->count();
    else
    $permi  = Permissao::where('arquivo_id',$data['arquivo_id'])->where('user_id',$data['user_id'])->count();


    if($permi == 0) {
      $nm   = \Auth::user()->empresa_id.'p'.$data['user_id'];
      if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0)
      return redirect()->route('pasta.index')->with(['danger' => 'Usuário não possui pasta.']);

      $idP  = Pasta::select('id_pasta')->where('pasta',$nm)->get();

      if($data['arquivo_id'] != NULL){
        $idPer = Permissao::create([
          'arquivo_id'=> $data['arquivo_id'],
          'pasta_id'  => $idP[0]->id_pasta,
          'crud'      => 1,
          'updown'    => 0,
          'user_id'   => $data['user_id'],
          'sts'       => 2
          ])->id_permissao;
          $permissao  = Permissao::find($idPer);
        }else {
          $idPer = Permissao::create([
            'pasta_id'  => $data['pasta_id'],
            'crud'      => 1,
            'updown'    => 0,
            'user_id'   => $data['user_id'],
            'ref_pai'   => $idP[0]->id_pasta,
            'sts'       => 2
            ])->id_permissao;
            $permissao  = Permissao::find($idPer);
          }
    }else {
      if($data['arquivo_id'] != NULL){
        $idpermi      = Permissao::where('arquivo_id',$data['arquivo_id'])->where('user_id',$data['user_id'])->get();
        $permissao    = Permissao::find($idpermi[0]->id_permissao);
      }else {
        $idpermi      = Permissao::where('pasta_id',$data['pasta_id'])->where('user_id',$data['user_id'])->get();
        $permissao    = Permissao::find($idpermi[0]->id_permissao);
      }
    }

    $grupos           = Grupo::EmpAct()->get();
    $usuarios         = Usuario::EmpAct()->get();
    return view('doc.permissao', compact('permissao','grupos','usuarios'));
  }

  public function compartilhar($id){
    $grupos       = Grupo::EmpAct()->get();
    $usuarios     = Usuario::EmpAct()->get();
    $permissao    = Permissao::find($id);

    if($permissao->arquivo_id != NULL){
      $userComp   = Usuario::EmpAct()
      ->join('sog_permissao as P', 'id', '=', 'P.user_id')
      ->where('P.arquivo_id',$permissao->arquivo_id)
      ->distinct('users.name')
      ->get();
    }else {
      $userComp   = Usuario::EmpAct()
      ->join('sog_permissao as P', 'id', '=', 'P.user_id')
      ->where('P.pasta_id',$permissao->pasta_id)
      ->where('P.arquivo_id',NULL)
      ->get();
    }
    return view('doc.compartilhar', compact('grupos','usuarios','permissao','userComp'));
  }
  //--------------------------------------------------------------------------
  public function compStore(Request $request){
    $data   = (object) $request->all();
    $idUser = $this->usuariosP($request->all());

    if($data->documento_id != NULL) // CRIA PERMISSÂO PARA DOCUMENTOS
    $this->permiteDoc($request->all());

    elseif($data->arquivo_id != NULL) // CRIA PERMISSÂO PARA ARQUIVOS
    $this->permiteArq($request->all());
    else                          // CRIA PERMISSAO PARA PASTAS
    $this->permitePasta($request->all());

    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    // $log                = new Log;
    // $log->id_empresa    = \Auth::user()->empresa_id;
    // $log->id_user       = \Auth::id();
    // $log->desc          = 'Criado, COMPARTILHADO';
    // $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return back()->withInput();
  }
  //---------------------------------------------------------------------------
  public function edit($id){
    //
  }
  public function update(Request $request){


   if(isset($request->all()['acao']) == 'MOVER'){ // PARA MOVER DE PASTA

     if($request->id_arquivo != 0){

       $permissao = Permissao::where('arquivo_id',$request->id_arquivo)
       ->where('user_id',\Auth::user()->id)
       ->first();

       $permissao->pasta_id = $request->pasta_id_ref;

       $permissao->save();

     }
     else if($request->id_documento != 0){

       $permissao = Permissao::where('documento_id',$request->id_documento)
       ->where('user_id',\Auth::user()->id)
       ->first();

       $permissao->pasta_id = $request->pasta_id_ref;

       $permissao->save();

     }
     else if($request->id_pasta != 0){

       $permissao = Permissao::where('pasta_id',$request->id_pasta)
       ->where('user_id',\Auth::user()->id)
       ->whereNotNull('ref_pai')
       ->first();

       $permissao->ref_pai = $request->pasta_id_ref;

       $permissao->save();

     }


   }

   else{
     if($this->crud($request->all()) != 0){
      $idP            = Permissao::find($request->all()['id_permissao']);
      $idP->crud    = $this->crud($request->all());
      $idP->updown  = $this->updn($request->all());
      $idP->save();
    }else {
     $idp->delete();
     return redirect()->route('pasta.index');
   }

   $permissao      = Permissao::find($request->all()['id_permissao']);
   $grupos         = Grupo::EmpAct()->get();
   $usuarios       = Usuario::EmpAct()->get();
   // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
   /*$log                = new Log;
   $log->id_empresa    = \Auth::user()->empresa_id;
   $log->id_user       = \Auth::id();
   $log->permissao_id  = $idP->id_permissao;
   $log->desc          = 'Alterado, COMPARTILHADO';
   $log->save();*/
   // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
   return view('doc.permissao', compact('permissao','grupos','usuarios'))->with(['success' => 'Alterado com sucesso.']);
 }


}
  // public function update(Request $request){
  //   $idP            = Permissao::find($request['id_permissao']);
  //
  //   if(isset($request->all()['acao']) == 'MOVER'){ // PARA MOVER DE PASTA
  //     $idP->update($request->all());
  //     return  back()->withInput()->with(['success' => 'Alterado com sucesso.']);
  //   }
  //
  //   if($this->crud($request->all()) != 0){
  //     $idP->crud    = $this->crud($request->all());
  //     $idP->updown  = $this->updn($request->all());
  //     $idP->save();
  //   }else {
  //     $idp->delete();
  //     return redirect()->route('pasta.index');
  //   }
  //
  //   $permissao      = Permissao::find($request->all()['id_permissao']);
  //   $grupos         = Grupo::EmpAct()->get();
  //   $usuarios       = Usuario::EmpAct()->get();
  //   // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  //   $log                = new Log;
  //   $log->id_empresa    = \Auth::user()->empresa_id;
  //   $log->id_user       = \Auth::id();
  //   $log->permissao_id  = $idP->id_permissao;
  //   $log->desc          = 'Alterado, COMPARTILHADO';
  //   $log->save();
  //   // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  //   return view('doc.permissao', compact('permissao','grupos','usuarios'))->with(['success' => 'Alterado com sucesso.']);
  // }

  public function destroy($id){
    //
  }
}
