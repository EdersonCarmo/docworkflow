<?php

namespace Soged\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Soged\User;
use Soged\Alert;
use Soged\SessionController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

      public function __construct(){
        session()->forget('teste');
        session()->put('teste', Alert::Active()->orderBy('status', 'asc')->get());
      }
}
