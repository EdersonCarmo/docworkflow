<?php namespace Soged\Http\Controllers;

use Soged\Http\Controllers\ReplaceStr;
use Illuminate\Http\Request;
use Soged\Documento;
use Soged\Permissao;
use Soged\MolduraM;
use Soged\Template;
use Soged\Empresa;
use Soged\Usuario;
use Soged\Arquivo;
use Soged\Cliente;
use Soged\Fluxo;
use Soged\Alert;
use Soged\Pasta;
use Soged\Log;
use PDF;
use App;
use Carbon\Carbon;

class DocumentoController extends Controller
{
  public function fluxoAcao($id){
    $acao   = null;
    $idD    = Documento::findOrFail($id);

    // VERIFICA FLUXO E CRIA ALERTAS VERIFICA FLUXO E CRIA ALERTAS
    $idF = Fluxo::where('ordem', $idD->status)
    ->where('documento_id', $id)
    ->where('status', 1)
    ->get();

    Alert::where('documento_id', $id)->delete();

    if(count($idF) > 0) {
      foreach ($idF as $key) {
        $idA          = new Alert;
        $idA->desc    = $key->fluxo;
        $idA->status  = 1;
        $idA->user_id = $key->user_id;
        $idA->fluxo_id= $key->id_fluxo;
        $idA->Tipo    = $key->tipo;
        $idA->documento_id  = $key->documento_id;

        if($key->tipo == 1){ // ENCAMINHA
          $idA->alerta  = 'Encaminhado';
          $idA->save();
        }elseif($key->tipo == 2){ // ENVIA EMAIL
          //$idF->status  = $idF->status + 1; var_dump('emai \n');
          $idD->status = $idD->status + 1;
          $acao = $key->email;
        }elseif($key->tipo == 3){ // CONFIRMAÇÃO
          $idA->alerta  = 'Confirmação';
          $idA->save();
        }

        if($acao)
          Fluxo::where('id_fluxo', $key->id_fluxo)->delete();
        else{
          $idF2 = Fluxo::findOrFail($key->id_fluxo);
          $idF2->status   = 2;
          $idF2->save();
        }
      }
    }else {
      Fluxo::where('documento_id', $id)->delete();
      $idD->status = 1;
    }

    //$idD->status = $idD->status + 1;
    $idD->save();
    // VERIFICA FLUXO E CRIA ALERTAS VERIFICA FLUXO E CRIA ALERTAS
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->documento_id  = $idD->id_documento;
    $log->desc          = 'Alterado, DOC';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //

    if($acao)
      return redirect()->route('mailFluxo',[$id, $acao]);
    else
      return redirect()->route('pasta.index');
  }
  // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  public function enviaEmail($id){
    return redirect('mailsend/');
    //return redirect()->route('mailTrocaSenha',['desenvolvimento06soitic@gmail.com','123456','teste']);
  }

  public function index()  {
  }

  public function create()  { // CRIA TEMPLATE
  }

  public function createDoc($pasta)  { // CRIA DOCUMENTO

    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    return view('doc.editor', compact('pasta','molduras'));
  }

  public function createDocTem(Request $request)  { // CRIA DOCUMENTO
    $template = Documento::findOrFail($request->all()['id_documento']);
    $u        = Usuario::findOrFail(\Auth::id());
    $pasta    = $request->all()['pasta'];
    $template->texto = 'ReplaceDoc'($template->texto, $u);

    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    return view('doc.editor', compact('template', 'pasta','molduras'));
  }

  public function AlterarTags($texto,$cliente,$empresa){

    $tags = array('@nome_a',
      '@nome_b',
      '@cnpj_a',
      '@cnpj_b',
      '@razao_a',
      '@razao_b',
      '@ie_a',
      '@ie_b',
      '@uf_a',
      '@uf_b',
      '@cidade_a',
      '@cidade_b',
      '@bairro_a',
      '@bairro_b',
      '@rua_a',
      '@rua_b',
      '@numero_a',
      '@numero_b',
      '@cep_a',
      '@cep_b',
      '@comp_a',
      '@comp_b');

    $dados = array($empresa->empresa,
      $cliente->cliente,
      $empresa->cnpj,
      $cliente->cnpj,
      $empresa->razao,
      $cliente->razao,
      $empresa->ie,
      $cliente->id_rg,
      $empresa->uf,
      $cliente->uf,
      $empresa->cidade,
      $cliente->cidade,
      $empresa->bairro,
      $cliente->bairro,
      $empresa->logradouro,
      $cliente->logradouro,
      $empresa->numero,
      $cliente->numero,
      $empresa->cep,
      $cliente->cep,
      $empresa->complemento,
      $cliente->complemento);

    $texto_alterado = str_replace($tags, $dados, $texto);

    return $texto_alterado;
  }

  public function createDocs(Request $request){

    $template = Documento::findOrFail($request->all()['id_documento']);

    $empresa = Empresa::where('id_empresa',\Auth::user()->empresa_id)
    ->join('sog_enderecos','sog_enderecos.id_endereco','sog_empresas.endereco_id')
    ->first();

    foreach ($request->cliente_id as $key) {
      $cliente = Cliente::where('id_cliente',$key)
      ->join('sog_enderecos','sog_enderecos.id_endereco','sog_clientes.endereco_id')
      ->first();

      $texto_alterado = $this->AlterarTags($template->texto,$cliente,$empresa);

      $idD = Documento::create([
        'documento' => Carbon::now()->format('d-m-y').' - '.$cliente->cliente,
        'texto' => $texto_alterado,
        'status' => 1,
        'cliente_id' => $key,
        'pasta_id' => $request->pasta
      ])->id_documento;

      $idP = Permissao::create([
        'documento_id' => $idD,
        'ref_pai' => $request->pasta,
        'crud' => 15,
        'user_id' => \Auth::id()
      ])->id_permissao;
      
    }

    return back()->withInput()->with(['success' => 'Excluido com sucesso.']);

  }

  public function createTem($pasta){
    $templates = Template::EmpAct()->orderBy('template')->get();
    $tem  = 1;

    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    return view('doc.editor', compact('pasta','tem','templates','molduras'));
  }

  public function apaga($id){
    Documento::where('id_documento',$id)->update(['status' => 0]);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->documento_id    = $id;
    $log->desc          = 'Excluído, ARQ';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    // return back()->withInput()->with(['success' => 'Excluído com sucesso.']);
  }

  public function store(Request $request)  { // SALVA DOC + PERMISSAO

    $data = $request->all();

    if(isset($data['template_id'])) {
      $p = Pasta::where('pasta',\Auth::user()->empresa_id.'t'.\Auth::id())->get();
      $data['pasta_id'] = $p[0]['id_pasta'];
    }

    $u = Empresa::findOrFail(\Auth::user()->empresa_id);
    $data['texto'] = ReplaceStr($data, $u);

    $idD = Documento::create($data)->id_documento;

    $idP = Permissao::create([
      'documento_id' => $idD,
      'ref_pai' => $data['pasta_id'],
      'crud' => 15,
      'user_id' => \Auth::id()
    ])->id_permissao;
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->permissao_id  = $idP;
    $log->documento_id  = $idD;
    $log->desc          = 'Criado, DOC + PERMI';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('documento.show', $idD);
  }

  public function show($id, $end = 0)  {
    $clientes           = Cliente::EmpAct()->get();
    $documento          = Documento::find($id);
    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    return view('doc.editor', compact('documento','clientes','end','molduras'));
  }

  public function show2($id, $end)  {
    $clientes   = Cliente::EmpAct()->get();
    $documento  = Documento::find($id);
    $molduras   = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    if($documento->template_id > 0)
      return view('doc.editor', compact('documento','end','molduras'));

    return view('doc.editor', compact('documento','clientes','end','molduras'));
  }

  public function rename(Request $request, $id){
    $id = Documento::findOrFail($request->all()['id_documento']);
    $id->update($request->all());
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->arquivo_id    = $id->id_documento;
    $log->desc          = 'Alterado, ARQ';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    // return  back()->withInput()->with(['success' => 'Alterado com sucesso.']);
  }

  public function update(Request $request, $id)  {
    $data   = $request->all(); $acao = null;
    $idD    = Documento::findOrFail($data['id_documento']);

    if(isset($data['renomear_documento'])){

      $id = Documento::findOrFail($request->all()['id_documento']);
      $id->update($request->all());

    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
      $log                = new Log;
      $log->id_empresa    = \Auth::user()->empresa_id;
      $log->id_user       = \Auth::id();
      $log->arquivo_id    = $id->id_documento;
      $log->desc          = 'Alterado, ARQ';
      $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //

      return 'true';
    }

    if(isset($data['local'])){
      $idD->texto = '<div style="background-image:url('.$data['local'].'); background-size:100% 1080px">'.$id->texto;
      $idD->save();
    }else {
      $a    = Empresa::findOrFail(\Auth::user()->empresa_id);
      $b    = 0;

      
// VERIFICA FLUXO E CRIA ALERTAS VERIFICA FLUXO E CRIA ALERTAS
      if(isset($data['status']) and $idD->status != $data['status']){

        //$ret = $this->tipoFluxo($data, $idD);

        $idF = Fluxo::where('ordem',$data['status'])
        ->where('documento_id', $idD->id_documento)
        ->where('status', 1)
        ->get();

        Alert::where('documento_id', $data['id_documento'])->delete();

        if(count($idF) > 0) {
          foreach ($idF as $key) {
            $idA          = new Alert;
            $idA->desc    = $key->fluxo;
            $idA->status  = 1;
            $idA->user_id = $key->user_id;
            $idA->fluxo_id= $key->id_fluxo;
            $idA->Tipo    = $key->tipo;
            $idA->documento_id  = $key->documento_id;

            if($key->tipo == 1){ // ENCAMINHA
              $idA->alerta  = 'Encaminhado';
              $idA->save();
            }elseif($key->tipo == 2){ // ENVIA EMAIL
              $data['status'] = $data['status'] + 1;
              $acao = $key->email;
            }elseif($key->tipo == 3){ // CONFIRMAÇÃO
              $idA->alerta  = 'Confirmação';
              $idA->save();
            }

            if($acao)
              Fluxo::where('id_fluxo', $key->id_fluxo)->delete();
            else{
              $idF2 = Fluxo::findOrFail($key->id_fluxo);
              $idF2->status   = 2;
              $idF2->save();
            }
          }
        }else {
          Fluxo::where('documento_id', $idD->id_documento)
          ->delete();
          $data['status'] = 1;
        }

        $idD->update($data);

        if($acao)
          return redirect()->route('mailFluxo',[$id, $acao]);
        else
          return redirect()->route('pasta.index');
      }
      // VERIFICA FLUXO E CRIA ALERTAS VERIFICA FLUXO E CRIA ALERTAS
      $idD->update($data);
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
      $log                = new Log;
      $log->id_empresa    = \Auth::user()->empresa_id;
      $log->id_user       = \Auth::id();
      $log->documento_id  = $idD->id_documento;
      $log->desc          = 'Alterado, DOC';
      $log->save();
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    }

    //if(isset($idF) and count($idF) > 0) return redirect('mailsend/fluxo/desenvolvimento06soitic@gmail.com/1');
    // return $this->show($data['id_documento'])->with('success', 'Alterado com sucesso.');
    return back()->withInput()->with(['success' => 'Alterado com sucesso.']);

  }

  public function busca(Request $request)  {
    $documentos = Documento::UserAct()
    ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
    ->where('documento', 'like', '%'.$request->all()['nome'].'%')
    ->get();

    $arquivos = Arquivo::UserAct()
    ->join('sog_permissao as P', 'id_arquivo', '=', 'P.arquivo_id')
    ->where('arquivo', 'like', '%'.$request->all()['nome'].'%')
    ->get();

    $pastas   = Pasta::Active()
    ->join('sog_permissao as P', 'id_pasta', '=', 'P.pasta_id')
    ->where('pasta', 'like', '%'.$request->all()['nome'].'%')
    ->where('sog_pastas.user_id', \Auth::id())
    ->where('P.user_id', \Auth::id())
    ->distinct('pasta')
    ->get();

    $atual    = 'Busca: '.$request->all()['nome'];
    return view('doc.pasta',compact('arquivos','atual','documentos','pastas'));
  }

  public function reabreDoc($id)  {
    $idD      = Documento::findOrFail($id);
    $idD->status = 1;
    Fluxo::where('documento_id', $id)->update(['status' => 1]);
    Alert::where('documento_id', $id)->delete();
    $idD->save();

    return $this->show($id);
  }

// PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF PDF

  public function pdf($id)  {
    $idD    = Documento::findOrFail($id);
    $texto  = $idD->texto;
    $pdf    =  PDF::loadView('doc.pdf', ['texto' => $texto]);

    return $pdf->download('arquivo.pdf');
  }

  public function enviaPdf($id, $email)  { dd('FUNCTION: enviaPdf');
  $idD    = Documento::findOrFail($id);
  $texto  = $idD->texto;
  $pdf    =  PDF::loadView('doc.pdf', ['texto' => $texto]);

  return redirect()->route('mailFluxo',[$email, $pdf]);
    //return $pdf->download('arquivo.pdf');
}

public function Imprimir($id){
  $documento = Documento::find($id);

  return view('doc.print-doc',compact('documento'));
}

public function ImprimirMoldura(Request $dados){
  
  $documento = Documento::find($dados->id_documento);
  $moldura   = MolduraM::find($dados->moldura);

  return view('doc.print-moldura',compact('documento','moldura'));
}

public function download($id){
  $documento = Documento::find($id);

  $pdf = App::make('dompdf.wrapper');
  $pdf->loadHTML($documento->texto);
  return $pdf->stream();
}
}
