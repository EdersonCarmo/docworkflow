<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Documento;
use Soged\Cliente;
use Soged\Alert;
use Soged\Fluxo;

class AlertaController extends Controller
{
  public function index()  {
    //
  }

  public function create()  {
    //
  }

  public function store(Request $request)  {
    //
  }

  public function show($id)  { // LISTA ALERTAS
    //
  }

  public function edit($id)  { // ABRE DOCUMENTO DO ALERTA
    $idA          = Alert::findOrFail($id);
    $idF          = Fluxo::findOrFail($idA->fluxo_id);
    $clientes     = Cliente::EmpAct()->get();
    $idA->status  = 2;
    $idA->save();
    if($idA->documento_id > 1){
        $documento= Documento::find($idA->documento_id);
        $tipo     = $idF->tipo ; $end = 0;
        return view('doc.editor', compact('documento','clientes','tipo','end'));
    }

    return redirect()->route('pasta.index');
  }

  public function update(Request $request, $id)  {
    //
  }

  public function destroy($id)  {
    //
  }
}
