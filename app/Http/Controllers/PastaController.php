<?php
namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Pasta;
use Soged\Arquivo;
use Soged\Permissao;
use Soged\Session;
use Soged\SessionController;
use Soged\Documento;
use Soged\Alert;
use Soged\Log;
use Soged\User;
use Soged\Cliente;
use Soged\MolduraM;

class PastaController extends Controller
{
  public function pastaRaiz($tipo = 'p'){

    $atual = \Auth::user()->empresa_id.$tipo.\Auth::id();
    $id = Pasta::where('pasta', $atual)->get();
    $er = (object) $id;
    return $er[0]['id_pasta'];
  }

  public function arcDoc($tipo){

    $usuarios = User::where('empresa_id',\Auth::user()->empresa_id)->get();

    $atual    = $this->pastaRaiz($tipo);
    $pasta    = Pasta::find($atual);
    $_SESSION['idpastaatual'] = $atual;

    $pastas   = Pasta::Active()
    ->join('sog_permissao as P', 'id_pasta', '=', 'P.pasta_id')
    ->where('P.user_id', \Auth::id())
    ->where('P.ref_pai', $atual)
    ->where('P.sts', '>',0)
    ->where('status', '>',0)
    ->get();

    $folder   = Pasta::Active()->where('user_id',\Auth::id())->get();

    $arquivos = Arquivo::Active()
    ->join('sog_permissao as P', 'id_arquivo', '=', 'P.arquivo_id')
    ->where('P.ref_pai', $atual)
    ->where('P.user_id', \Auth::id())
    ->get();

    $documentos = Documento::select()
    ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
    ->where('P.ref_pai', $atual)
    ->where('P.user_id', \Auth::id())
    ->where('status', 1)
    ->get();

    $templates = Documento::select()
    ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
    ->whereNotNull('template_id')
    ->where('P.user_id', \Auth::id())
    ->get();

    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    $clientes = Cliente::Active()->orderBy('cliente')->get();

    $_SESSION['nomeatual'] = $pasta->pasta;
    return compact('pastas','arquivos','atual','folder','documentos','templates', 'usuarios', 'tipo','clientes','molduras');
  }

  public function index(Request $request){
    $request->session()->forget('pasta');
    $request->session()->forget('pnome');

    return view('doc.pasta', $this->arcDoc($tipo= 'p'));
  }

  public function abrir($tipo){

    return view('doc.pasta', $this->arcDoc($tipo= $tipo));

  }

  public function store(Request $request){
    $data = $request->all();

    $idPasta = Pasta::create([
      'pasta' => $request->pasta,
      'user_id' => \Auth::id(),
      'ref_pai' => $request->ref_pai,
      'status' => 2
    ]);

    $pasta = Pasta::find($request->ref_pai);

    /* VERIFICA SE A PASTA É COMPARTILHADA */
    if($pasta->status == 2){
      $usuarios_permitidos = Permissao::where('pasta_id',$pasta->id_pasta)->get();

      foreach ($usuarios_permitidos as $key) {

        Permissao::create([
          'pasta_id'=> $idPasta->id_pasta,
          'ref_pai'  => $request->ref_pai,
          'crud'      => $key->CRUD,
          'user_id'   => $key->user_id
        ])->id_permissao;

      }
    }
    else{

      $idP = Permissao::create([
        'pasta_id'  => $idPasta->id_pasta,
        'ref_pai'   => $idPasta->ref_pai,
        'crud'      => 63,
        'user_id'   => \Auth::id()
      ]);

    }

    return 'true';
  }

  public function show(Request $request, $atual){

    $idU      = \Auth::id();
    $pasta    = Pasta::find($atual);
    $folder   = Pasta::Active()->where('user_id',$idU)->get();
    $pastas   = Pasta::Active()->where('ref_pai',$atual)->get();

    $breadcrumb = Pasta::find($atual);
    $breadcrumb_lista = array($breadcrumb);

    while ($breadcrumb->ref_pai) {
      $pasta_pai = $breadcrumb->ref_pai;
      $breadcrumb = Pasta::find($pasta_pai);
      array_push($breadcrumb_lista, $breadcrumb);
    }

    $id_empresa = \Auth::user()->empresa_id;
    $usuarios = User::where('empresa_id',$id_empresa)->get();

    $pastas   = Pasta::Active()
    ->join('sog_permissao as P', 'id_pasta', '=', 'P.pasta_id')
    ->where('P.ref_pai', $atual)
    ->where('P.user_id', $idU)
    ->distinct()
    ->get();

    $arquivos = Arquivo::Active()
    ->join('sog_permissao', 'id_arquivo', '=', 'sog_permissao.arquivo_id')
    ->where('sog_permissao.ref_pai', $atual)
    ->where('user_id', $idU)
    ->distinct()
    ->get();

    $documentos = Documento::select()
    ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
    ->where('P.ref_pai', $atual)
    ->where('P.user_id', $idU)
    ->where('status','>',0)
    ->get();

    $templates = Documento::select()
    ->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
    ->where('template_id', 1)
    ->where('P.user_id', $idU)
    ->get();

    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
    ->join('users as U', 'U.id','user_id')
    ->where('U.empresa_id', \Auth::user()->empresa_id)
    ->where('sog_molduras.status', 1)
    ->get();

    $_SESSION['idpastaatual'] = $atual;
    $_SESSION['nomeatual'] = $pasta->pasta;
    $request->session()->push('pasta', [['id' => $atual],['nome' => $pasta->pasta]]);

    return view('doc.pasta', compact('pastas','arquivos','folder','atual','documentos','templates','breadcrumb_lista','usuarios','molduras'));
  }

  public function edit($id){}

  public function update(Request $request){

  // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;

    $idPa = Pasta::findOrFail($request['id_pasta']);
    $idPa->update($request->all());
    if(isset($request['id_permissao'])){
      $idPe = Permissao::findOrFail($request['id_permissao']);
      $idPe->update($request->all());
      $log->permissao_id  = $idPe->id_permissao;
    }

    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->pasta_id      = $idPa->id_pasta;
    $log->desc          = 'Alterado, PERMI + PASTA';
    $log->save();

    // return redirect()->route('pasta.index');
  }

  public function apaga($id){
    $id = Permissao::findOrFail($id);
    if($id->sts == 2) // SE COMPARTILHADO APAGA
    $id = Permissao::where('id_permissao', $id)->delete();

    Permissao::where('id_permissao',$id)->update(['sts' => 0]);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->pasta_id      = $idP->id_pasta;
    $log->permissao_id  = $id;
    $log->desc          = 'Excluído, PERMI.PASTA';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return back()->withInput()->with(['success' => 'Excluido com sucesso.']);
  }

  public function destroy($id){}
}
