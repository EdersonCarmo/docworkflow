<?php namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Http\Requests\StoreEmpresa;
use Soged\Empresa;
use Soged\Endereco;
use Soged\Log;

class EmpresaController extends Controller
{
  public function index(){
    return view('empresa.listEmpresa')->with('empresas', Empresa::Active()->get());
  }

  public function create(){
    return view('empresa.empresa');
  }

  function clean($string) {
   $string = str_replace(' ', '-', $string);

   return preg_replace('/[^0-9]+/', '', $string);
 }

 public function store(Request $request){

  $cnpj = $this->clean($request->cnpj);
  $request->merge(['cnpj' => $cnpj]);
  $telefone = $this->clean($request->telefone);
  $request->merge(['telefone' => $telefone]);
  $celular = $this->clean($request->celular);
  $request->merge(['celular' => $celular]);
  
  $end    = Endereco::create($request->all())->id_endereco;
  $dados  = $request->all();
  $dados['endereco_id'] = $end;
  $id     = Empresa::create($dados)->id_empresa;
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  $log                = new Log;
  $log->id_empresa    = \Auth::user()->empresa_id;
  $log->id_user       = \Auth::id();
  $log->empresa_id    = $id;
  $log->desc          = 'Criado, EMP + END';
  $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  return redirect()->route('empresa.index');
}

public function edit($id){
  return view('empresa.empresa')->with('empresa', Empresa::find($id));
}

public function update(StoreEmpresa $request, $id){
  $emp    = $request->all();
  $id     = Empresa::findOrFail($id);
  $ide    = Endereco::findOrFail($emp['endereco_id']);

  $ide->numero = $emp['numero'];
  $id->cnpj = $emp['cnpj'];

  $ide->update($emp);
  $id->update($emp);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  $log                = new Log;
  $log->id_empresa    = \Auth::user()->empresa_id;
  $log->id_user       = \Auth::id();
  $log->empresa_id    = $ide->id_empresa;
  $log->desc          = 'Alterado, EMP + END';
  $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  return redirect()->route('empresa.index');
}

public function apaga($id){
  Empresa::where('id_empresa',$id)->update(['status' => 0]);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  $log                = new Log;
  $log->id_empresa    = \Auth::user()->empresa_id;
  $log->id_user       = \Auth::id();
  $log->empresa_id    = $id;
  $log->desc          = 'Excluído, EMP';
  $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
  return redirect()->route('empresa.index')->with(['success' => 'Cadastrado com sucesso.']);
}

}
