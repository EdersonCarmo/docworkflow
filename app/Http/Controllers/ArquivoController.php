<?php namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Soged\Arquivo;
use Soged\Pasta;
use Soged\Documento;
use Soged\Permissao;
use Storage;
use Illuminate\Http\UploadedFile;
use Soged\Log;
use PDF;

class ArquivoController extends Controller
{

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	|																					|
	|			LIMPAR CARACTERES ESPECIAIS					|
	|																					|
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

	public static function clean($string) {

		$result  = preg_replace('/[^a-zA-Z0-9_ -.,\/]/s','_',$string);

		return $result;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
	|																					|
	|							SALVAR ARQUIVOS							|
	|																					|
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

	public static function SalvarArquivos(Request $request){

		$data     = $request->all();
		$arq      = $request->file('arquivo');
		$pasta    = 'empresas/'.\Auth::user()->empresa_id.'/'.$data['pastaend'];

		if(!empty($arq)){

			foreach ($arq as $key) {

				/* ENVIO DE ARQUIVOS MULTIPLOS S3 */
				$filename = $pasta.$key->getClientOriginalName();

				$filename =	self::clean($filename);

				$existe   = Storage::disk('s3')->exists($filename);
				if($existe == true){
					Storage::disk('s3')->delete($filename);
				}

				$s3 = Storage::disk('s3')->put($pasta, $key, 'public');
				Storage::disk('s3')->move($s3, $filename);
				Storage::disk('s3')->delete($s3);

				/* SALVA ARQUIVOS NO BD */
				$idA = Arquivo::create([
					'arquivo'   => $key->getClientOriginalName(),
					'local'     => $filename,
					'pasta_id'  => $data['pastaatual'],
					'size'      => filesize($key)
				])->id_arquivo;

				$pasta = Pasta::find($data['pastaatual']);

				/* VERIFICA SE A PASTA É COMPARTILHADA */
				if($pasta->status == 2){
					$usuarios_permitidos = Permissao::where('pasta_id',$pasta->id_pasta)->get();

					foreach ($usuarios_permitidos as $key) {

						$idP = Permissao::create([
							'arquivo_id'=> $idA,
							'ref_pai'  => $data['pastaatual'],
							'crud'      => $key->crud,
							'user_id'   => $key->user_id
						])->id_permissao;

					}
				}
				else{

					$idP = Permissao::create([
						'arquivo_id'=> $idA,
						'ref_pai'  => $data['pastaatual'],
						'crud'      => 29,
						'user_id'   => \Auth::id()
					])->id_permissao;

				}
			}
		}
		return back()->withInput();
	}

	public function baixar($id){
		$down = Arquivo::findOrFail($id);

		return  back()->withInput()->with(['download' => $down->local]);
	}

	public function index(){
		$info = Storage::disk('s3')->getObjectInfo('empresas/fotos', '1.png');
		print $info['size'];
	}

	public function update(Request $request, $id){
		$id = Arquivo::findOrFail($request->all()['id_arquivo']);
		$id->update($request->all());
// REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
		$log                = new Log;
		$log->id_empresa    = \Auth::user()->empresa_id;
		$log->id_user       = \Auth::id();
		$log->arquivo_id    = $id->id_arquivo;
		$log->desc          = 'Alterado, ARQ';
		$log->save();
// REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
// return  back()->withInput()->with(['success' => 'Alterado com sucesso.']);
	}

	public function apaga($id){
		Arquivo::where('id_arquivo',$id)->update(['status' => 0]);
// REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
		$log                = new Log;
		$log->id_empresa    = \Auth::user()->empresa_id;
		$log->id_user       = \Auth::id();
		$log->arquivo_id    = $id;
		$log->desc          = 'Excluído, ARQ';
		$log->save();
// REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
// return back()->withInput()->with(['success' => 'Excluído com sucesso.']);
	}

	public function salvaPdf($id, $end)  {
		$end      = decrypt($end);
		$idP      = Permissao::where('documento_id', $id)
		->where('user_id', \Auth::id())
		->get();
		$idD      = Documento::findOrFail($id);
		$pasta    = 'empresas/'.\Auth::user()->empresa_id.'/'.$end.$idD->documento.'.pdf';
		$texto    = $idD->texto;
		$pdf      = PDF::loadView('doc.pdf', ['texto' => $texto]);
//$pdftemp  = 'empresas/'.\Auth::user()->empresa_id.'-'.\Auth::id().'.pdf';

//file_put_contents($pdftemp, $pdf->output());
//Storage::disk('public')->put('teste.pdf', $pdf->output()); // SALVA NA PASTA

		$filename = $end.$idD->documento.'.pdf';
		$existe   = Storage::disk('s3')->exists($filename);
		if($existe == true)
			Storage::disk('s3')->delete($filename);

		$s3 = Storage::disk('s3')->put($pasta, $pdf->output(), 'public');
// SALVA ARQUIVO NO BD
		if($existe == false){
			$idA = Arquivo::create([
				'arquivo'   => $idD->documento.'.pdf',
				'local'     => $pasta,
				'pasta_id'  => $idP[0]->pasta_id
			])->id_arquivo;

			$idP2 = Permissao::create([
				'arquivo_id'=> $idA,
				'pasta_id'  => $idP[0]->pasta_id,
				'crud'      => 15,
				'updown'    => 7,
				'user_id'   => \Auth::id()
			])->id_permissao;
			return back()->withInput()->with(['success' => 'Criado o arquivo PDF na pasta com sucesso.']);
		}
	}
}
