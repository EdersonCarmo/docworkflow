<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Grupo;
use Soged\Log;

class GrupoController extends Controller
{
  public function index(){
    $grupos = Grupo::EmpAct()->get();
    return view('user.listGrupos')->with('grupos', $grupos);
  }

  public function create(){
    return view('user.grupo');
  }

  public function store(Request $request){
    $idG = Grupo::create($request->all())->id_grupo;
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->grupo_id      = $idG;
    $log->desc          = 'Criado, GRUPO';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('grupo.index')->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function show($id){
    //
  }

  public function edit($id){
    return view('user.grupo')->with('grupo', Grupo::find($id));
  }

  public function update(Request $request, $id){
    $idG = Grupo::findOrFail($id);
    $idG->update($request->all());
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->grupo_id      = $id;
    $log->desc          = 'Alterado, GRUPO';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('grupo.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function apaga($id){
    Grupo::where('id_grupo',$id)->update(['status' => 0]);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->grupo_id      = $id;
    $log->desc          = 'Excluído, GRUPO';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('grupo.index')->with(['success' => 'Excluido com sucesso.']);
  }

  public function destroy($id){
    //
  }
}
