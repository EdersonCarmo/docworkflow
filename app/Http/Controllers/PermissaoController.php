<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Permissao;
use Soged\Usuario;
use Soged\Grupo;
use Soged\Pasta;
use Soged\Arquivo;
use Soged\Documento;

class PermissaoController extends Controller
{
	private $arvore_pastas = [];
	private $arvore_pastas_objeto = [];
	private $variavel_global = 0;

// RETORNA VALOR DO CRUD
	public function crud($crud){ $soma = 0;
		if(isset($crud['crud'])) {
			if($crud['crud'] < 100) return $crud['crud'];
			elseif($crud['crud'] < 200) return $crud['crud'] - 100;
			elseif($crud['crud'] < 300) return $crud['crud'] - 200;
			elseif($crud['crud'] < 400) return $crud['crud'] - 300;
		}

		if(isset($crud['c'])) $soma = $soma + $crud['c'];
		if(isset($crud['r'])) $soma = $soma + $crud['r'];
		if(isset($crud['u'])) $soma = $soma + $crud['u'];
		if(isset($crud['d'])) $soma = $soma + $crud['d'];
		return $soma;
	}
// RETORNA VALOR DO UPDN
	public function updn($updn){ $soma = 0;
		if(isset($crud['crud'])) {
			if($crud['crud'] < 100) return 0;
			elseif($crud['crud'] < 200) return 1;
			elseif($crud['crud'] < 300) return 2;
			elseif($crud['crud'] < 400) return 3;
		}

		if(isset($updn['dn'])) $soma = $soma + $updn['dn'];
		if(isset($updn['up'])) $soma = $soma + $updn['up'];
		if(isset($updn['p']))  $soma = $soma + $updn['p'];
		return $soma;
	}
// RETORNA USUÁRIOS SELECIONADOS
	public function usuariosP($dados){
		$data   = (object) $dados;
		$idUser = array();

		if(!isset($data->grupo_id) AND !isset($data->user_id))
			return back()->withInput()->with(['danger' => 'Selecione um usuário ou grupo.']);

		if(isset($data->grupo_id))
			foreach ($data->grupo_id as $key){
				$idG  = Usuario::select('id')
				->where('grupo_id','=', $key, 'or')
				->get();

				foreach ($idG as $item)
					array_push($idUser, $item->id);
			}

			if(isset($data->user_id))
				foreach ($data->user_id as $item)
					array_push($idUser, $item);
				return $idUser;
			}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|			BUSCA PASTAS RAIZ			  	|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
public function pastaRaiz($id){
	$nm     = \Auth::user()->empresa_id.'p'.$id;

	if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0){
		return false;
	}

	$idP    = Pasta::select('id_pasta')->where('pasta',$nm)->get();
	return $idP[0]->id_pasta;
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|	BUSCA PASTAS RAIZ COMPARTILHADA	 		|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
public function pastaRaizCompartilhada($id){
	$nm     = \Auth::user()->empresa_id.'c'.$id;

	if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0){
		return false;
	}


	$idP    = Pasta::select('id_pasta')->where('pasta',$nm)->get();
	return $idP[0]->id_pasta;
}

// ===================================================================
public $pastaB = array();
public $arquiB = array();
public $documB = array();


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|	BUSCA PASTAS E ARQUIVOS EM ÁRVORE 		|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
public function buscaPasta($id){

	/*ADD id RAIZ*/
	if(count($this->pastaB) == 0){ 
		array_push($this->pastaB, ['id' => $id, 'ref_pai' => null]);

		/*BUSCA ARQUIVOS NA PASTA RAIZ*/
		$a = Arquivo::Active()->where('pasta_id',$id)->get();
		if(count($a) > 0){
			foreach ($a as $item){
				array_push($this->arquiB, ['id' => $item->id_arquivo, 'pasta_id' => $item->pasta_id]);
			}
		}
		/*BUSCA ARQUIVOS NA PASTA RAIZ*/
		/*BUSCA DOCUMENTOS NA PASTA RAIZ*/
		$d = Documento::select()
		->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
		->where('P.user_id', \Auth::id())
		->where('P.pasta_id',$id)
		->get();

		if(count($d) > 0){
			foreach ($d as $item){
				array_push($this->documB, ['id' => $item->id_documento, 'pasta_id' => $item->pasta_id]);
			}
		}
		/*BUSCA DOCUMENTOS NA PASTA RAIZ*/
	}
	/*BUSCA SUBPASTAS*/
	$p = Pasta::Active()->where('ref_pai',$id)->get();
	foreach ($p as $key) {
		array_push($this->pastaB, ['id' => $key->id_pasta, 'ref_pai' => $key->ref_pai]);
		/*BUSCA SUBPASTAS*/
		/*BUSCA ARQUIVOS SUBPASTA*/
		$a = Arquivo::Active()->where('pasta_id',$key->id_pasta)->get();
		if(count($a) > 0)
			foreach ($a as $item)
				array_push($this->arquiB, ['id' => $item->id_arquivo, 'pasta_id' => $item->pasta_id]);
			/*BUSCA ARQUIVOS SUBPASTA*/
			/*BUSCA DOCUMENTOS SUBPASTA*/
			$d = Documento::select()
			->join('sog_permissao as P', 'id_documento', '=', 'P.documento_id')
			->where('P.user_id', \Auth::id())
			->where('pasta_id',$key->id_pasta)
			->get();

			if(count($d) > 0)
				foreach ($d as $item)
					array_push($this->documB, ['id' => $item->id_documento, 'pasta_id' => $item->pasta_id]);
				/*BUSCA DOCUMENTOS SUBPASTA*/

				$this->buscaPasta($key->id_pasta);
			}
		}


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		APAGA PERMISSÕES ARQUIVOS 			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
public function apagaPermissaoArquivo($id_arquivo, $user_id){

	$permissao_arquivo = Permissao::where('user_id',$user_id)
	->where('arquivo_id',$id_arquivo)
	->delete();

}


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		APAGA PERMISSÕES DOCUMENTO 			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
public function apagaPermissaoDocumento($id_documento, $user_id){

	$permissao_arquivo = Permissao::where('user_id',$user_id)
	->where('documento_id',$id_documento)
	->delete();

}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|			APAGA PERMISSÕES PASTA 			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function apagaPermissaoPasta($id_pasta,$user_id){

	$permissao_pasta = Permissao::where('user_id',$user_id)
	->where('pasta_id',$id_pasta)
	->delete();

}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		MUDA STATUS DA PASTA				|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function mudaStatusPasta($id_pasta){
	$pasta = Pasta::where('id_pasta',$id_pasta)->first();
	$pasta->status = 2;
	$pasta->save();
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		CRIA PERMISSÕES PASTA				|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function permitePasta($dados){

	$data = (object) $dados;

	/*FUNC SELECIONA TODAS AS PASTAS*/
	$this->pastaRecursiva($data->pasta_id);

	$arquivos = Arquivo::whereIn('pasta_id',$this->arvore_pastas)
	->where('status',1)
	->get();

	$documentos = Documento::whereIn('pasta_id',$this->arvore_pastas)
	->where('status',1)
	->get();

	$crud = 0;

	foreach ($data->crud as $key) {
		$crud += $key;
	}

	foreach ($data->user_id as $user_id) {

		/*FUNC DEL PERMISSÕES PASTA*/
		$this->apagaPermissaoPasta($data->pasta_id,$user_id);

		$pasta_raiz = $this->pastaRaizCompartilhada($user_id);

		$permissao_pasta = Permissao::create([
			'crud' => $crud,
			'user_id' => $user_id,
			'pasta_id' => $data->pasta_id,
			'ref_pai' => $pasta_raiz
		]);

		foreach ($this->arvore_pastas_objeto as $pastas_id) {
			$this->permitePastaFilha($pastas_id->id_pasta,$user_id,$crud,$pastas_id->ref_pai);
		}

		foreach ($arquivos as $item) {
			$this->permiteArq($item->id_arquivo,$user_id,$crud,$item->pasta_id);
		}

		foreach ($documentos as $item) {
			$this->permiteDoc($item->id_documento,$user_id,$crud,$item->pasta_id);
		}

	}

	return back()->withInput()->with(['success' => 'Compartilhado com sucesso.']);
}


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		FUNÇÃO RECURSIVA DE PASTAS			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
public function pastaRecursiva($id_pasta){

	/*FUNC MUDA STATUS PASTA*/
	$this->mudaStatusPasta($id_pasta);

	$this->arvore_pastas[] = (int) $id_pasta;

	$pasta = Pasta::where('ref_pai',$id_pasta)->where('status','>',0)->get();

	if(sizeof($pasta) > 0){

		foreach ($pasta as $key) {
			$this->arvore_pastas_objeto[] = $key;
			$this->pastaRecursiva($key->id_pasta);
		}

	}
	else{
		return false;
	}

}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		CRIA PERMISSÕES PASTAS POR ID		|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function permitePastaFilha($id_pasta,$user_id,$crud,$ref_pai){

	/*FUNC DEL PERMISSÕES PASTA*/
	$this->apagaPermissaoPasta($id_pasta,$user_id);

	$permissao_pasta = Permissao::create([
		'crud' => $crud,
		'user_id' => $user_id,
		'pasta_id' => $id_pasta,
		'ref_pai' => $ref_pai
	]);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		CRIA PERMISSÕES ARQUIVOS			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function permiteArq($id_arquivo,$user_id,$crud,$ref_pai){

	/*FUNC DEL PERMISSÕES ARQ*/
	$this->apagaPermissaoArquivo($id_arquivo,$user_id);

	$permissao_arquivo = Permissao::create([
		'crud' => $crud,
		'user_id' => $user_id,
		'arquivo_id' => $id_arquivo,
		'ref_pai' => $ref_pai
	]);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		CRIA PERMISSÕES DOCUMENTOS			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function permiteDoc($id_documento,$user_id,$crud,$ref_pai){

	/*FUNC DEL PERMISSÕES ARQ*/
	$this->apagaPermissaoDocumento($id_documento,$user_id);

	$permissao_arquivo = Permissao::create([
		'crud' => $crud,
		'user_id' => $user_id,
		'documento_id' => $id_documento,
		'ref_pai' => $ref_pai
	]);
}

public function show($id){
	$p = Permissao::findOrFail($id);

	if($p->arquivo_id != null){
		$permissao = Permissao::where('arquivo_id',$p->arquivo_id)
		->join('users AS U','U.id','=','user_id')
		->get();
	}elseif ($p->documento_id != null) {
		$permissao = Permissao::where('documento_id',$p->documento_id)
		->join('users AS U','U.id','=','user_id')
		->get();
	}elseif ($p->ref_pai != null) {
		$permissao = Permissao::where('pasta_id',$p->pasta_id)
		->join('users AS U','U.id','=','user_id')
		->get();
	}

	return $permissao;

}

public function mostrar(Request $request){
	$data   = $request->all();
	if ($data['arquivo_id'] == NULL)
		$permi  = Permissao::where('pasta_id',$data['pasta_id'])->where('user_id',$data['user_id'])->count();
	else
		$permi  = Permissao::where('arquivo_id',$data['arquivo_id'])->where('user_id',$data['user_id'])->count();


	if($permi == 0) {
		$nm   = \Auth::user()->empresa_id.'p'.$data['user_id'];
		if(Pasta::select('id_pasta')->where('pasta',$nm)->count() == 0)
			return redirect()->route('pasta.index')->with(['danger' => 'Usuário não possui pasta.']);

			$idP  = Pasta::select('id_pasta')->where('pasta',$nm)->get();

			if($data['arquivo_id'] != NULL){
				$idPer = Permissao::create([
					'arquivo_id'=> $data['arquivo_id'],
					'pasta_id'  => $idP[0]->id_pasta,
					'crud'      => 1,
					'updown'    => 0,
					'user_id'   => $data['user_id'],
					'sts'       => 2
				])->id_permissao;
				$permissao  = Permissao::find($idPer);
			}else {
				$idPer = Permissao::create([
					'pasta_id'  => $data['pasta_id'],
					'crud'      => 1,
					'updown'    => 0,
					'user_id'   => $data['user_id'],
					'ref_pai'   => $idP[0]->id_pasta,
					'sts'       => 2
				])->id_permissao;
				$permissao  = Permissao::find($idPer);
			}
		}else {
			if($data['arquivo_id'] != NULL){
				$idpermi      = Permissao::where('arquivo_id',$data['arquivo_id'])->where('user_id',$data['user_id'])->get();
				$permissao    = Permissao::find($idpermi[0]->id_permissao);
			}else {
				$idpermi      = Permissao::where('pasta_id',$data['pasta_id'])->where('user_id',$data['user_id'])->get();
				$permissao    = Permissao::find($idpermi[0]->id_permissao);
			}
		}

		$grupos           = Grupo::EmpAct()->get();
		$usuarios         = Usuario::EmpAct()->get();
		return view('doc.permissao', compact('permissao','grupos','usuarios'));
	}

	public function compartilhar($id){
		$grupos       = Grupo::EmpAct()->get();
		$usuarios     = Usuario::EmpAct()->get();
		$permissao    = Permissao::find($id);

		if($permissao->arquivo_id != NULL){
			$userComp   = Usuario::EmpAct()
			->join('sog_permissao as P', 'id', '=', 'P.user_id')
			->where('P.arquivo_id',$permissao->arquivo_id)
			->distinct('users.name')
			->get();
		}else {
			$userComp   = Usuario::EmpAct()
			->join('sog_permissao as P', 'id', '=', 'P.user_id')
			->where('P.pasta_id',$permissao->pasta_id)
			->where('P.arquivo_id',NULL)
			->get();
		}
		return view('doc.compartilhar', compact('grupos','usuarios','permissao','userComp'));
	}
//--------------------------------------------------------------------------


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
|										  	|
|		COMPARTILHAR DOC/ARQ/PASTA			|
|											|
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

public function compStore(Request $request){
	$data = (object) $request->all();


	/*CRIA PERMISSÂO PARA DOCUMENTOS*/
	if(isset($data->documento_id)){

		$crud = 0;

		foreach ($request->crud as $key) {
			$crud += $key;
		}

		foreach ($request->user_id as $key) {
			$pasta_raiz = $this->pastaRaizCompartilhada($key);

			$this->permiteDoc($request->documento_id,$key,$crud,$pasta_raiz);
		}

	}

	/*CRIA PERMISSÂO PARA ARQUIVOS*/
	elseif(isset($data->arquivo_id)){

		$crud = 0;

		foreach ($request->crud as $key) {
			$crud += $key;
		}

		foreach ($request->user_id as $key) {
			$pasta_raiz = $this->pastaRaizCompartilhada($key);

			$this->permiteArq($request->arquivo_id,$key,$crud,$pasta_raiz);
		}

	}

	/*CRIA PERMISSAO PARA PASTAS*/
	else                          
		$this->permitePasta($request->all());

	return back()->withInput()->with(['success' => 'Compartilhado com sucesso.']);
}
//---------------------------------------------------------------------------
public function edit($id){
//
}
public function update(Request $request){


if(isset($request->all()['acao']) == 'MOVER'){ // PARA MOVER DE PASTA

	if($request->id_arquivo != 0){

		$permissao = Permissao::where('arquivo_id',$request->id_arquivo)
		->where('user_id',\Auth::user()->id)
		->first();

		$permissao->pasta_id = $request->pasta_id_ref;

		$permissao->save();

	}
	else if($request->id_documento != 0){

		$permissao = Permissao::where('documento_id',$request->id_documento)
		->where('user_id',\Auth::user()->id)
		->first();

		$permissao->pasta_id = $request->pasta_id_ref;

		$permissao->save();

	}
	else if($request->id_pasta != 0){

		$permissao = Permissao::where('pasta_id',$request->id_pasta)
		->where('user_id',\Auth::user()->id)
		->whereNotNull('ref_pai')
		->first();

		$permissao->ref_pai = $request->pasta_id_ref;

		$permissao->save();

	}


}

else{
	if($this->crud($request->all()) != 0){
		$idP            = Permissao::find($request->all()['id_permissao']);
		$idP->crud    = $this->crud($request->all());
		$idP->updown  = $this->updn($request->all());
		$idP->save();
	}else {
		$idp->delete();
		return redirect()->route('pasta.index');
	}

	$permissao      = Permissao::find($request->all()['id_permissao']);
	$grupos         = Grupo::EmpAct()->get();
	$usuarios       = Usuario::EmpAct()->get();

	/*$log                = new Log;
	$log->id_empresa    = \Auth::user()->empresa_id;
	$log->id_user       = \Auth::id();
	$log->permissao_id  = $idP->id_permissao;
	$log->desc          = 'Alterado, COMPARTILHADO';
	$log->save();*/

	return view('doc.permissao', compact('permissao','grupos','usuarios'))->with(['success' => 'Alterado com sucesso.']);
}


}
}
