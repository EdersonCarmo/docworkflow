<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Illuminate\Http\UploadedFile;
use Soged\Endereco;
use Soged\Empresa;

class PerfilEmpController extends Controller
{
  public function index(){
    return view('empresa.perfilEmp')->with('empresa', Empresa::find(\Auth::user()->empresa_id));
  }

  public static function SalvarImgS3(Request $request){
    $data       = $request->all();
    $img        = $request->file('image');
    $pasta      = 'empresas/fotos/'; //dd($img);
    $filename   = $pasta.$data['id_emp'].'.'.$img->getClientOriginalExtension();

    Storage::disk('s3')->delete($filename);
    $s3         = Storage::disk('s3')->put($pasta, $img, 'public');
    Storage::disk('s3')->move($s3, $filename);
    Storage::disk('s3')->delete($s3);
    // 'public' = Colocar isso caso o arquivo seja acessado de outros lugares

    $idE        = Empresa::find($data['id_emp']);
    $idE->foto  = $filename;
    $idE->update();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->empresa_id    = $idU->id_empresa;
    $log->desc          = 'Alterado, IMG.S3';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return  back()->withInput();
  }

  public function create(){
    //
  }

  public function store(Request $request){
    //
  }

  public function show($id){
    //
  }

  public function edit($id){
    //
  }

  public function update(Request $request, $id){
    $idEmp = Empresa::findOrFail($id);
    $idEnd = Endereco::findOrFail($idEmp->endereco_id);

    $idEmp->update($request->all());
    $idEnd->update($request->all());
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->empresa_id    = $id;
    $log->desc          = 'Alterado, EMP + END';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return  back()->withInput();
  }

  public function destroy($id){
    //
  }
}
