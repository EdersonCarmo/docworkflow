<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\User;
use Soged\Endereco;
use Storage;
use Illuminate\Http\UploadedFile;

class PerfilController extends Controller
{
  public function index(){
    $usuario    = User::find(\Auth::id());
    $endereco   = Endereco::find($usuario->endereco_id);
    return view('user.perfilUsu')->with('usuario', $usuario)->with('endereco', $endereco);
  }

  public static function SalvarImgS3(Request $request){
    $data       = $request->all();
    $img        = $request->file('image');
    $pasta      = 'empresas/'.\Auth::user()->empresa_id.'/users/'; //dd($img);
    $filename   = $pasta.$data['id_user'].'.'.$img->getClientOriginalExtension();

    Storage::disk('s3')->delete($filename);
    $s3         = Storage::disk('s3')->put($pasta, $img, 'public');
    Storage::disk('s3')->move($s3, $filename);
    Storage::disk('s3')->delete($s3);
    // 'public' = Colocar isso caso o arquivo seja acessado de outros lugares

    $idU        = User::find($data['id_user']);
    $idU->foto  = $filename;
    $idU->update();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->user_id       = $idU->id;
    $log->desc          = 'Alterado, IMG.S3';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return  back()->withInput();
  }

  public function create(){
    //
  }

  public function store(Request $request){
    //
  }

  public function show($id){
    //
  }

  public function edit($id){
  }

  public function update(Request $request, $id){
    $user       = $request->all();            //dd($user);
    $idU        = User::findOrFail($id);

    if($user['alt_pass']==1){
      $user['password'] = bcrypt($user['password']);
      $idU->update($user);
      $logDesc  = 'PASS';
    }else {//dd($user);
      $idE      = Endereco::findOrFail($user['endereco_id']);
      $idE->update($user);
      $idU->celular = $user['celular'];
      $idU->update($user);
      $logDesc  = 'USER + END';
    }
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->user_id       = $id;
    $log->desc          = 'Alterado, '.$logDesc;
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return  back()->withInput();
  }

  public function destroy($id){
    //
  }
}
