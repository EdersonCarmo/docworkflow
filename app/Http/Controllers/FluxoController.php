<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Usuario;
use Soged\Grupo;
use Soged\Cliente;
use Soged\Fluxo;
use Soged\Log;

class FluxoController extends Controller
{  // RETORNA USUÁRIOS SELECIONADOS
  public function usuariosP($dados){
    $data   = (object) $dados;
    $idUser = array();

    if(!isset($data->grupo_id) AND !isset($data->user_id))
    return back()->withInput()->with(['danger' => 'Selecione um usuário ou grupo.']);

    if(isset($data->grupo_id))
    foreach ($data->grupo_id as $key){
      $idG  = Usuario::select('id')
      ->where('grupo_id','=', $key, 'or')
      ->get();

      foreach ($idG as $item)
      array_push($idUser, $item->id);
    }

    if(isset($data->user_id))
    foreach ($data->user_id as $item)
    array_push($idUser, $item);
    return $idUser;
  }
  //-------------------------------------------------------------------------
  public function index()  {
    $fluxos = Fluxo::all();
    return view('doc.fluxos', compact('fluxos'));
  }

  public function create()  {
    //
  }

  public function store(Request $request)  {
    $data   = $request->all(); //dd($data);
    $users  = $this->usuariosP($data);
    $ordem  = 1;

    $cont   = Fluxo::where('documento_id', $data['documento_id'])->count();
    if($cont > 0) $ordem = Fluxo::where('documento_id', $data['documento_id'])->max('ordem');

    foreach ($users as $key) {
      if($data['tipo'] == 2) $key = 1;
      Fluxo::create([
        'fluxo'         =>  $data['fluxo'],
        'tipo'          =>  $data['tipo'],
        'documento_id'  =>  $data['documento_id'],
        'ordem'         =>  $ordem + 1,
        'user_id'       =>  $key,
        'email'         =>  $data['email']
      ]);
    }

    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->desc          = 'Criado, FLUXO';
    $log->documento_id  = $data['documento_id'];
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('fluxo.show',$data['documento_id']);
  }

  public function show($id)  {
    $grupos       = Grupo::EmpAct()->get();
    $usuarios     = Usuario::EmpAct()->get();
    $clientes     = Cliente::EmpAct()->get();
    $fluxos       = Fluxo::where('documento_id', $id)->groupBy('ordem')->get();
    $idDoc        = $id;

    return view('doc.fluxo', compact('idDoc','grupos','usuarios','fluxos','clientes'));
  }

  public function edit($id)  {
    //
  }

  public function update(Request $request, $id)  {
    $data   = $request->all(); $teste= '>';
    $ordem  = 2;
    $idF    = array();

    foreach ($data['teste'] as $key) {
      $f = Fluxo::where('ordem', $key)
      ->where('documento_id', '=', $data['documento_id'], 'and')
      ->get();
      foreach ($f as $keyF) {
        array_push($idF, ['id' => $keyF->id_fluxo, 'ordem' => $ordem]);
      }
      $ordem++;
    }
    foreach ($idF as $key) {
      Fluxo::where('id_fluxo', $key['id'])
      ->update(['ordem' => $key['ordem']]);
    }

    return redirect()->route('fluxo.show',$data['documento_id']);
  }

  public function destroy($id)  {
    //
  }

  public function apaga($id){
    $idF = Fluxo::findOrFail($id);

    Fluxo::where('documento_id', $idF->documento_id)
    ->where('ordem','=',$idF->ordem,'and')
    ->delete();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->desc          = 'Excluído, FLUXO';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return back()->withInput();
  }
}
