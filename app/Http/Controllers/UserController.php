<?php
namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Soged\Http\Requests\StoreUsuario;
use Illuminate\Support\Facades\Hash;
use Soged\User;
use Soged\Usuario;
use Soged\Grupo;
use Soged\Empresa;
use Soged\Pasta;
use Storage;

class UserController extends Controller
{
  public function __construct(){
    $this->middleware('auth');
  }

  public static function TratarImagem($imagem){
    $imagem_explode = explode(",", $imagem);
    $imagem_decode  = base64_decode($imagem_explode[1]);

    return $imagem_decode;
  }

  public function index(){

    $usu      = Usuario::EmpAct()->get();
    if(\Auth::user()->empresa_id == 1){
      $usu      = Usuario::Active()->get();
    }

    return view('user.listUsers')->with('usuarios', $usu,['success' => 'Compartilhado com sucesso.']);
  }

  public function create(){
    $grupos     = Grupo::Active()
    ->where('empresa_id', \Auth::user()->empresa_id)
    ->get();
    $empresas   = Empresa::Active()->get();
    return view('user.usuario', compact('grupos','empresas'));
  }

  protected function validator(array $data){
    return Validator::make($data, [
      'name'        => 'required|string|max:255',
      'email'       => 'required|string|email|max:255|unique:users',
      'password'    => 'required|string|min:6|confirmed',
    ]);
  }

  protected function store(Request $request){

    $hrp            = str_random(8);
    $data           = $request->all();
    $filename = '';

    if($data['foto'] != null) {
      $img          = $this->TratarImagem($data['foto']);
      $pasta        = 'empresas/'.$data['empresa_id'].'/foto_perfil/';
      $filename     = str_random(8).date('ymdhis').'.png';

      Storage::disk('s3')->delete($filename);
      $s3           = Storage::disk('s3')->put($filename,$img, 'public');
    }

    $usu            = Usuario::create([
      'name'        => $data['name'],
      'email'       => $data['email'],
      'password'    => bcrypt($data['password']),
      'grupo_id'    => $data['grupo_id'],
      'empresa_id'  => $data['empresa_id'],
      'foto'        => $filename
    ]);

    Pasta::create(['pasta' => $usu->empresa_id.'p'.$usu->id,'user_id' => $usu->id]);
    Pasta::create(['pasta' => $usu->empresa_id.'a'.$usu->id,'user_id' => $usu->id]);
    Pasta::create(['pasta' => $usu->empresa_id.'c'.$usu->id,'user_id' => $usu->id]);
    Pasta::create(['pasta' => $usu->empresa_id.'t'.$usu->id,'user_id' => $usu->id]);

    return redirect()->action('UserController@index');

  }

  public function show($id){

    $grupos         = Grupo::Active()->EmpId()->get();
    $empresa        = Empresa::Active()->get();

    return view('user.usuario')->with('grupos', $grupos)->with('empresas',$empresa)->with('empresa',$id);
  }

  public function edit($id){
    $grupos     = Grupo::Active()
    ->where('empresa_id', \Auth::user()->empresa_id)
    ->get();
    $empresa        = Empresa::Active()->get();
    return view('user.usuario')->with('usuario', User::find($id))->with('grupos', $grupos)->with('empresas',$empresa);
  }

  public function update(Request $request, $id){

    $id = User::findOrFail($id);
    $id->name = $request['name'];
    $id->email = $request['email'];
    $id->password = bcrypt($request['password']);
    $id->grupo_id = $request['grupo_id'];
    $id->empresa_id = $request['empresa_id'];

    $id->save();

    return redirect()->route('usuario.index');
  }

  public function apaga($id){
    User::where('id',$id)->update(['status' => 0]);

    return redirect()->route('usuario.index');
  }

  public function trocaSenha(User $id){
    $hrp            = str_random(8);
    $id->password   = bcrypt($hrp);
    $id->save();
    $ee = 'desenvolvimento06soitic@gmail.com';

    return redirect('mailsend/trocaSenha/'.$ee.'/'.$hrp.'/'.$id->name);
  }

  public function RetornarUsuarios(){

    $id_empresa = \Auth::user()->empresa_id;
    $usuarios = User::where('empresa_id',$id_empresa)->get();

    return response()->json($usuarios);
  }

  public function RetornaEmails(){
    $usuarios = User::all();

    return response()->json($usuarios);
  }
}
