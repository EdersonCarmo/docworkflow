<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Http\Controllers\Controller;

class SessionController extends Controller
{
   public function getSession(Request $request, $nome) {
      if($request->session()->has($nome))
         echo $request->session()->get($nome);
      else
         echo 'No data in the session';
   }
   public function putSession(Request $request, $nome, $valor) {
      $request->session()->put($nome, $valor);
      echo "Data has been added to session";
   }
   public function forgetSession(Request $request) {
      $request->session()->forget('my_name');
      echo "Data has been removed from session.";
   }
}
