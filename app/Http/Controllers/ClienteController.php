<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Http\Requests\StoreCliente;
use Soged\Cliente;
use Soged\Endereco;
use Soged\Log;

class ClienteController extends Controller
{
  public function index(){
    $clientes = Cliente::Active()->EmpId()->get();
    return view('cliente.listCliente')->with('clientes', $clientes);
  }

  public function create(){
    return view('cliente.cliente');
  }

  public function store(Request $request){ 
    $end    = Endereco::create($request->all());
    $dados  = $request->all();
    $dados['endereco_id'] = $end->id_endereco;
    $dados['empresa_id']  = \Auth::user()->empresa_id;
    $idD = Cliente::create($dados)->id_cliente;
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->cliente_id    = $idD;
    $log->desc          = 'Criado, CLIENTE';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('cliente.index')->withInput()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function show($id){
    //
  }

  public function edit($id){
    return view('cliente.cliente')->with('cliente', Cliente::find($id));
  }

  public function update(Request $request, $id){
    $cli    = $request->all();
    $id     = Cliente::findOrFail($id);
    $ide    = Endereco::findOrFail($id['endereco_id']);

    $ide->update($cli);
    $id->update($cli);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->cliente_id    = $id->id_cliente;
    $log->desc          = 'Alterado, CLIENTE';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('cliente.index')->withInput()->with(['success' => 'Alterado com sucesso.']);
  }

  public function apaga($id){
    Cliente::where('id_cliente',$id)->update(['status' => 0]);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->cliente_id    = $id;
    $log->desc          = 'Excluído, CLIENTE';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('cliente.index')->with(['success' => 'Excluido com sucesso.']);
  }

  public function destroy($id){
    //
  }
}
