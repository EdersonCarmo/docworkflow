<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;

class CepController extends Controller
{
  public function consultaCEP(){ //dd('stop');
    $cep = $_POST['cep'];

    $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);

    $dados['sucesso'] = (string) $reg->resultado;
    $dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
    $dados['bairro']  = (string) $reg->bairro;
    $dados['cidade']  = (string) $reg->cidade;
    $dados['estado']  = (string) $reg->uf;

    //echo json_encode($dados);
    return response()->json($dados);
  }
}
