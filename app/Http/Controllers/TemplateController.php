<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Soged\Template;
use Soged\Log;

class TemplateController extends Controller
{
  public function index()  {
    $templates = Template::EmpAct()->get(); //dd($templates);
    return view('template.listTemplate', compact('templates'));
  }

  public function create()  {
    return view('template.template');
  }

  public function store(Request $request)  { 
    $idT = Template::create($request->all())->id_template;
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->template_id   = $idT;
    $log->desc          = 'Criado, TEMPLATE';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('template.index')->with(['success' => 'Criado com sucesso.']);
  }

  public function show($id)  {
    //
  }

  public function edit($id)  {
    return view('template.template')->with('template', Template::find($id));
  }

  public function update(Request $request, $id)  {
    $idT = Template::findOrFail($id);
    $idT->update($request->all());
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->template_id   = $id;
    $log->desc          = 'Alterado, COMPARTILHADO';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('template.index')->with(['success' => 'Criado com sucesso.']);
  }

  public function apaga($id){
    Template::where('id_template',$id)->update(['status' => 0]);
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    $log                = new Log;
    $log->id_empresa    = \Auth::user()->empresa_id;
    $log->id_user       = \Auth::id();
    $log->template_id   = $id;
    $log->desc          = 'Excluído, TEMPLATE';
    $log->save();
    // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    return redirect()->route('template.index')->with(['success' => 'Excluido com sucesso.']);
  }

  public function destroy($id)  {
    //
  }
}
