<?php

namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Soged\MailModel;
use Soged\Mail\SendMail;
use Soged\Log;
use Soged\Documento;
use Storage;
use PDF;

class MailController extends Controller
{
  public function SendEmail(){
    $data = new MailModel;

    $data->nome       = 'teste22';
    $data->email      = 'teste@teste.com';
    $data->telefone   = '12345678';
    $data->assunto    = 'teste11';
    $data->mensagem   = 'teste teste';
    $data->titulo     = 'CONFIRMAÇÃO DE CADASTRO - SOGED';
    $data->info       = 'Parabéns. Você foi cadastrado para ter acesso ao site do SOGED.
    <p> Acesse através do link abaixo, utilize seu endereço de e-mail e a senha provisória para entrar.
    </p>';

    Mail::to('desenvolvimento06soitic@gmail.com')->send(new SendMail($data));
  }

  public function SendEmailCad($email, $senha, $empresa){
    $data = new MailModel;

    $data->nome       = 'https://www.soged.com.br/';
    $data->email      = $email;
    $data->titulo     = 'CONFIRMAÇÃO DE CADASTRO - SOGED';
    $data->info       = 'Parabéns. Você foi cadastrado para ter acesso ao site do SOGED.
    Acesse através do link abaixo, utilize seu endereço de e-mail e a senha provisória para entrar.
    ';
    $data->Senha      = $senha;
    $data->empresa    = $empresa;
    $data->envio      = 'cadastro';

    if(Mail::to($email)->send(new SendMail($data))){
      return redirect()->route('usuario.index')->with(['success' => 'E-Mail foi enviado para o usuário.']);
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
      $log                = new Log;
      $log->id_empresa    = \Auth::user()->empresa_id;
      $log->id_user       = \Auth::id();
      $log->desc          = 'Envio, E-MAIL CAD'.$email;
      $log->save();
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    }else
        return redirect()->route('usuario.index')->with(['danger' => 'E-Mail não foi enviado.']);
  }

  public function trocaSenha($email, $senha, $nome){
    $data = new MailModel;

    $data->titulo     = 'RECUPERAÇÃO DE SENHA - SOGED';
    $data->info       = 'Foi solicitado a recuperação de senha para acesso ao SOGED.
    Acesse através do link abaixo, utilize seu endereço de e-mail e a senha provisória para entrar.
    ';
    $data->Senha      = $senha;
    $data->empresa    = $nome;
    $data->envio      = 'cadastro';

    if(Mail::to($email)->send(new SendMail($data))){
      return redirect()->route('usuario.index')->with(['success' => 'E-Mail foi enviado para o usuário.']);
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
      $log                = new Log;
      $log->id_empresa    = \Auth::user()->empresa_id;
      $log->id_user       = \Auth::id();
      $log->desc          = 'Renvio, E-MAIL SENHA'.$email;
      $log->save();
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
    }else
        return redirect()->route('usuario.index')->with(['danger' => 'E-Mail não foi enviado.']);
  }

  public function fluxo($id, $email){
    $idD      = Documento::findOrFail($id);
    $texto    = $idD->texto;
    $pdf      = PDF::loadView('doc.pdf', ['texto' => $texto]);
    $pdftemp  = 'empresas/'.\Auth::user()->empresa_id.'/'.$idD->documento.'.pdf';
    //file_put_contents($pdftemp, $pdf->output());
    $s3 = Storage::disk('s3')->put($pdftemp, $pdf->output(), 'public');
    //$tes = Storage::disk('public')->put('teste.pdf', $pdf->output()); // SALVA NA PASTA
    $data = new MailModel;

    $data->titulo     = 'NOTIFICAÇÂO DE FLUXO - SOGED';
    $data->info       = 'Exite um alerta aguadando interação no SOGED.
    Acesse através do link abaixo, utilize seu endereço de e-mail e a senha provisória para entrar.
    ';
    //$data->empresa    = $empresa;
    $data->envio      = 'fluxo';
    $data->assunto    = 'SOGED';
    $data->anexo      = 'https://s3.us-east-1.amazonaws.com/soged-arq/'.$pdftemp;

    if(Mail::to($email)->send(new SendMail($data)) == NULL){
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
      $log                = new Log;
      $log->id_empresa    = \Auth::user()->empresa_id;
      $log->id_user       = \Auth::id();
      $log->desc          = 'Fluxo, ENVIO E-MAIL';
      $log->save();
      // REGISTRO DE LOG REGISTRO DE LOG REGISTRO DE LOG //
      Storage::disk('s3')->delete($pdftemp);

      return redirect()->action('DocumentoController@fluxoAcao', $id);
      //return back()->withInput(); //redirect()->route('pasta.index')->with(['success' => 'E-Mail foi enviado para o usuário.']);
    }//else{
      //return redirect()->route('pasta.index')->with(['danger' => 'E-Mail não foi enviado.']);
    //}
  }
}
