<?php namespace Soged\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Soged\MolduraM;
use Storage;

class MolduraC extends Controller
{
  public static function TratarImagem($imagem){
    $imagem_explode = explode(",", $imagem);
    $imagem_decode  = base64_decode($imagem_explode[1]);

    return $imagem_decode;
  }

  public function index()  {
    $molduras = MolduraM::select('*','sog_molduras.created_at AS criado')
              ->join('users as U', 'U.id','user_id')
              ->where('U.empresa_id', \Auth::user()->empresa_id)
              ->where('sog_molduras.status', 1)
              ->get();

    return view('template.listMoldura', compact('molduras'));
  }

  public function create()  {
    return view('template.moldura');
  }

  public function store(Request $data)  {
    $idM          = new MolduraM;

    if($data['fundo'] != null) {
      $img          = $this->TratarImagem($data['fundo']);
      $pasta        = 'empresas/'.\Auth::user()->empresa_id.'/molduras/';
      $filename     = $pasta.date('ymdhis').'.png';

      Storage::disk('s3')->delete($filename);
      $s3           = Storage::disk('s3')->put($filename,$img, 'public');
      $idM->fundo     = $filename;
    }

    $idM->moldura   = $data['moldura'];
    if($data['cabecalho'] != null) $idM->cabecalho = $data['cabecalho'];
    if($data['rodape'] != null) $idM->rodape    = $data['rodape'];
    $idM->user_id   = \Auth::id();
    $idM->save();

    return redirect()->route('moldura.index')->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function show($id)  {
    //
  }

  public function edit($id)  {
    return view('template.moldura', ['moldura'=>MolduraM::findOrFail($id)]);
  }

  public function update(Request $data, $id)  {
    $idM = MolduraM::findOrFail($id);

    if($data['fundo'] != null){
      $img          = $this->TratarImagem($data['fundo']);
      $pasta        = 'empresas/'.\Auth::user()->empresa_id.'/molduras/';
      $filename     = $pasta.date('ymdhis').'.png';

      Storage::disk('s3')->delete($filename);
      $s3           = Storage::disk('s3')->put($filename,$img, 'public');
      $idM->fundo   = $filename;
    }

    $idM->moldura = $data['moldura'];
    if($data['cabecalho'] != null) $idM->cabecalho = $data['cabecalho'];
    if($data['rodape'] != null) $idM->rodape = $data['rodape'];
    $idM->save();

    return redirect()->route('moldura.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function apaga($id)  {
    MolduraM::where('id_moldura',$id)->update(['status'=>0]);

    return redirect()->route('moldura.index')->with(['success' => 'Excluído com sucesso.']);
  }

  public function destroy($id)  {
    //
  }
}
