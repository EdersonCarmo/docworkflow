<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Pasta extends Scope
{
  use Notifiable;

  protected $table = 'sog_pastas';
  protected $fillable = ['pasta', 'ref_pai', 'user_id','status'];
  protected $primaryKey = 'id_pasta';
}
