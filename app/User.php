<?php

namespace Soged;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;

  protected $fillable = [
    'empresa_id','name', 'email', 'password','foto'
    ,'telefone', 'endereco_id', 'celular','grupo_id'
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];

  public function grupo(){
    return $this->belongsTo('Soged\Grupo', 'grupo_id');
  }
}
