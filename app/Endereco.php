<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Endereco extends Scope
{
  use Notifiable;
  protected $table = 'sog_enderecos';
  protected $fillable = [
    'logradouro', 'numero','complemento','bairro','cidade','uf','cep'];
  protected $primaryKey = 'id_endereco';
}
