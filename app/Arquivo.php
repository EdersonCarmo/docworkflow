<?php

namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Arquivo extends Scope
{
  use Notifiable;
  protected $table = 'sog_arquivos';
  protected $fillable = ['arquivo', 'local','pasta_id','status','size'];
  protected $primaryKey = 'id_arquivo';

  public function pasta(){
    return $this->belongsTo('Soged\Pastas', 'pasta_id');
  }

  public function permissao(){
    return $this->hasMany('Soged\Permissao', 'arquivo_id');
  }
}
