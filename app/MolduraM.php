<?php namespace Soged;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class MolduraM extends Scope
{
  use Notifiable;
  protected $table = 'sog_molduras';
  protected $fillable = ['moldura','cabecalho','rodape','fundo','status','user_id'];
  protected $primaryKey = 'id_moldura';
}
