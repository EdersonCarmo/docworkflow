<?php

return [

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => 'localhost:8000/storage',
            'visibility' => 'public',
        ],

        /*'s3' => [
            'driver' => 's3',
            'key' => 'AKIAJ3JUUYGOIS7CW4ZA',
            'secret' => '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',
            'region' => 'us-east-1',
            'bucket' => 'soged-arq',
        ],*/

        's3' => [
            'driver' => 's3',
            'key' => 'AKIAJIFZ44323KC44SRQ',
            'secret' => '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',
            'region' => 'us-west-2',
            'bucket' => 'ecos-soged',
        ],

    ],

];
