<?php

return [

    'driver' => 'smtp',

    'host' => 'smtp.kinghost.net',

    'port' =>  465,

    'from' => [
        'address' =>  'no-reply@soitic.com',
        'name' =>  'SOGED',
    ],

    'encryption' =>  'ssl',

    'username' => 'no-reply@soitic.com',

    'password' => 'N0001EMAILy',

    'sendmail' => '/usr/sbin/sendmail -bs',

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];
