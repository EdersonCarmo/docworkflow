<?php

// Route::get('/info', function () { phpinfo(); });

Route::get('/', function () { return view('auth.login'); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/teste', 'HomeController@teste2')->name('teste');
Route::get('all-users','UserController@RetornarUsuarios');
// ------------------------------------------------------------------------
Route::group(['middleware' => ['auth']], function(){
  Route::get('inicial', function () {
    if(Auth::user()->empresa_id == 1) return redirect()->route('empresa.index');
    if(Auth::user()->empresa_id > 1) return redirect()->route('pasta.index');
  });
  // ALERTAS
  Route::resource('alerta', 'AlertaController');
  // PERFIL
  Route::post('/perfil/SalvarImgS3/', 'PerfilController@SalvarImgS3')->name('fotoperfil');
  Route::resource('perfil', 'PerfilController');
  Route::post('/perfilemp/SalvarImgS3/', 'PerfilEmpController@SalvarImgS3')->name('fotoemp');
  Route::resource('perfilemp', 'PerfilEmpController');
  // PERFIS
  Route::get('/perfil_emp', 'HomeController@perfil_emp')->name('perfil_emp');
  Route::get('/perfil_usu', 'HomeController@perfil_usu')->name('perfil_usu');
  // USUARIOS
  Route::resource('usuario', 'UserController');
  Route::get('usuario/apaga/{id}','UserController@apaga')->name('apaga_usuario');
  Route::get('usuario/trocaSenha/{id}','UserController@trocaSenha')->name('trocaSenha_usuario');
  // GRUPOS
  Route::resource('grupo', 'GrupoController');
  Route::get('grupo/apaga/{id}','GrupoController@apaga')->name('apaga_grupo');
  // EMPRESAS
  Route::resource('empresa', 'EmpresaController');
  Route::get('empresa/apaga/{id}','EmpresaController@apaga')->name('apaga_empresa');
  // CLIENTES
  Route::resource('cliente', 'ClienteController');
  Route::get('cliente/apaga/{id}','ClienteController@apaga')->name('apaga_cliente');
  // ARQUIVOS
  Route::resource('arquivo', 'ArquivoController');
  Route::post('arquivo/SalvarArqS3/', 'ArquivoController@SalvarArqS3')->name('arquivos3');
  Route::post('arquivo/SalvarArquivos/', 'ArquivoController@SalvarArquivos')->name('arquivos');
  Route::get('arquivo/apaga/{id}','ArquivoController@apaga')->name('apaga_arquivo');
  Route::get('arquivo/baixar/{id}','ArquivoController@baixar')->name('baixar');
  Route::get('arquivo/salvaPdf/{id}/{end}','ArquivoController@salvaPdf')->name('salvaPdf');
  // PASTAS
  Route::resource('pasta', 'PastaController');
  Route::get('pasta/apaga/{id}','PastaController@apaga')->name('apaga_pasta');
  // PERMISSAO
  Route::resource('permissao', 'PermissaoController');
  Route::post('permissao/mostrar','PermissaoController@mostrar')->name('mostrar_permissao');
  Route::get('permissao/compartilhar/{id}','PermissaoController@compartilhar')->name('compartilhar_permissao');
  Route::post('permissao/compStore/','PermissaoController@compStore')->name('compStore_permissao');
  Route::get('permissao/apagaPermissao/{id}','PermissaoController@apagaPermissao')->name('apagaPermissao');
  // SENDMAIL
  Route::get('mailsend/','MailController@SendEmail')->name('mailsend');
  Route::get('mailsend/cadastrado/{email}/{senha}/{empresa}','MailController@SendEmailCad')->name('user_cadastrado');
  Route::get('mailsend/trocaSenha/{email}/{senha}/{nome}','MailController@trocaSenha')->name('mailTrocaSenha');
  Route::get('mailsend/fluxo/{id}/{email}','MailController@fluxo')->name('mailFluxo');
  // TEMPLATE
  Route::resource('template', 'TemplateController');
  Route::get('template/apaga/{id}','TemplateController@apaga')->name('apaga_template');
  // DOCUMENTO fluxoAcao
  Route::resource('documento', 'DocumentoController');
  Route::get('documento/apaga/{id}','DocumentoController@apaga')->name('apaga_documento');
  Route::put('documento/rename/{id}','DocumentoController@rename')->name('renomear_documento');
  Route::get('documento/createDoc/{pasta}','DocumentoController@createDoc')->name('createDoc');
  Route::get('documento/createTem/{pasta}','DocumentoController@createTem')->name('createTem');
  Route::post('documento/createDocTem/','DocumentoController@createDocTem')->name('createDocTem');
  Route::get('documento/pdf/{id}','DocumentoController@pdf')->name('pdf');
  Route::get('documento/enviaPdf/{id}/{emal}','DocumentoController@enviaPdf');
  Route::get('documento/reabreDoc/{id}','DocumentoController@reabreDoc')->name('reabreDoc');
  Route::post('documento/busca/','DocumentoController@busca')->name('buscar');
  Route::get('documento/show2/{id}/{end?}','DocumentoController@show2')->name('show2');
  Route::get('documento/fluxoAcao/{id}','DocumentoController@fluxoAcao')->name('fluxoAcao');
  // FLUXO
  Route::resource('fluxo', 'FluxoController');
  Route::get('fluxo/apagaFluxo/{id}','FluxoController@apaga')->name('apagaFluxo');
  // CEP
  Route::post('consultaCEP','CepController@consultaCEP')->name('consultaCEP');

  Route::get('session/get/{id}','SessionController@getSession');
  //Route::get('session/put/{id}/{id}','SessionController@putSession');
  Route::get('session/forget','SessionController@forgetSession');

  
});


/* RESOURCE
GET     /photos                  index    name.index
GET     /photos/create           create   name.create
POST    /photos                  store    name.store
GET     /photos/{name}           show     name.show
GET     /photos/{name}/edit      edit     name.edit
PUT     /PATCH  /name/{name}     update   name.update
DELETE  /photos/{name}           destroy  name.destroy
*/
