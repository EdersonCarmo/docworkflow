@extends('layouts.soged')

@section('content')
@php
$uf = 'MG';
if(isset($empresa)){
 $endereco = (object) $empresa->endereco;
 $uf = $endereco->uf;
}
@endphp

<div class="container-fluid">

  <div class="card">

    <div class="mb-2 card-header">

      <div class="p-1">
        <strong>{{ isset($usuario)?"Alterando Empresa":"Cadastrar Empresa"}}</strong>
      </div>

    </div>

    <div class="container">

      <div class="col-12 col-md-8 co-lg-6 my-3">

        @if (isset($empresa))
        <form class="" action="{{route('empresa.update',$empresa->id_empresa)}}" method="post">
          <input type="hidden" name="endereco_id" value="{{$empresa->endereco_id}}">
          {{ method_field('PUT') }}
          @else
          <form class="" action="{{route('empresa.store')}}" method="post">
            @endif
            {{ csrf_field() }}

            <div class="form-group">
              <label for="empresa">Nome</label>
              <input name="empresa" type="text" class="form-control form-control-sm required" id="empresa" value="{{$empresa->empresa or old('empresa')}}" required autofocus>
            </div>

            <div class="form-group">
              <label for="razao">Razão Social</label>
              <input name="razao" type="text" class="form-control form-control-sm required" id="razao" value="{{$empresa->razao or old('razao')}}" required>
            </div>

            <div class="row">
             <div class="form-group col-12 col-md-6">
              <label for="cnpj">CNPJ/MF</label>
              <input name="cnpj" type="text" class="form-control form-control-sm cnpj required" id="cnpj" value="{{$empresa->cnpj or old('cnpj')}}" required>
            </div>

            <div class="form-group col-12 col-md-6">
              <label for="ie">Inscrição Estadual</label>
              <input name="ie" type="text" class="form-control form-control-sm" id="ie" value="{{$empresa->ie or old('ie')}}">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-12 col-md-6">
              <label for="telefone">Telefone</label>
              <input name="telefone" type="text" class="form-control form-control-sm telefone required" id="telefone" value="{{$empresa->telefone or old('telefone')}}" required>
            </div>
            <div class="form-group col-12 col-md-6">
              <label for="celular">Celular</label>
              <input name="celular" type="text" class="form-control form-control-sm celular" id="celular" value="{{$empresa->celular or old('celular')}}">
            </div>
          </div>

          <div class="form-group">
            <label for="cep">CEP</label>
            <input name="cep" type="text" class="form-control form-control-sm cep required" id="cep" value="{{$endereco->cep or old('cep')}}" required>
          </div>

          <div class="row">
            <div class="form-group col-12 col-md-10">
              <label for="logradouro">Logradouro</label>
              <input name="logradouro" type="text" class="form-control form-control-sm" id="logradouro" value="{{$endereco->logradouro or old('logradouro')}}" required>
            </div>
            <div class="form-group col-12 col-md-2">
              <label for="numero">Nº</label>
              <input name="numero" type="text" class="form-control form-control-sm" id="numero" value="{{$endereco->numero or old('numero')}}" required>
            </div>
          </div>
          

          <div class="row">
            <div class="form-group col-6 col-md-4">
              <label for="complemento">Comp.</label>
              <input name="complemento" type="text" class="form-control form-control-sm" id="complemento" value="{{$endereco->complemento or old('complemento')}}">
            </div>
            <div class="form-group col-12 col-md-8">
              <label for="bairro">Bairro</label>
              <input name="bairro" type="text" class="form-control form-control-sm" id="bairro" value="{{$endereco->bairro or old('bairro')}}" required>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-12 col-md-9">
              <label for="cidade">Cidade</label>
              <input name="cidade" type="text" class="form-control form-control-sm" id="cidade" value="{{$endereco->cidade or old('cidade')}}" required>
            </div>
            <div class="form-group col-12 col-md-3">
              <label for="exampleFormControlSelect1">UF</label>
              <select name="uf" class="form-control" id="uf">
                <option value="AC" {{ $uf == "AC"?"selected":""}}>AC</option>
                <option value="AL" {{ $uf == "AL"?"selected":""}}>AL</option>
                <option value="AP" {{ $uf == "AP"?"selected":""}}>AP</option>
                <option value="AM" {{ $uf == "AM"?"selected":""}}>AM</option>
                <option value="BA" {{ $uf == "BA"?"selected":""}}>BA</option>
                <option value="CE" {{ $uf == "CE"?"selected":""}}>CE</option>
                <option value="DF" {{ $uf == "DF"?"selected":""}}>DF</option>
                <option value="ES" {{ $uf == "ES"?"selected":""}}>ES</option>
                <option value="GO" {{ $uf == "GO"?"selected":""}}>GO</option>
                <option value="MA" {{ $uf == "MA"?"selected":""}}>MA</option>
                <option value="MT" {{ $uf == "MT"?"selected":""}}>MT</option>
                <option value="MS" {{ $uf == "MS"?"selected":""}}>MS</option>
                <option value="MG" {{ $uf == "MG"?"selected":""}}>MG</option>
                <option value="PA" {{ $uf == "PA"?"selected":""}}>PA</option>
                <option value="PB" {{ $uf == "PB"?"selected":""}}>PB</option>
                <option value="PR" {{ $uf == "PR"?"selected":""}}>PR</option>
                <option value="PE" {{ $uf == "PE"?"selected":""}}>PE</option>
                <option value="PI" {{ $uf == "PI"?"selected":""}}>PI</option>
                <option value="RJ" {{ $uf == "RJ"?"selected":""}}>RJ</option>
                <option value="RN" {{ $uf == "RN"?"selected":""}}>RN</option>
                <option value="RS" {{ $uf == "RS"?"selected":""}}>RS</option>
                <option value="RO" {{ $uf == "RO"?"selected":""}}>RO</option>
                <option value="RR" {{ $uf == "RR"?"selected":""}}>RR</option>
                <option value="SC" {{ $uf == "SC"?"selected":""}}>SC</option>
                <option value="SP" {{ $uf == "SP"?"selected":""}}>SP</option>
                <option value="SE" {{ $uf == "SE"?"selected":""}}>SE</option>
                <option value="TO" {{ $uf == "TO"?"selected":""}}>TO</option>
              </select>
            </div>
          </div>

            {{-- <div class="form-group">
              <label for="cod_vip">CÓD VIP</label>
              <input name="cod_vip" type="text" class="form-control form-control-sm" id="cod_vip" value="{{$endereco->cod_vip or old('cod_vip')}}">
              @if ($errors->has('cod_vip'))
              <span class="help-block">
                <strong>{{ $errors->first('cod_vip') }}</strong>
              </span>
              @endif
            </div> --}}

            <script type="text/javascript">
              $(document).ready( function() {
                /* Executa a requisição quando o campo CEP perder o foco */
                $('#cep').blur(function(){
                  /* Configura a requisição AJAX */
                  $.ajax({
                    url : 'http://localhost:8000/consulta_cep.php', /* URL que será chamada */
                    type : 'POST', /* Tipo da requisição */
                    data: 'cep=' + $('#cep').val(), /* dado que será enviado via POST */
                    dataType: 'json', /* Tipo de transmissão */
                    success: function(data){
                      if(data.sucesso == 1){
                        $('#logradouro').val(data.rua);
                        $('#bairro').val(data.bairro);
                        $('#cidade').val(data.cidade);
                        $('#uf').val(data.estado);

                        $('#numero').focus();
                      }
                    }
                  });
                  return false;
                })
              });
            </script>


            @if (!isset($empresa))
            <button type="submit" class="btn btn-primary loader-button">Salvar</button>
            @else
            <button type="submit" class="btn btn-primary loader-button">Salvar</button>
            @endif

          </form>
        </div>
      </div>


      <!-- /.container-fluid-->
      @endsection
