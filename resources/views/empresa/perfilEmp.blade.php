@extends('layouts.soged')

@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Example DataTables Card-->
      <div class="p-2 mb-2 bg-primary text-white"><h5>Perfil da Empresa</h5></div>
      <div class="row">

        <div class="col-lg-4">
          <form class="" action="{{route('perfilemp.update',$empresa->id_empresa)}}" method="post">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="form-group">
              <label for="nome">Nome</label>
              <input name="nome" type="text" class="form-control form-control-sm" id="nome" value="{{$empresa->nome}}">
              @if ($errors->has('nome'))
                <span class="help-block">
                  <strong>{{ $errors->first('nome') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="razao">Razão</label>
              <input type="text" class="form-control form-control-sm" id="razao" name="razao" value="{{$empresa->razao}}">
              @if ($errors->has('razao'))
                <span class="help-block">
                  <strong>{{ $errors->first('razao') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="telefone">Telefone</label>
              <input name="telefone" type="number" class="form-control form-control-sm" id="telefone" value="{{$empresa->telefone}}">
              @if ($errors->has('telefone'))
                <span class="help-block">
                  <strong>{{ $errors->first('telefone') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="logradouro">Logradouro</label>
              <input name="logradouro" type="text" class="form-control form-control-sm" id="logradouro" value="{{$empresa->endereco->logradouro}}">
              @if ($errors->has('logradouro'))
                <span class="help-block">
                  <strong>{{ $errors->first('logradouro') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="complemento">Complemento</label>
              <input name="complemento" type="text" class="form-control form-control-sm" id="complemento" value="{{$empresa->endereco->complemento}}">
              @if ($errors->has('complemento'))
                <span class="help-block">
                  <strong>{{ $errors->first('complemento') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="bairro">Bairro</label>
              <input name="bairro" type="text" class="form-control form-control-sm" id="bairro" value="{{$empresa->endereco->bairro}}">
              @if ($errors->has('bairro'))
                <span class="help-block">
                  <strong>{{ $errors->first('bairro') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="exampleFormControlSelect1">Estado</label>
              <select name="uf" class="form-control" id="exampleFormControlSelect1">
                <option value="AC" {{$empresa->endereco->uf == "AC"?"selected":""}}>Acre<option>
                <option value="AL" {{$empresa->endereco->uf == "AL"?"selected":""}}>Alagoas<option>
                <option value="AP" {{$empresa->endereco->uf == "AP"?"selected":""}}>Amapá<option>
                <option value="AM" {{$empresa->endereco->uf == "AM"?"selected":""}}>Amazonas<option>
                <option value="BA" {{$empresa->endereco->uf == "BA"?"selected":""}}>Bahia<option>
                <option value="CE" {{$empresa->endereco->uf == "CE"?"selected":""}}>Ceara<option>
                <option value="DF" {{$empresa->endereco->uf == "DF"?"selected":""}}>Distrito Federal<option>
                <option value="ES" {{$empresa->endereco->uf == "ES"?"selected":""}}>Espírito Santo<option>
                <option value="GO" {{$empresa->endereco->uf == "GO"?"selected":""}}>Goiás<option>
                <option value="MA" {{$empresa->endereco->uf == "MA"?"selected":""}}>Maranhão<option>
                <option value="MT" {{$empresa->endereco->uf == "MT"?"selected":""}}>Mato Grosso<option>
                <option value="MS" {{$empresa->endereco->uf == "MS"?"selected":""}}>Mato Grosso do Sul<option>
                <option value="MG" {{$empresa->endereco->uf == "MG"?"selected":""}}>Minas Gerais<option>
                <option value="PA" {{$empresa->endereco->uf == "PA"?"selected":""}}>Pará<option>
                <option value="PB" {{$empresa->endereco->uf == "PB"?"selected":""}}>Paraíba<option>
                <option value="PR" {{$empresa->endereco->uf == "PR"?"selected":""}}>Paraná<option>
                <option value="PE" {{$empresa->endereco->uf == "PE"?"selected":""}}>Pernambuco<option>
                <option value="PI" {{$empresa->endereco->uf == "PI"?"selected":""}}>Piauí<option>
                <option value="RJ" {{$empresa->endereco->uf == "RJ"?"selected":""}}>Rio de Janeiro<option>
                <option value="RN" {{$empresa->endereco->uf == "RN"?"selected":""}}>Rio Grande do Norte<option>
                <option value="RS" {{$empresa->endereco->uf == "RS"?"selected":""}}>Rio Grande do Sul<option>
                <option value="RO" {{$empresa->endereco->uf == "RO"?"selected":""}}>Rondônia<option>
                <option value="RR" {{$empresa->endereco->uf == "RR"?"selected":""}}>Roraima<option>
                <option value="SC" {{$empresa->endereco->uf == "SC"?"selected":""}}>Santa Catarina<option>
                <option value="SP" {{$empresa->endereco->uf == "SP"?"selected":""}}>São Paulo<option>
                <option value="SE" {{$empresa->endereco->uf == "SE"?"selected":""}}>Sergipe<option>
                <option value="TO" {{$empresa->endereco->uf == "TO"?"selected":""}}>Tocantins<option>
              </select>
            </div>

            <div class="" style="display: {{ Auth::user()->grupo_id >2?"none":"" }}">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Confirma a alteração?')">
                <i class='fa fa-edit fa-lg' aria-hidden='true'></i> Alterar
              </button>
            </div>
          </div>


          <div class="col-lg-4">
            <div class="form-group">
              <label for="cnpj">CNPJ/MF</label>
              <input name="cnpj" type="number" class="form-control form-control-sm" id="cnpj" value="{{$empresa->cnpj}}">
              @if ($errors->has('cnpj'))
                <span class="help-block">
                  <strong>{{ $errors->first('cnpj') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="ie">Inscrição Estadual</label>
              <input name="ie" type="number" class="form-control form-control-sm" id="ie" value="{{$empresa->ie}}">
              @if ($errors->has('ie'))
                <span class="help-block">
                  <strong>{{ $errors->first('ie') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="celular">Celular</label>
              <input name="celular" type="number" class="form-control form-control-sm" id="celular" value="{{$empresa->celular}}">
              @if ($errors->has('celular'))
                <span class="help-block">
                  <strong>{{ $errors->first('celular') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="numero">Número</label>
              <input name="numero" type="text" class="form-control form-control-sm" id="numero" value="{{$empresa->endereco->numero}}">
              @if ($errors->has('numero'))
                <span class="help-block">
                  <strong>{{ $errors->first('numero') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="cidade">Cidade</label>
              <input name="cidade" type="text" class="form-control form-control-sm" id="cidade" value="{{$empresa->endereco->cidade}}">
              @if ($errors->has('cidade'))
                <span class="help-block">
                  <strong>{{ $errors->first('cidade') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group">
              <label for="cep">CEP</label>
              <input name="cep" type="number" class="form-control form-control-sm" id="cep" value="{{$empresa->endereco->cep}}">
              @if ($errors->has('cep'))
                <span class="help-block">
                  <strong>{{ $errors->first('cep') }}</strong>
                </span>
              @endif
            </div>

          </form>
        </div>

        <!-- IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG -->
        <div class="col-lg-4">
          <img src="{{env('APP_S3').$empresa->foto}}" alt="..." class="img-responsive" width="250" >
          <form class="form-inline" action="{{route('fotoemp')}}" method="post" enctype="multipart/form-data" style="display: {{ Auth::user()->grupo_id >2?"none":"" }}">
            <input type="hidden" name="id_emp" value="{{$empresa->id_empresa}}">
            {{ csrf_field() }}
            <div class="form-group mx-sm-2">
              <input class="form-control" id="image" type="file" name="image" value="" accept=".png, .jpg">

              <input name="image" type="file" id="file2" class="custom-file-input" accept=".png, .jpg">
            </div>
            <button type="submit" class="btn btn-primary" onclick="return confirm('Confirma a alteração?')">Enviar</button>
          </form>
        </div>
      </div>
      <!-- IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG -->
      <br>

    </div>

    <!-- /.container-fluid-->
  @endsection
