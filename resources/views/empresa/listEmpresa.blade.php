@extends('layouts.soged')

@section('content')
<div class="container-fluid">

  <!-- Example DataTables Card-->
  <div class="card mb-3">

   <div class="card-header">

    <div>
      <strong>Lista de Empresas</strong>
    </div>

    <div>
      <div class="text-right">
        <a class="btn btn-primary" href="{{route("empresa.create")}}" title="Cadastrar Empresa">
          <i class="text-white fa fa-plus" aria-hidden="true"></i>
        </a>
      </div>
    </div>

  </div>

  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th> Empresas </th>
            <th> Telefone </th>
            <th> Cidade </th>
            <th> Rua </th>
            <th> Ação </th>
          </tr>
        </thead>
        <tbody>
          @foreach ($empresas as $item)
          @php
          $teste = (object) $item->endereco;
          @endphp
          <tr>
            <td>{{ $item->empresa }}</td>
            <td>{{ $item->telefone  }}</td>
            <td>{{ $teste->cidade  }}</td>
            <td>{{ $teste->logradouro  }}</td>
            <td>
              <a href="{{route('empresa.edit',$item->id_empresa)}}" title="Edita lançamento"><i class="far fa-edit" aria-hidden="true"></i></a>&nbsp;
              <a href="{{route('apaga_empresa',$item->id_empresa)}}" onclick="return confirm('Confirma a exclusão?')" title="Exclui lançamento">
                <i class="text-danger far fa-trash-alt" aria-hidden="true"></i>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
  </div>

</div>
<!-- /.container-fluid-->
@endsection
