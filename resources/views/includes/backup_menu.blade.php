@php
$alertas = session()->pull('teste');
$bell = 'fa-bell-slash-o';
@endphp
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<a class="navbar-brand" href="pasta">{{ config('app.name', 'Laravel') }}</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- MENU LATERAL -->
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
					<a class="nav-link" href="{{route("perfil.index")}}">
						<img src="{{env('APP_S3').Auth::user()->foto}}" alt="..." class="img-responsive" width="30" >
						<span class="nav-link-text">{{ Auth::user()->name }} {{ Auth::user()->empresa_id }} {{ Auth::user()->grupo_id }}</span>
					</a>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard" style="display: {{ Auth::user()->empresa_id==1?"":"none" }}">
					<a class="nav-link" href="{{url("empresa")}}">
						<i class="fa fa-fw fa-industry"></i>
						<span class="nav-link-text">Empresas</span>
					</a>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard" style="display: {{ Auth::user()->empresa_id==1?"none":"" }}">
					<a class="nav-link" href="{{url("cliente")}}">
						<i class="fa fa-fw fa-users"></i>
						<span class="nav-link-text">Clientes</span>
					</a>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components" style="display: {{ Auth::user()->grupo_id < 3?"":"none" }}">
					<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
						<i class="fa fa-fw fa-user"></i>
						<span class="nav-link-text">Usuários</span>
					</a>
					<ul class="sidenav-second-level collapse" id="collapseComponents">
						<li>
							<a href="{{route("grupo.index")}}">Grupos de Usuários</a>
						</li>
						<li>
							<a href="{{route("usuario.index")}}">Usuários</a>
						</li>
					</ul>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables" style="display: {{ Auth::user()->grupo_id==2?"":"none" }}">
					<a class="nav-link" href="{{route("fluxo.index")}}">
						<i class="fa fa-sort-amount-asc"></i>
						<span class="nav-link-text">Fluxos</span>
					</a>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables" style="display: {{ Auth::user()->empresa_id==1?"none":"" }}">
					<a class="nav-link" href="{{route("template.index")}}">
						<i class="fa fa-fw fa-file"></i>
						<span class="nav-link-text">Tipos de Templates</span>
					</a>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Documentos" style="display: {{ Auth::user()->empresa_id==1?"none":"" }}">
					<a class="nav-link" href="{{url("pasta")}}">
						<i class="fa fa-fw fa-folder"></i>
						<span class="nav-link-text">Meus documentos </span>
					</a>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels" style="display:none">
					<a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
						<i class="fa fa-fw fa-sitemap"></i>
						<span class="nav-link-text">Menu Levels</span>
					</a>
					<ul class="sidenav-second-level collapse" id="collapseMulti">
						<li>
							<a href="#">Second Level Item</a>
						</li>
						<li>
							<a href="#">Second Level Item</a>
						</li>
						<li>
							<a href="#">Second Level Item</a>
						</li>
						<li>
							<a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Third Level</a>
							<ul class="sidenav-third-level collapse" id="collapseMulti2">
								<li>
									<a href="#">Third Level Item</a>
								</li>
								<li>
									<a href="#">Third Level Item</a>
								</li>
								<li>
									<a href="#">Third Level Item</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>

				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link" style="display:none">
					<a class="nav-link" href="#">
						<i class="fa fa-fw fa-link"></i>
						<span class="nav-link-text">Link</span>
					</a>
				</li>
			</ul>

			<!-- MENU SUPERIOR -->
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item">
					<a class="nav-link text-center" id="sidenavToggler">
						<i class="fa fa-fw fa-angle-left"></i>
					</a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<!-- Notificação -->
				<li class="nav-item dropdown">

					

					<div class="dropdown-menu" aria-labelledby="alertsDropdown">
						<h6 class="dropdown-header">&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;:&nbsp;:&nbsp; NOTIFICAÇÕES E ALERTAS &nbsp;:&nbsp;:&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</h6>
						<div class="dropdown-divider"></div>

						@if ($alertas != NULL)
						@foreach ($alertas as $key)
						@if (Auth::id() == $key->user_id)
						@php
						$text = 'text-success';
						$bell = 'fa-bell';
						if($key->status == 2)   $text = 'text-dark';
						if($key->tipo == 1)     $icone = "fa fa-hand-o-right";
						elseif($key->tipo == 2) $icone = "fa fa-envelope-o";
						elseif($key->tipo == 3) $icone = "fa fa-thumbs-o-up";

						@endphp

						<a class="dropdown-item" href="{{route('alerta.edit',$key->id_alert)}}">
							<span class="{{$text}}">
								<strong>
									<i class="{{$icone}} fa-fw"></i> {{ $key->alerta or 'Sem alertas' }}</strong>
								</span>
								<span class="small float-right text-muted" title="{{ date( 'd/m/Y' , strtotime($key->created_at)) }}">
									{{ date( 'h:m' , strtotime($key->created_at)) }}</span>
									<div class="dropdown-message small">{{ $key->desc or '' }}</div>
								</a>
								<div class="dropdown-divider "></div>

								@endif
								@endforeach
								@endif

								<a class="dropdown-item small d-lg-none" href="#">Abrir todos os alertas</a>
							</div>

							

							<a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-fw {{$bell}} fa-lg"></i>
								<span class="d-lg-none">Alerts
									<span class="badge badge-pill badge-warning">6 New</span>
								</span>
								@if ($bell == 'fa-bell')
								<span class="indicator text-danger d-none d-lg-block" title="Alertas">
									<i class="fa fa-fw fa-circle"></i>
								</span>
								@endif
							</a>
						</li>
						<!-- Fim notificação -->
						<!-- BUSCA ARQUIVOS E PASTAS BUSCA ARQUIVOS E PASTAS BUSCA ARQUIVOS E PASTAS BUSCA ARQUIVOS E PASTAS -->
						<li class="nav-item">
							<form class="form-inline my-2 my-lg-0 mr-lg-2" action="{{route("buscar")}}" method="post">
								{{ csrf_field() }}
								<div class="input-group">
									<input class="busca" name="nome" type="text" placeholder="Buscar Arquivos e Pastas">
								</div>
							</form>
						</li>
						<!-- BUSCA ARQUIVOS E PASTAS BUSCA ARQUIVOS E PASTAS BUSCA ARQUIVOS E PASTAS BUSCA ARQUIVOS E PASTAS -->
						<li class="nav-item">
							<div class="row">
								<div class="col-xs-8 col-md-4">
									<a href="{{route("perfilemp.index")}}">
										<img src="{{env('APP_S3').'empresas/fotos/'.\Auth::user()->empresa_id}}.png" alt="..." class="img-responsive" width="30" >
									</a>
								</div>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="modal" data-target="#exampleModal">
								<i class="fa fa-fw fa-sign-out"></i>Sair</a>
							</li>
						</ul>
					</div>
				</nav>
