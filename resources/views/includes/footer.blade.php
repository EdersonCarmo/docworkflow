
<!-- /.content-wrapper-->
<footer class="p-3">
  <div class="container">
    <div class="text-center">
      <small>Copyright © SOGED 2017</small>
    </div>
  </div>
</footer>

<!-- Logout -->
<form id="logout-form" action="{{ route('logout') }}" method="POST">
  {{ csrf_field() }}
</form>
