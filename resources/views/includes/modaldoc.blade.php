<!-- ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS ARQUIVOS -->
<!-- Modal MENU NEW ARQUIVOS -->
<div class="modal fade" id="newarqModal" tabindex="-1" role="dialog" aria-labelledby="newarqModallLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newarqModalLabel"><i class="fa fa-file"></i> Criar novo arquivo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{route("pasta.store")}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="user_id" value="{{ Auth::id() }}">
          <input type="hidden" name="ref_pai" value="{{session('pasta.atual')}}">

          <label for="pasta">Nome do arquivo </label>
          <input class="form-control" id="pasta" type="text" name="pasta" value="" required autofocus>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Criar a pasta</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU NEW ARQUIVOS -->
<!-- Modal MENU RENAME ARQUIVOS -->
<div class="modal fade" id="renameArqModal" tabindex="-1" role="dialog" aria-labelledby="renameArqModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="renameArqModalLabel"><i class="fa fa-file"></i> RENOMEAR ARQUIVO</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{route("arquivo.update",$idarq)}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="form-group">
            <label for="arquivo" class="control-label">Novo nome</label>
            <input class="form-control" id="arquivo" type="text" name="arquivo" value="" required autofocus>
          </div>
        </div>
        <div class="modal-footer">
          <input id="id_arquivo" type="hidden" name="id_arquivo" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Confirma alteração</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU RENAME ARQUIVOS -->

<!-- Modal MENU MOVE ARQUIVO -->
<div class="modal fade" id="moveArqModal" tabindex="-1" role="dialog" aria-labelledby="moveArqModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="moveArqModalLabel"><i class="fa fa-file"></i> MOVER ARQUIVO PARA </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      @isset($folder)
      <div class="modal-body">
        <form class="" action="{{route("permissao.update",1)}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <input type="hidden" name="acao" value="MOVER">
          <div  class="col-lg-12">
            <select class="form-control" name="pasta_id">
              @foreach ($folder as $key)
              <option value="{{$key->id_pasta}}">{{$key->pasta}}</option>
              @endforeach
            </select>
          </div>
        </div>
        @endisset

        <div class="modal-footer">
          <input id="id_permissao" type="hidden" name="id_permissao" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
          <button type="submit" class="btn btn-primary">Mover</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU MOVE ARQUIVO -->
<!-- Modal DEFINE FUNDO TEMPLATE ARQUIVO -->
<div class="modal fade" id="fundoTemModal" tabindex="-1" role="dialog" aria-labelledby="fundoTemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="fundoTemModalLabel"><i class="fa fa-file"></i> MOVER ARQUIVO PARA +</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form class="" action="{{route("documento.update",1)}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <input id="id_documento" type="hidden" name="id_documento" value="">

          @isset($templates)
          <div class="col-lg-12">
            <select class="form-control" name="id_documento" onchange="this.form.submit()">
              <option value="">Selecione ...</option>
              @foreach ($templates as $key)
              @if ($key->crud == 15)
              <option value="{{$key->id_documento}}">{{$key->nomeDoc}}</option>
              @endif
              @endforeach
            </select>
          </div>
          @endisset
        </div>

        <div class="modal-footer">
          <input id="local" type="hidden" name="local" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal DEFINE FUNDO TEMPLATE ARQUIVO -->
<!-- PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS PASTAS -->
<!-- Modal MENU RENAME PASTAS -->
<div class="modal fade" id="renamePastaModal" tabindex="-1" role="dialog" aria-labelledby="renamePastaModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="renamePastaModalLabel"><i class="fa fa-folder"></i> RENOMEAR PASTA</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{route("pasta.update",$idarq)}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div class="form-group">
            <label for="arquivo" class="control-label">Novo nome</label>
            <input class="form-control" id="pasta" type="text" name="pasta" value="" required autofocus>
          </div>
        </div>
        <div class="modal-footer">
          <input id="id_pasta" type="hidden" name="id_pasta" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Confirma alteração</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU RENAME PASTAS -->
<!-- Modal MENU UPLOAD -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="uploadModalLabel">
          <i class="fa  fa-cloud-upload" aria-hidden="true"></i> Envio de arquivos
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{route("arquivos")}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
          @isset($_SESSION['pastaend'])
          <input type="hidden" name="pastaend" value="{{ $_SESSION['pastaend'] }}">
          @endisset
          @isset($_SESSION['idpastaatual'])
          <input type="hidden" name="pastaatual" value="{{ $_SESSION['idpastaatual'] }}">
          @endisset
          <input class="form-control" id="arquivo" type="file" name="arquivo[]" value="" required autofocus multiple>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><i class="fa  fa-cloud-upload" aria-hidden="true"></i> Enviar arquivo</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU UPLOAD -->
<!-- Modal MENU NEW PASTAS -->
<div class="modal fade" id="newPastaModal" tabindex="-1" role="dialog" aria-labelledby="newPastaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newPastaModalLabel"><i class="fa  fa-folder" aria-hidden="true"></i> CRIAR PASTA {{$atual}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{route("pasta.store")}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="user_id" value="{{ Auth::id() }}">

          <label for="pasta">Nome da pasta </label>
          <input class="form-control" id="pasta" type="text" name="pasta" value="" required autofocus>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="ref_pai" value="">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Criar a pasta</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU NEW PASTAS -->

<!-- Modal MENU MOVE PASTA -->
<div class="modal fade" id="movePastaModal" tabindex="-1" role="dialog" aria-labelledby="movePastaModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="movePastaModalLabel"><i class="fa fa-file"></i> SELECIONE A PASTA DE DESTINO </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      @isset($folder)
      <div class="modal-body">
        <form class="" action="{{route("pasta.update",$idarq)}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <div  class="col-lg-12">
            <select class="form-control" name="ref_pai">
              @foreach ($folder as $key)
              <option value="{{$key->id_pasta}}">{{$key->pasta}}</option>
              @endforeach
            </select>
          </div>
        </div>

        @endisset
        <div class="modal-footer1">
          <input id="id_permissao" type="hidden" name="id_permissao" value="">
        </div>

        <div class="modal-footer">
          <input id="id_pasta" type="hidden" name="id_pasta" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancela</button>
          <button type="submit" class="btn btn-primary">Mover</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU MOVE PASTA -->

<!-- PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO -->
<!-- Modal PERMISSAO -->
<div class="modal fade" id="permissaoModal" tabindex="-1" role="dialog" aria-labelledby="permissaoModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="permissaoModalLabel"><i class="fa fa-file"></i> teste teste </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <form class="was-validated" action="{{route("pasta.store")}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <input type="hidden" name="users_id" value="{{ Auth::user()->id }}">
          <div class="col-lg-12">
            <label class="custom-control custom-checkbox">
              <input value="1" type="checkbox" class="custom-control-input" value="1" checked required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Visualizar</span>
            </label>
            <label class="custom-control custom-checkbox">
              <input value="2" type="checkbox" class="custom-control-input" value="2" required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Criar</span>
            </label>
            <label class="custom-control custom-checkbox">
              <input value="4" type="checkbox" class="custom-control-input" value="4" required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Alterar</span>
            </label>
            <label class="custom-control custom-checkbox">
              <input value="8" type="checkbox" class="custom-control-input" value="8" required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Apagar</span>
            </label>


            <label class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" value="1" required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Download</span>
            </label>

            <label class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" value="2" required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">UpLoad</span>
            </label>

            <label class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" value="4" required>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Alterar Permissões</span>
            </label>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleFormControlSelect2" title="Use 'Ctrl' para mais de um item">Grupos de usuários</label>
                <select multiple class="form-control" id="exampleFormControlSelect2" name="grupos">
                  <option value="1">João</option>
                  <option value="2">André</option>
                  <option value="3">Pedro</option>
                  <option value="4">Francisco</option>
                  <option value="5">Joaquin</option>
                </select>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="exampleFormControlSelect2" title="Use 'Ctrl' para mais de um item">Usuários</label>
                <select multiple class="form-control" id="exampleFormControlSelect2" name="usuarios">
                  <option value="1">João</option>
                  <option value="2">André</option>
                  <option value="3">Pedro</option>
                  <option value="4">Francisco</option>
                  <option value="5">Joaquin</option>
                </select>
              </div>
            </div>

          </div>

        </div>
        <div class="modal-footer">
          <input id="id_arquivo" type="hidden" name="id_arquivo" value="">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Gravar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal PERMISSAO -->
<!-- PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO PERMISSAO -->
<!-- DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO -->
<!-- Modal SELECIONA TEMPLATE -->
<div class="modal fade" id="selectTemModal" tabindex="-1" role="dialog" aria-labelledby="selectTemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="selectTemModalLabel"><i class="fa fa-file"></i> Escolha o template para utilizar-lo. </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @isset($templates)
      <div class="modal-body">
        <form class="" action="{{route('createDocTem')}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="pasta" value="{{$atual}}">

          <div class="col-lg-12">
            <label class="" for="">&nbsp;Escolha o modelo de template ou documetno em branco.</label>
            <select class="form-control" name="id_documento" onchange="this.form.submit()">
              <option value="">Selecione ...</option>
              @foreach ($templates as $key)
              <option value="{{$key->id_documento}}">{{$key->documento}}</option>
              @endforeach
            </select>
          </div>
        </div>

        @endisset

        <div class="modal-footer">
          <a class="btn btn-primary" href="{{route('createDoc',$atual)}}">Documento em branco</a>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal SELECIONA TEMPLATE -->
<!-- Modal MENU RENAME DOCUMENTOS -->
<div class="modal fade" id="renameDocModal" tabindex="-1" role="dialog" aria-labelledby="renameDocModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="renameDocModalLabel"><i class="fa fa-file"></i> RENOMEAR DOCUMENTO</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="{{route("documento.update",'rename')}}" method="post">
          {{ csrf_field() }} {{ method_field('PUT') }}
          <input type="hidden" name="cliente_id" value="">
          <div class="form-group">
            <label for="documento" class="control-label">Novo nome</label>
            <input class="form-control" id="documento" type="text" name="documento" value="" required autofocus>
          </div>
        </div>
        <div class="modal-footer">
          <input id="id_documento" type="hidden" name="id_documento" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Confirma alteração</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Modal MENU RENAME DOCUMENTOS -->
<!-- DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO DOCUMENTO -->
<!-- IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG -->
<!-- VISUALIZADOR IMG -->
<div class="modal fade bs-example-modal-lg" id="imgModal" tabindex="-1" role="dialog" aria-labelledby="imgModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="imgModalLabel"><i class="fa fa-file-image-o"></i></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <center>
            <img src="" alt="" style="max-width:100%;">
          </center>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- VISUALIZADOR IMG -->
<!-- IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG -->
