<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Soged') }}</title>

    <!-- Styles -->
    <!--
        <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
        <!-- Bootstrap core CSS-->

        <link rel="stylesheet" href="{{URL::asset('/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

        <!-- Custom styles for this template-->

        <link href="{{URL::asset('css/sb-admin.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/soged.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/teste.css')}}" media="print" rel="stylesheet">  
        <link href="{{URL::asset('/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
        <link href="{{URL::asset('css/select2.min.css') }}" rel="stylesheet">
        <link href="{{URL::asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{URL::asset('css/lightbox.min.css') }}" rel="stylesheet">
        <link href="{{URL::asset('css/jquery-ui.theme.min.css') }}" rel="stylesheet">
        <link href="{{URL::asset('css/croppie.css') }}" rel="stylesheet">
        <link href="{{URL::asset('css/demo.css') }}" rel="stylesheet">
        

        <script src="{{URL::asset('/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{URL::asset('/js/jquery-ui.min.js')}}"></script>
        <script src="{{URL::asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- Custom scripts for this page-->
        <!-- Page level plugin JavaScript-->
        <script src="{{URL::asset('/vendor/datatables/jquery.dataTables.js')}}"></script>
        <script src="{{URL::asset('/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{URL::asset('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
        <!-- Custom scripts for all pages-->
        <script src="{{URL::asset('/js/sb-admin.min.js')}}"></script>
        <!-- Custom scripts for this page-->
        <!-- Toggle between fixed and static navbar-->
        <script src="{{URL::asset('/js/sb-admin-datatables.min.js')}}"></script>

        <script src="{{URL::asset('/js/sweetalert.min.js')}}"></script>

        <script src="{{URL::asset('/js/jquery.mask.min.js')}}"></script>

        <script src="{{URL::asset('/js/select2.min.js')}}"></script>

        <script src="{{URL::asset('/js/interact.min.js')}}"></script>

        <script src="{{URL::asset('/js/custom.js')}}"></script>

        <script src="{{URL::asset('/js/fontawesome.js')}}"></script>

        <script src="{{URL::asset('/js/lightbox.min.js')}}"></script>

        <script src="{{URL::asset('/js/ckeditor.js')}}"></script>

        <script src="{{URL::asset('/js/croppie.js')}}"></script>

        <script src="{{URL::asset('/js/demo.js')}}"></script>

        


    </head>

    <body>
