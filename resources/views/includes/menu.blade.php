@php
$alertas = session()->pull('teste');
@endphp
<!-- Sidebar Holder -->
<nav id="sidebar">
	<div class="sidebar-header">

		<div class="perfil mt-3">
			<img src="{{ env('APP_S3').Auth::user()->foto }}" class="img-fluid">

		</div>

		<small>Olá, {{ Auth::user()->name }}</small>

		<ul class="list-inline justify-content-center mb-0">

			<!-- Notificação -->
			{{-- <li class="list-inline-item">
				<a href="#" id="notificacoes-click" style="position: relative">
					<i class="fa fa-bell"></i>
					@if($alertas != NULL)
					<div class="circle"></div>
					@endif
				</a>
			</li> --}}

			<!-- Perfil -->
			<li class="list-inline-item">
				<a href="{{url('usuario/'.Auth::id().'/edit')}}" id="editar"><i class="fa fa-user"></i></a>
			</li>

			<!-- Sair -->
			<li class="list-inline-item">
				<a href="#" id="sair"><i class="fa fa-sign-out-alt"></i></a>
			</li>

		</ul>
	</div>

	<ul class="list-unstyled components">

		@if(Auth::user()->empresa_id!=1)
		<li>
			<a href="{{url('pasta')}}"><i class="fas fa-folder-open mr-2"></i>Meus Documentos</a>
		</li>
		<li>

			<a href="#pageSubmenuM" data-toggle="collapse" aria-expanded="false" id="documentos"><i class="far fa-folder-open mr-2"></i>Outros Locais</a>

			<ul class="collapse list-unstyled" id="pageSubmenuM">
				{{-- <li><a href="{{route('abrir','a')}}"><i class="far fa-thumbs-up mr-2"></i>Documentos aprovados</a></li> --}}
				<li><a href="{{route('abrir','c')}}"><i class="far fa-share-square mr-2"></i>Compartilhado comigo</a></li>
				<li><a href="{{route('abrir','t')}}"><i class="far fa-file-alt mr-2"></i>Templates</a></li>
				<li><a href="{{route('moldura.index')}}"><i class="far fa-file-alt mr-2"></i>Cabeçalhos e rodapés</a></li>
				@if(Auth::user()->empresa_id!=1)
				<li>
					<a href="{{route('template.index')}}">
						<i class="fa fa-file-alt mr-2"></i>
						Categorias Templates
					</a>
				</li>
				@endif
			</ul>
		</li>

		<li>
			<a href="{{url('cliente')}}">
				<i class="fa fa-users mr-2"></i>
				Clientes
			</a>
		</li>
		@endif
		@if(Auth::user()->empresa_id==1)
		<li>
			<a href="{{url('empresa')}}">
				<i class="fa fa-building mr-2"></i>
				Empresas
			</a>
		</li>
		@endif

		@if(Auth::user()->grupo_id < 3)
		<li>
			<a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
				<i class="fa fa-key mr-2"></i>
				Usuários
			</a>
			<ul class="collapse list-unstyled" id="pageSubmenu">
				<li><a href="{{route('usuario.index')}}">Usuários</a></li>
				@if(Auth::user()->empresa_id != 1)
				<li><a href="{{route('grupo.index')}}">Grupos de Usuários</a></li>
				@endif
			</ul>
		</li>
		@endif

		@if(Auth::user()->grupo_id==2)
		{{-- <li>
			<a href="{{route('fluxo.index')}}">
				<i class="fa fa-exchange-alt mr-2"></i>
				Fluxos
			</a>
		</li> --}}
		@endif

		


	</ul>
</nav>

<!-- Notificações Lista -->
<div class="overlay-notificacoes"></div>

<div class="menu-notificacoes" aria-labelledby="alertsDropdown" id="notificacoes">
	<h6 class="dropdown-header">&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;:&nbsp;:&nbsp; NOTIFICAÇÕES E ALERTAS &nbsp;:&nbsp;:&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</h6>
	<div class="dropdown-divider"></div>

	@if ($alertas != NULL)
	@foreach ($alertas as $key)
	@if (Auth::id() == $key->user_id)
	@php
	$text = 'text-success';
	$bell = 'fa-bell';
	if($key->status == 2)   $text = 'text-dark';
	if($key->tipo == 1)     $icone = "fa fa-hand-point-right";
	elseif($key->tipo == 2) $icone = "fa fa-envelope";
	elseif($key->tipo == 3) $icone = "fa fa-thumbs-up";

	@endphp

	<a class="dropdown-item" href="{{route('alerta.edit',$key->id_alert)}}">
		<span class="{{$text}}">
			<strong>
				<i class="{{$icone}} fa-fw"></i> {{ $key->alerta or 'Sem alertas' }}
			</strong>
		</span>
		<span class="small float-right text-muted" title="{{ date( 'd/m/Y' , strtotime($key->created_at)) }}">{{ date( 'h:m' , strtotime($key->created_at)) }}</span>
		<div class="dropdown-message small">{{ $key->desc or '' }}</div>
	</a>

	<div class="dropdown-divider "></div>

	@endif
	@endforeach
	@endif
</div>
