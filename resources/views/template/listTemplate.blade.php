@extends('layouts.soged')

@section('content')
<div class="container-fluid">
  <div class="card mb-3">

    <div class="card-header">

      <div>
        <strong>Lista de Templates</strong>
      </div>
      
      <div>
        <div class="text-right">
          <a class="btn btn-primary" href="{{route('template.create')}}" title="Cadastrar Template">
            <i class="text-white fa fa-plus" aria-hidden="true"></i>
          </a>
        </div>
      </div>

    </div>

    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Template</th>
              <th>Criação</th>
              <th class="text-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($templates as $key)
            <tr>
              <td>{{ $key->template }}</td>
              <td>{{ date( 'd/m/Y h:m' , strtotime($key->created_at)) }}</td>
              <td class="actions">
                <a href="{{route('template.edit',$key->id_template)}}" title="Alterar Template">
                  <i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                <a href="{{route('apaga_template',$key->id_template)}}" title="Excluir Template" class="excluir-registro">
                  <i class="text-danger fa fa-trash-alt" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
  </div>
</div>
@endsection
