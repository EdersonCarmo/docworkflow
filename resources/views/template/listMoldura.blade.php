@extends('layouts.soged')

@section('content')
<div class="container-fluid">
  <div class="card mb-3">

    <div class="card-header">

      <div>
        <i class="fa fa-file-alt"></i>
        <strong>Lista de molduras cabeçalhos e rodapés</strong>
      </div>

      <div>
        <div class="text-right">
          <a class="btn btn-primary" href="{{route('moldura.create')}}" title="Cadastrar Moldura">
            <i class="text-white fa fa-plus" aria-hidden="true"></i>
          </a>
        </div>
      </div>

    </div>

    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">

          <thead>
            <tr>
              <th>Molduras</th>
              <th>Criação</th>
              <th class="text-center">Ações</th>
            </tr>
          </thead>

          <tbody>
            @foreach ($molduras as $key)
            <tr>
              <td>{{ $key->moldura }}</td>
              <td>{{ date( 'd/m/Y H:m' , strtotime($key->criado)) }}</td>
              <td class="actions">
                <a href="{{route('moldura.edit',$key->id_moldura)}}" title="Alterar moldura">
                  <i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                <a href="{{route('apaga_moldura',$key->id_moldura)}}" title="Excluir moldura" class="excluir-registro">
                  <i class="text-danger fa fa-trash-alt" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>

        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
  </div>
</div>
@endsection
