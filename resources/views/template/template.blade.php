
@extends('layouts.soged')

@section('content')

<div class="container-fluid">

  <div class="card">

    <div class="mb-2 card-header">

     <div class="p-1">
      <strong>{{ isset($template)?"Alterando Tipo de Template":"Cadastrar Tipo de Template"}}</strong>
    </div>

  </div>



  <div class="container">

    <div class="col-12 col-lg-6">

      @if (isset($template))
      <form class="pb-5 form" action="{{route('template.update',$template->id_template)}}" method="post">
        {{ method_field('PUT') }}
        @else
        <form class="pb-5 form" method="POST" action="{{ route('template.store') }}">
          <input type="hidden" name="empresa_id" value="{{\Auth::user()->empresa_id}}">
          @endif
          {{ csrf_field() }}

          <div class="row">

            <div class="form-group col-12">
              <label for="template">Tipo de template</label>
              <input name="template" type="text" class="form-control form-control-sm required" id="template" value="{{$template->template or old('template')}}" required autofocus>
              @if ($errors->has('template'))
              <span class="help-block">
                <strong>{{ $errors->first('template') }}</strong>
              </span>
              @endif
            </div>

          </div>

          @if (!isset($template))
          <button type="submit" class="btn btn-primary salvar-cadastro loader-button">Cadastrar</button>
          @else
          <button type="submit" class="btn btn-primary salvar-cadastro loader-button">Alterar</button>
          @endif  


        </div>

      </form>
    </div>
  </div>

  @endsection
