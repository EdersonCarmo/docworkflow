
@extends('layouts.soged')

@section('content')
  @php
    $texto = '@nome';
  @endphp

  <div class="content-wrapper">
    <div class="container-fluid">

      <div id="form">
      <div>
          <label for="txtFind" >Find:</label>
          <input id="txtFind" type="text" value="@nome" />
      </div>
      <div>
          <label for="txtReplace" >Replace:</label>
          <input id="txtReplace" type="text" value="=====" />
      </div>
      <div>
          <input id="btReplace" type="button" value="Substituir" />
      </div>
  <div>
    <textarea id="content" name="name" rows="8" cols="80">
      Lorem ipsum dolor sit amet, @nome consectetur adipiscing elit. {{ $texto }} Fusce molestie urna
    </textarea>

  <div id="content">
      Teste - Lorem Ipsum
      <p>Lorem ipsum dolor sit amet, @nome consectetur adipiscing elit. Fusce molestie urna dignissim justo iaculis aliquam. Ut auctor sit amet justo laoreet semper. Curabitur hendrerit ornare vulputate. In dictum dui et sem ultricies, id rhoncus nisi dignissim. Suspendisse ac enim sollicitudin, gravida leo eu, fringilla nisl. Suspendisse potenti. Aenean blandit ut purus eget varius. Fusce ornare pretium mauris, placerat viverra est facilisis in. Nunc elementum, leo ac tincidunt luctus, ipsum ipsum laoreet tortor, finibus pulvinar enim erat sit amet mi. Phasellus vulputate fringilla tortor, id tempus urna suscipit id. Vivamus ipsum libero, lacinia quis felis mollis, luctus viverra mi. Praesent id elementum metus, quis porttitor est. Integer condimentum rhoncus eros eget varius. Vestibulum vitae arcu libero.</p>
  </div>
  <button type="button" name="button" onclick="javascript:conversor()">teste</button>

    </div>
  </div>

<script type="text/javascript">
String.prototype.replaceAll = function(de, para){
  var str = this;
  var pos = str.indexOf(de);
  while (pos > -1){
  str = str.replace(de, para);
  pos = str.indexOf(de);
}
  return (str);
}

function conversor(){
  var result=document.getElementById('content').value;
  result.replaceAll('@nome','--------');
}
</script>

<script type="text/javascript">
var content = document.getElementById("content");
var txtFind = document.getElementById("txtFind");
var txtReplace = document.getElementById("txtReplace");
var btReplace = document.getElementById("btReplace");

var replaceText = function (elem, find, replace) {
    for (var indice in elem.childNodes) {
        var childNode = elem.childNodes[indice];
        if (childNode.nodeType == 3) {
            childNode.data = childNode.data.replace(find, replace);
        }

        if (childNode.nodeType == 1) {
            replaceText(childNode, find, replace);
        }
    }
}

btReplace.onclick = function () {
    var find = new RegExp(txtFind.value, 'g');
    var replace = txtReplace.value;

    replaceText(content, find, replace);
}

  function troca(){
    var txt = document.getElementById("ckeditor");
  }
</script>

@endsection
