@extends('layouts.soged')

@section('content')
<div class="container-fluid">
	<div class="card mb-3">

		<div class="card-header">

			<div>
				<i class="fa fa-key"></i>
				<strong>Usuários</strong>
			</div>
			
			<div>
				<div class="text-right">
					<a class="btn btn-primary" href="{{route('usuario.create')}}" title="Cadastrar Usuário">
						<i class="text-white fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
			</div>

		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Email</th>
							<th>Grupo</th>
							<th>Criação</th>
							<th class="text-center">Ações</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($usuarios as $key)
						<tr>
							<td>{{ $key->name }}</td>
							<td>{{ $key->email }}</td>
							<td>{{ $key->grupo['grupo'] }}</td>
							<td>{{ date( 'd/m/Y h:m' , strtotime($key->created_at)) }}</td>
							<td class="actions">
								{{-- <a href="{{route('trocaSenha_usuario',$key->id)}}" title="Gera nova senha" onclick="return confirm('Confirma a alteração de senha?')">
									<i class="fa fa-key text-success" aria-hidden="true"></i>
								</a> --}}
								<a href="{{route('usuario.edit',$key->id)}}" title="Alterar Usuário">
									<i class="fa fa-edit" aria-hidden="true"></i>
								</a>
								<a href="{{route('apaga_usuario',$key->id)}}" title="Excluir Usuário" class="excluir-registro">
									<i class="text-danger fa fa-trash-alt" aria-hidden="true"></i>
								</a>
							</td>
						</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>
		<div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
	</div>
</div>

@if (session('success'))
<script type="text/javascript">
	window.swal({
		type: 'success',
		title: 'Sucesso',
		text: '{{ session("success") }}',
	});
</script>
@endif
@endsection
