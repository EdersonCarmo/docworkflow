@extends('layouts.soged')

@section('content')
@php
$idG = 0;
$idE = 0;
if(isset($usuario)) $idG = $usuario->grupo_id;
if(isset($usuario)) $idE = $usuario->empresa_id;
if(isset($empresa)) $idE = $empresa;
@endphp

<div class="container-fluid">

	<div class="card">
		<div class="mb-2 card-header">

			<div class="p-1">
				<strong>{{ isset($usuario)?"Alterando Usuário":"Cadastrar Usuário"}}</strong>
			</div>

		</div>

		<div class="container">

			<div class="col-12 col-lg-6">

				@if (isset($usuario))
				<form class="pb-5 form" id="usuario_form" action="{{route('usuario.update',$usuario->id)}}" method="post">
					{{ method_field('PUT') }}
					@else
					<form class="pb-5 form" id="form_store" method="POST" action="{{ route('usuario.store') }}">
						@endif
						{{ csrf_field() }}

						<div class="row">

							<div class="form-group col-12">
								<label for="cliente">Nome</label>
								<input id="name" type="text" class="form-control form-control-sm required" name="name" value="{{ $usuario->name or old('name') }}" required autofocus>
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12">
								<label for="email">E-Mail</label>

								<input id="email" type="email" class="form-control form-control-sm required" name="email" value="{{ $usuario->email or old('email') }}" required {{isset($usuario->email) ? 'disabled' : '' }}>
								<small style="display: none" id="email_erro">O email digitado já existe.</small>
							</div>

						</div>

						<div class="row">
							<div class="form-group col-12 col-lg-6">
								<label for="password">Senha</label>
								<input id="password" type="password" class="form-control form-control-sm required" name="password" required>
							</div>

							<div class="form-group col-12 col-lg-6">
								<label for="password-confirm">Confirma Senha</label>
								<input id="password-confirm" type="password" class="form-control form-control-sm required" name="password_confirmation" required>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-12">
								<label for="grupo">Grupo</label>
								<select name="grupo_id" class="form-control required" id="grupo">
									@foreach ($grupos as $key)
									@if ($key->status != 0)											
									<option value="{{$key->id_grupo}}" {{ $idG == $key->id_grupo ? "selected" : ""}}>{{$key->grupo}}</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>

						<div class="row">
							@if (Auth::user()->empresa_id == 1)
							<div class="form-group col-12">
								<label for="empresa">Empresa</label>
								<select name="empresa_id" class="form-control required" id="empresa">
									@foreach ($empresas as $key)
									<option value="{{$key->id_empresa}}" {{ $idE == $key->id_empresa?"selected":""}}>{{$key->empresa}}</option>
									@endforeach
								</select>
							</div>
							@else
							<input type="hidden" name="empresa_id" value="{{Auth::user()->empresa_id}}">
							@endif
						</div>

						<div class="row">

							@isset($usuario)
							<div class="m-3">
								<img src="{{ env('APP_S3').$usuario->foto }}" alt="" width="200">
							</div>
							@endisset

							<div class="">
								<div class="demo-wrap upload-demo form-group">

									<label>{{ isset($usuario)?"Trocar foto":" Foto" }}</label>
									<div>
										<a class="btn file-btn btn-primary">
											<span>Enviar</span>
											<input type="file" id="upload" value="Choose a file" accept="image/*" />
										</a>

										<button type="button" class="upload-result btn btn-secondary">Cortar</button>
										<button type="button" id="remover_marcadagua" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									</div>

									<div class="upload-msg my-3">
										<i class="fa fa-image fa-2x"></i>
									</div>

									<div class="my-3">
										<img src="" width="150" height="150" class="uploaded-img">
									</div>

									<div class="upload-demo-wrap my-3">
										<div id="upload-demo"></div>
									</div>

								</div>
							</div>
						</div>

						<input type="hidden" name="foto" value="" id="foto-cortada">

						@if (!isset($usuario))
						<button type="submit" id="submit_button" class="btn btn-primary loader-button">
							Salvar
						</button>
						@else
						<button type="submit" id="submit_button" class="btn btn-primary loader-button">
							Salvar
						</button>
						@endif

					</div>

				</form>
			</div>
		</div>

		<script>

			var verificar_corte = 0;
			var counter_email = 0;

			$(document).ready(function(){
				var $uploadCrop;

				function readFile(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('.upload-demo').addClass('ready');
							$uploadCrop.croppie('bind', {
								url: e.target.result
							}).then(function(){
							});

						}

						reader.readAsDataURL(input.files[0]);
					}
					else {
						swal("Desculpe, seu navegador não suporta esse recurso");
					}
				}

				$uploadCrop = $('#upload-demo').croppie({
					viewport: {
						width: 150,
						height: 150,
						type: 'square'
					},
					enableExif: true,
					boundary: {
						width: 200,
						height: 200
					}
				});

				$('#upload').on('change', function () {
					readFile(this);
				});

				$('.upload-result').on('click', function (ev) {

					var size = { width: 150, height: 150 };

					$uploadCrop.croppie('result', {
						type: 'canvas',
						size: size
					}).then(function (resp) {

						$('#foto-cortada').val(resp);
						$('.uploaded-img').attr('src', resp);
						$('.uploaded-img').show();
						$('.upload-msg').hide();
						$('.upload-demo').removeClass('ready');
						verificar_corte++;
					});
				});

			});

			$('#submit_button').click(function(e){

				e.preventDefault();

				var counter = 0;

				$('.required').each(function() {

					if($(this).val() == '' && $(this).parent().css('display') == 'block'){
						$(this).css('box-shadow','0 0 8px 0 red');
						counter++;
					}
					else{
						$(this).css('box-shadow','none');
					}

				});

				if(counter_email == 0){
					if(counter !=0){
						window.swal({
							type: 'error',
							title: 'Ops...',
							text: 'Preencha os campos em destaque!',
						});
					}
					else{
						if($('#password').val() === $('#password-confirm').val()){
							if($('#upload').val() == "" || verificar_corte > 0){

								$('.form').submit();
							}
							else{
								swal("Corte a imagem antes de salvar.");
							}
						}
						else{
							swal("As senhas não conferem!");
						}
					}
				}
				else{
					window.swal({
						type: 'error',
						title: 'Ops...',
						text: 'O email já está sendo utilizado!',
					});
				}

				


			});

			$('#remover_marcadagua').click(function(){

				$('.upload-demo').removeClass('ready');
				$('.uploaded-img').hide();
				$('.upload-msg').show();
				$('#foto-cortada').val('');
				$('#upload').val('');
				verificar_corte = 0;

			});

			$('#email').blur(function(){
				$('#email').css('box-shadow','none');
				$('#email_erro').hide();
				$.ajax({
					url:'/retorna_emails_cadastrados',
					type:'GET',
					success: function(result){
						var email = $('#email').val();
						counter_email = 0;

						for (var i = result.length - 1; i >= 0; i--) {
							console.log(result[i].email);
							if(email == result[i].email){
								counter_email++;
							}
						}
						if(counter_email > 0){
							$('#email').css('box-shadow','0 0 8px 0 red');
							$('#email_erro').show();
							counter_email++;
						}

					}
				});
			});


		</script>

		@endsection
