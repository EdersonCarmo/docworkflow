@extends('layouts.soged')

@section('content')
<div class="container-fluid">
  <div class="card mb-3">

    <div class="card-header">

      <div>
        <i class="fa fa-users"></i>
        <strong>Lista de Grupos</strong>
      </div>
      
      <div>
        <div class="text-right">
          <a class="btn btn-primary" href="{{route('grupo.create')}}" title="Cadastrar Grupo">
            <i class="text-white fa fa-plus" aria-hidden="true"></i>
          </a>
        </div>
      </div>

    </div>

    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Criação</th>
              <th class="text-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($grupos as $key)
            <tr>
              <td>{{ $key->grupo }}</td>
              <td>{{ date( 'd/m/Y h:m' , strtotime($key->created_at)) }}</td>
              <td class="actions">
                <a href="{{route('grupo.edit',$key->id_grupo)}}" title="Alterar Usuário"><i class="fa fa-edit" aria-hidden="true"></i>
                </a>
                <a href="{{route('apaga_grupo',$key->id_grupo)}}" title="Excluir Usuário" class="excluir-registro">
                  <i class="text-danger fa fa-trash-alt" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
  </div>
</div>
@endsection
