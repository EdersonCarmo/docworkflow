
@extends('layouts.soged')

@section('content')
  @php
  $uf = 'MG';
  if(isset($endereco->uf)) $uf = $endereco->uf;
  @endphp
<div class="content-wrapper">
  <div class="container-fluid">

    <!-- Example DataTables Card-->
    <div class="p-2 mb-2 bg-primary text-white"><h5>Perfil do Usuário</h5></div>
    <div class="row">

      <div class="col-lg-4">
        <form class="" action="{{route('perfil.update',$usuario->id)}}" method="post">
          <input type="hidden" name="alt_pass" value="0">
          <input type="hidden" name="endereco_id" value="{{$usuario->endereco_id}}">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <div class="form-group input-group-sm">
            <label for="name">Nome</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$usuario->name}}">
            @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group input-group-sm">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" id="email" name="email" value="{{$usuario->email}}">
            @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <label for="logradouro">Logradouro</label>
            <input name="logradouro" type="text" class="form-control form-control-sm" id="logradouro" value="{{$endereco->logradouro or old('logradouro') }}">
            @if ($errors->has('logradouro'))
              <span class="help-block">
                <strong>{{ $errors->first('logradouro') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <label for="complemento">Complemento</label>
            <input name="complemento" type="text" class="form-control form-control-sm" id="complemento" value="{{$endereco->complemento or old('complemento') }}">
            @if ($errors->has('complemento'))
              <span class="help-block">
                <strong>{{ $errors->first('complemento') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <label for="cidade">Cidade</label>
            <input name="cidade" type="text" class="form-control form-control-sm" id="cidade" value="{{$endereco->cidade or old('cidade') }}">
            @if ($errors->has('cidade'))
              <span class="help-block">
                <strong>{{ $errors->first('cidade') }}</strong>
              </span>
            @endif
          </div>
      </div>


              <div class="col-lg-2">

                  <div class="form-group">
                    <label for="celular">Celular</label>
                    <input name="celular" type="text" class="form-control form-control-sm" id="celular" value="{{$usuario->celular or old('') }}">
                    @if ($errors->has('celular'))
                      <span class="help-block">
                        <strong>{{ $errors->first('celular') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="cep">CEP</label>
                    <input name="cep" type="number" class="form-control form-control-sm" id="cep" value="{{$endereco->cep or old('cep') }}">
                    @if ($errors->has('cep'))
                      <span class="help-block">
                        <strong>{{ $errors->first('cep') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="numero">Número</label>
                    <input name="numero" type="text" class="form-control form-control-sm" id="numero" value="{{$endereco->numero or old('numero') }}">
                    @if ($errors->has('numero'))
                      <span class="help-block">
                        <strong>{{ $errors->first('numero') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="bairro">Bairro</label>
                    <input name="bairro" type="text" class="form-control form-control-sm" id="bairro" value="{{$endereco->bairro or old('bairro') }}">
                    @if ($errors->has('bairro'))
                      <span class="help-block">
                        <strong>{{ $errors->first('bairro') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Estado</label>
                    <select name="uf" class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option value="AC" {{$uf == "AC"?"selected":""}}>Acre<option>
                      <option value="AL" {{$uf == "AL"?"selected":""}}>Alagoas<option>
                      <option value="AP" {{$uf == "AP"?"selected":""}}>Amapá<option>
                      <option value="AM" {{$uf == "AM"?"selected":""}}>Amazonas<option>
                      <option value="BA" {{$uf == "BA"?"selected":""}}>Bahia<option>
                      <option value="CE" {{$uf == "CE"?"selected":""}}>Ceara<option>
                      <option value="DF" {{$uf == "DF"?"selected":""}}>Distrito Federal<option>
                      <option value="ES" {{$uf == "ES"?"selected":""}}>Espírito Santo<option>
                      <option value="GO" {{$uf == "GO"?"selected":""}}>Goiás<option>
                      <option value="MA" {{$uf == "MA"?"selected":""}}>Maranhão<option>
                      <option value="MT" {{$uf == "MT"?"selected":""}}>Mato Grosso<option>
                      <option value="MS" {{$uf == "MS"?"selected":""}}>Mato Grosso do Sul<option>
                      <option value="MG" {{$uf == "MG"?"selected":""}}>Minas Gerais<option>
                      <option value="PA" {{$uf == "PA"?"selected":""}}>Pará<option>
                      <option value="PB" {{$uf == "PB"?"selected":""}}>Paraíba<option>
                      <option value="PR" {{$uf == "PR"?"selected":""}}>Paraná<option>
                      <option value="PE" {{$uf == "PE"?"selected":""}}>Pernambuco<option>
                      <option value="PI" {{$uf == "PI"?"selected":""}}>Piauí<option>
                      <option value="RJ" {{$uf == "RJ"?"selected":""}}>Rio de Janeiro<option>
                      <option value="RN" {{$uf == "RN"?"selected":""}}>Rio Grande do Norte<option>
                      <option value="RS" {{$uf == "RS"?"selected":""}}>Rio Grande do Sul<option>
                      <option value="RO" {{$uf == "RO"?"selected":""}}>Rondônia<option>
                      <option value="RR" {{$uf == "RR"?"selected":""}}>Roraima<option>
                      <option value="SC" {{$uf == "SC"?"selected":""}}>Santa Catarina<option>
                      <option value="SP" {{$uf == "SP"?"selected":""}}>São Paulo<option>
                      <option value="SE" {{$uf == "SE"?"selected":""}}>Sergipe<option>
                      <option value="TO" {{$uf == "TO"?"selected":""}}>Tocantins<option>
                    </select>
                  </div>

                  <button type="submit" class="btn btn-primary" onclick="return confirm('Confirma a alteração?')">
                    <i class='fa fa-edit fa-lg' aria-hidden='true'></i> Alterar
                  </button>
                  </form>
                </div>
<!-- IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG -->
      <div class="col-lg-5">
        <img src="{{env('APP_S3').$usuario->foto}}" alt="..." class="img-responsive" width="300" >
        <form class="form-inline" action="{{route('fotoperfil')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="id_user" value="{{Auth::id()}}">

          <input class="form-control" id="image" type="file" name="image" value="" accept=".png, .jpg">

          <button type="submit" class="btn btn-primary" onclick="return confirm('Confirma a alteração?')">Enviar</button>
        </form>
      </div>
<!-- IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG IMG -->
<!-- PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS   -->
    </div>
<br>
    <div class="col-lg-4 p-2 mb-2 bg-primary text-white"><h5>Alterar Senha</h5></div>

    <div class="row">

      <div class="col-lg-4">
        <form class="" action="{{route('perfil.update',$usuario->id)}}" method="post">
          <input type="hidden" name="alt_pass" value="1">
          {{ method_field('PUT') }}
          {{ csrf_field() }}

          <div class="">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="col-md-4 control-label">Senha</label>

              <div class="col-md-12">
                <input id="password" type="password" class="form-control form-control-sm" name="password" placeholder="******" required>

                @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <label for="password-confirm" class="col-md-6 control-label">Confirma Senha</label>

              <div class="col-md-12">
                <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" placeholder="******" required>
              </div>
            </div>
          </div>

          <button type="submit" class="btn btn-primary"><i class='fa fa-edit fa-lg' aria-hidden='true'></i> Alterar</button>
        </form>
      </div>

      <div class="col-lg-6">
      </div>
    </div>
<!-- PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS PASS   -->


  </div>

  <!-- /.container-fluid-->
  @endsection
