@extends('layouts.soged')

@section('content')

@php
$c = $r = $u = $d = $up = $dn = $p = '';
if(isset($permissao)) {
  $updn = $permissao->updown;
  $crud = $permissao->crud;
  if($crud > 7) $d        = 'checked';
  if($crud % 2 != 0) $r   = 'checked';
  if($crud==2 or $crud==3 or $crud==6 or $crud==7 or $crud==10 or $crud==11 or $crud==14 or $crud==15) $c='checked';
  if($crud==4 or $crud==5 or $crud==6 or $crud==7 or $crud==12 or $crud==13 or $crud==14 or $crud==15) $u='checked';
  if($updn > 3) $p        = 'checked';
  if($updn % 2 != 0) $dn  = 'checked';
  if($updn==2 or $updn==3 or $updn==6 or $updn==7 or $updn==10 or $updn==11 or $updn==14 or $updn==15) $up='checked';
}
@endphp

<div class="container-fluid">

  <div class="card">

    <div class="mb-2 card-header">

      <div class="p-1">
        <i class="fa fa-key"></i>
        <strong>{{ isset($grupo)?"Alterando Usuário":"Cadastrar Grupo"}}</strong>
      </div>
      
    </div>

    <div class="container">

      <div class="col-12 col-lg-6">

        @if (isset($grupo))
        <form class="pb-5 form" action="{{route('grupo.update',$grupo->id_grupo)}}" method="post">
          {{ method_field('PUT') }}
          @else
          <form class="pb-5 form" method="POST" action="{{ route('grupo.store') }}">
            @endif
            {{ csrf_field() }}
            <input type="hidden" name="empresa_id" value="{{ Auth::user()->empresa_id }}">

            <div class="row">

              <div class="form-group col-12">
                <label for="grupo">Nome do Grupo </label>
                <input id="grupo" type="text" class="form-control form-control-sm required" name="grupo" value="{{ $grupo->grupo or old('grupo') }}" required autofocus>
                @if ($errors->has('grupo'))
                <span class="help-block">
                  <strong>{{ $errors->first('grupo') }}</strong>
                </span>
                @endif
              </div>

            </div>

            @if (!isset($grupo))
            <button type="submit" class="btn btn-primary salvar-cadastro">
              Cadastrar
            </button>
            @else
            <button type="submit" class="btn btn-primary salvar-cadastro">
              Alterar
            </button>
            @endif 

          </div> 

        </form>
      </div>
    </div>

    @endsection