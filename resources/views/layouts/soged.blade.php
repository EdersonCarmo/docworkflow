<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@component('includes.head')
@endcomponent

<body>

	<div class="loader-overlay">

		<div class="loader"></div>
	</div>

	@component('includes.menu')
	@endcomponent

	<div id="content" class="content-wrapper">   

		@yield('content')    

	</div>


	@component('includes.footer')
	@endcomponent

	<script type="text/javascript">
		$(document).ready(function () {
			$('#sidebarCollapse').on('click', function () {
				$('#sidebar').toggleClass('active');
			});
		});
	</script>

</body>

</html>