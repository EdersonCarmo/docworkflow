@extends('layouts.soged')

@section('content')
@php
$uf = 'MG';
$ck = 0;
if(isset($cliente)){
	$endereco = (object) $cliente->endereco;
	$uf = $endereco->uf;
	$ck = $cliente->status;
}
@endphp

<div class="container-fluid">

	<div class="card">
		<div class="mb-2 card-header">

			<div class="p-1">
				<strong>{{ isset($cliente)?"Alterando Cliente":"Cadastrar Cliente"}}</strong>
			</div>
			
		</div>
		<div class="container">

			<div class="col-12 col-lg-6">

				@if (isset($cliente))
				<form class="pb-5 form" action="{{route('cliente.update',$cliente->id_cliente)}}" method="post">
					{{ method_field('PUT') }}
					@else
					<form class="pb-5 form" method="POST" action="{{ route('cliente.store') }}">
						@endif
						{{ csrf_field() }}

						<div class="btn-group btn-group-toggle my-3">
							<label class="btn btn-dark">
								<input class="rbAtivoInativo" type="radio" name="status" id="rd_PJ" value="2" checked> Pessoa Jurídica
							</label>
							<label class="btn btn-dark">
								<input class="rbAtivoInativo" type="radio" name="status" id="rd_PF" value="1" {{ $ck == 1?"checked":""}}> Pessoa Física &nbsp;
							</label>
						</div>

						<div class="row">

							<div class="form-group col-12">
								<label for="cliente">Nome</label>
								<input name="cliente" type="text" class="form-control form-control-sm required" id="cliente" value="{{$cliente->cliente or old('cliente')}}" required autofocus>
								@if ($errors->has('cliente'))
								<span class="help-block">
									<strong>{{ $errors->first('cliente') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12" id="PJ_razao">
								<label for="razao">Razão Social</label>
								<input name="razao" type="text" class="form-control form-control-sm required" id="razao" value="{{$cliente->razao or old('razao')}}" required>
								@if ($errors->has('razao'))
								<span class="help-block">
									<strong>{{ $errors->first('razao') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12 col-lg-6" id="PJ_cnpj">
								<label for="cnpj">CNPJ/MF</label>
								<input name="cnpj" class="form-control form-control-sm cnpj required" id="cnpj" value="{{$cliente->cnpj or old('cnpj')}}" >
								@if ($errors->has('cnpj'))
								<span class="help-block">
									<strong>{{ $errors->first('cnpj') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group col-12 col-lg-6" id="PJ_ie">
								<label for="ie_rg">Inscrição Estadual</label>
								<input name="ie_rg" class="form-control form-control-sm" id="ie_rg" value="{{$cliente->ie_rg or old('ie_rg')}}">
								@if ($errors->has('ie_rg'))
								<span class="help-block">
									<strong>{{ $errors->first('ie_rg') }}</strong>
								</span>
								@endif
							</div>


							<div class="form-group col-12" id="PF_cpf">
								<label for="cpf">CPF</label>
								<input name="cpf" class="required form-control form-control-sm cpf" id="cpf" value="{{$cliente->cpf or old('cpf')}}" >
								@if ($errors->has('cpf'))
								<span class="help-block">
									<strong>{{ $errors->first('cpf') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12 col-lg-6">
								<label for="telefone">Telefone</label>
								<input name="telefone" class="required form-control form-control-sm telefone" id="telefone" value="{{$cliente->telefone or old('telefone')}}">
								@if ($errors->has('telefone'))
								<span class="help-block">
									<strong>{{ $errors->first('telefone') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group col-12 col-lg-6">
								<label for="celular">Celular</label>
								<input name="celular" class="form-control form-control-sm celular" id="celular" value="{{$cliente->celular or old('celular')}}">
								@if ($errors->has('celular'))
								<span class="help-block">
									<strong>{{ $errors->first('celular') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12">
								<label for="cep">CEP</label>
								<input name="cep" class="form-control form-control-sm cep required" id="cep" value="{{$cliente->endereco->cep or old('cep')}}" required>
								@if ($errors->has('cep'))
								<span class="help-block">
									<strong>{{ $errors->first('cep') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12 col-lg-10">
								<label for="logradouro">Logradouro</label>
								<input name="logradouro" type="text" class="form-control form-control-sm required" id="logradouro" value="{{$cliente->endereco->logradouro or old('logradouro')}}" required>
								@if ($errors->has('logradouro'))
								<span class="help-block">
									<strong>{{ $errors->first('logradouro') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group col-12 col-lg-2">
								<label for="numero">Nº</label>
								<input name="numero" type="text" class="form-control form-control-sm required" id="numero" value="{{$cliente->endereco->numero or old('numero')}}" required>
								@if ($errors->has('numero'))
								<span class="help-block">
									<strong>{{ $errors->first('numero') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12 col-lg-4">
								<label for="complemento">Complemento</label>
								<input name="complemento" type="text" class="form-control form-control-sm" id="complemento" value="{{$cliente->endereco->complemento or old('complemento')}}">
								@if ($errors->has('complemento'))
								<span class="help-block">
									<strong>{{ $errors->first('complemento') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group col-12 col-lg-8">
								<label for="bairro">Bairro</label>
								<input name="bairro" type="text" class="form-control form-control-sm required" id="bairro" value="{{$cliente->endereco->bairro or old('bairro')}}" required>
								@if ($errors->has('bairro'))
								<span class="help-block">
									<strong>{{ $errors->first('bairro') }}</strong>
								</span>
								@endif
							</div>

						</div>

						<div class="row">

							<div class="form-group col-12 col-lg-8">
								<label for="cidade">Cidade</label>
								<input name="cidade" type="text" class="form-control form-control-sm required" id="cidade" value="{{$cliente->endereco->cidade or old('cidade')}}" required>
								@if ($errors->has('cidade'))
								<span class="help-block">
									<strong>{{ $errors->first('cidade') }}</strong>
								</span>
								@endif
							</div>


							<div class="form-group col-12 col-lg-4">
								<label for="exampleFormControlSelect1">Estado</label>
								<select name="uf" class="form-control" id="uf">
									<option value="AC" {{$uf == "AC"?"selected":""}}>Acre</option>
									<option value="AL" {{$uf == "AL"?"selected":""}}>Alagoas</option>
									<option value="AP" {{$uf == "AP"?"selected":""}}>Amapá</option>
									<option value="AM" {{$uf == "AM"?"selected":""}}>Amazonas</option>
									<option value="BA" {{$uf == "BA"?"selected":""}}>Bahia</option>
									<option value="CE" {{$uf == "CE"?"selected":""}}>Ceara</option>
									<option value="DF" {{$uf == "DF"?"selected":""}}>Distrito Federal</option>
									<option value="ES" {{$uf == "ES"?"selected":""}}>Espírito Santo</option>
									<option value="GO" {{$uf == "GO"?"selected":""}}>Goiás</option>
									<option value="MA" {{$uf == "MA"?"selected":""}}>Maranhão</option>
									<option value="MT" {{$uf == "MT"?"selected":""}}>Mato Grosso</option>
									<option value="MS" {{$uf == "MS"?"selected":""}}>Mato Grosso do Sul</option>
									<option value="MG" {{$uf == "MG"?"selected":""}}>Minas Gerais</option>
									<option value="PA" {{$uf == "PA"?"selected":""}}>Pará</option>
									<option value="PB" {{$uf == "PB"?"selected":""}}>Paraíba</option>
									<option value="PR" {{$uf == "PR"?"selected":""}}>Paraná</option>
									<option value="PE" {{$uf == "PE"?"selected":""}}>Pernambuco</option>
									<option value="PI" {{$uf == "PI"?"selected":""}}>Piauí</option>
									<option value="RJ" {{$uf == "RJ"?"selected":""}}>Rio de Janeiro</option>
									<option value="RN" {{$uf == "RN"?"selected":""}}>Rio Grande do Norte</option>
									<option value="RS" {{$uf == "RS"?"selected":""}}>Rio Grande do Sul</option>
									<option value="RO" {{$uf == "RO"?"selected":""}}>Rondônia</option>
									<option value="RR" {{$uf == "RR"?"selected":""}}>Roraima</option>
									<option value="SC" {{$uf == "SC"?"selected":""}}>Santa Catarina</option>
									<option value="SP" {{$uf == "SP"?"selected":""}}>São Paulo</option>
									<option value="SE" {{$uf == "SE"?"selected":""}}>Sergipe</option>
									<option value="TO" {{$uf == "TO"?"selected":""}}>Tocantins</option>
								</select>
							</div>

						</div>


						@if (!isset($cliente))
						<button type="submit" class="btn btn-primary salvar-cadastro loader-button">
							Cadastrar
						</button>
						@else
						<button type="submit" class="btn btn-primary salvar-cadastro loader-button">
							Alterar
						</button>
						@endif
						
					</div>

				</form>

			</div>

		</div>



		@endsection
