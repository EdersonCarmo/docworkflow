@extends('layouts.soged')

@section('content')
<div class="container-fluid">
	<div class="card mb-3">

		<div class="card-header">

			<div>
				<i class="fa fa-users"></i>
				<strong>Clientes</strong>
			</div>
			
			@if(Auth::user()->grupo_id <= 2)
			<div>
				<div class="text-right">
					<a class="btn btn-primary" href="{{route('cliente.create')}}" title="Cadastrar Cliente">
						<i class="text-white fa fa-plus" aria-hidden="true"></i>
					</a>
				</div>
			</div>
			@endif

		</div>

		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Empresas</th>
							<th>Telefone</th>
							<th>Cidade</th>
							<th>Rua</th>
							@if(Auth::user()->grupo_id <= 2)
							<th class="text-center">Ações</th>
							@endif
						</tr>
					</thead>
					<tbody>
						@foreach ($clientes as $item)
						@php
						$teste = (object) $item->endereco;
						@endphp
						<tr>
							<td>{{ $item->cliente }}</td>
							<td>{{ $item->telefone }}</td>
							<td>{{ $teste->cidade }}</td>
							<td>{{ $teste->logradouro }}</td>
							@if(Auth::user()->grupo_id <= 2)
							<td class="actions">
								<a href="{{route('cliente.edit',$item->id_cliente)}}">
									<i class="fa fa-edit" aria-hidden="true"></i>
								</a>
								<a href="{{route('apaga_cliente',$item->id_cliente)}}" class="excluir-registro" title="Excluir Cliente">
									<i class="text-danger fa fa-trash-alt" aria-hidden="true"></i>
								</a>
							</td>
							@endif
						</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>
		<div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
	</div>
</div>
@endsection
