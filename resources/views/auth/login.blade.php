@extends('layouts.app')

@section('content')
<div id="login-screen">
	<div class="container">
		<div class="row justify-content-center align-center">

			<div class="col-md-6 col-xs-12">
				<div>
					<div class="panel-body">

						<div class="img-fluid">
							<img src="{{URL::asset('image/soged-horizontal.png')}}">
						</div>
						
						<form class="form-horizontal" method="POST" action="{{ route('login') }}">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email" class="control-label">Login</label>

								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

								@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password" class="control-label">Senha</label>

								<input id="password" type="password" class="form-control" name="password" required>

								@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
								@endif
							</div>

							<div class="form-group justify-content-center row button-group">

								<div class="col-xs-4 col-sm-4">
									<button type="submit" class="btn btn-link">
										Entrar
									</button>
								</div>

								<div class="col-xs-4 col-sm-4">
									<a class="btn btn-link" href="{{ route('password.request') }}">
										Esqueci a senha
									</a>
								</div>
								

								

							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">

				<div class="panel">
					<h1>SOGED</h1>
					<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam arcu lorem, euismod et aliquet vitae, vulputate sit amet est. Aliquam erat volutpat. Proin eu pharetra augue, eget imperdiet velit. Aenean sed elit erat. Integer venenatis massa mi, sed ornare dui consequat et. Etiam condimentum arcu nisl, et vehicula dui congue eget. Vestibulum vitae urna eget augue semper euismod sed a massa. Mauris id nisl ultricies, porttitor neque in, vulputate dolor. Vivamus lectus velit, commodo nec aliquam ut, sagittis ac lacus. Curabitur interdum dictum leo sed dictum. Praesent cursus ornare augue, suscipit malesuada enim fermentum vitae. Etiam laoreet est sed hendrerit lobortis. Duis ultricies, purus sed consectetur mattis, urna est congue ligula, a suscipit sem risus eget tellus.</p>	
				</div>
				
			</div>

		</div>
	</div>
</div>
@endsection
