@extends('layouts.soged')

@section('content')

<div class="container-fluid">

	<div class="card">
		<div class="mb-2 card-header">

			<div class="p-1">
				<i class="fa fa-file-alt"></i>
				<strong>Cadastrar Cabeçalho e Rodapé</strong>
			</div>

		</div>

		<div class="container">

			<form class="pb-5 form" action="#" method="post">

				<div class="form-group">
					<label for="exampleTextarea">Nome do modelo</label>
					<input type="text" class="form-control" name="moldura">
				</div>

				<div class="form-group">
					<label for="exampleTextarea">Cabeçalho</label>
					<textarea class="form-control"  id="ckeditor" name="cabecalho" rows="3"></textarea>
				</div>

				<div class="form-group">
					<label for="exampleTextarea">Rodapé</label>
					<textarea class="form-control"  id="ckeditor2" name="rodape" rows="3"></textarea>
				</div>

				<input type="hidden" name="background" value="" id="marca-dagua">

				<div class="demo-wrap upload-demo form-group">

					<label>Marca d'água</label>
					<div>
						<a class="btn file-btn btn-primary">
							<span>Upload</span>
							<input type="file" id="upload" value="Choose a file" accept="image/*" />
						</a>

						<button type="button" class="upload-result btn btn-secondary">Cortar</button>
						<button type="button" id="remover_marcadagua" class="btn btn-danger"><i class="fa fa-trash"></i></button>
					</div>

					<div class="upload-msg my-3">
						<i class="fa fa-image fa-2x"></i>
					</div>

					<div class="my-3">
						<img src="" width="248" height="350" class="uploaded-img">
					</div>

					<div class="upload-demo-wrap my-3">
						<div id="upload-demo"></div>
					</div>

					<small>Escolha imagens com tamanho mínimo de 2480 x 3508 para obter melhores resultados.</small>

				</div>

				<button class="btn btn-primary" type="button" id="submit">Salvar</button>

			</form>

		</div>

	</div>

</div>

<script>
	CKEDITOR.replace( 'ckeditor' );
	CKEDITOR.replace( 'ckeditor2' );

	var verificar_corte = 0;

	$(document).ready(function(){
		var $uploadCrop;

		function readFile(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('.upload-demo').addClass('ready');
					$uploadCrop.croppie('bind', {
						url: e.target.result
					}).then(function(){
					});

				}

				reader.readAsDataURL(input.files[0]);
			}
			else {
				swal("Desculpe, seu navegador não suporta esse recurso");
			}
		}

		$uploadCrop = $('#upload-demo').croppie({
			viewport: {
				width: 248,
				height: 350,
				type: 'square'
			},
			enableExif: true,
			boundary: {
				width: '100%',
				height: 450
			}
		});

		$('#upload').on('change', function () {
			readFile(this);
		});

		$('.upload-result').on('click', function (ev) {

			var size = { width: 2480, height: 3508 };

			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: size
			}).then(function (resp) {
				$('#marca-dagua').val(resp);
				$('.uploaded-img').attr('src', resp);
				$('.uploaded-img').show();
				$('.upload-msg').hide();
				$('.upload-demo').removeClass('ready');
				verificar_corte++;
			});
		});

	});

	$('#submit').click(function(){

		if($('#upload').val() == "" || verificar_corte > 0){
			$('form').submit();
		}
		else{
			swal("Corte a imagem antes de salvar.");
		}

	});

	$('#remover_marcadagua').click(function(){

		$('.upload-demo').removeClass('ready');
		$('.uploaded-img').hide();
		$('.upload-msg').show();
		$('#marca-dagua').val('');
		$('#upload').val('');
		verificar_corte = 0;

	});


</script>

@endsection
