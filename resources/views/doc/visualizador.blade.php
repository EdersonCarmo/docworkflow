@extends('layouts.soged')

@section('content')

<div class="content-wrapper">
  <div class="container-fluid">
    <div class="fundo">

      <div style="background-image:url(https://s3.us-east-1.amazonaws.com/soged-arq/empresas/4/4p34/TEMPLATES/FundoDocSoitic02.png); background-size:100% 1000px">
      <h1>&nbsp;</h1>

      <h1>&nbsp;</h1>

      <h1 style="text-align:center">&nbsp;<span style="font-size:14px"><strong>CONTRATO DE LICEN&Ccedil;A DE USO E PRESTA&Ccedil;&Atilde;O DE SERVI&Ccedil;O DE SOFTWARE</strong></span>&nbsp;</h1>

      <p>&nbsp;</p>

      <p>&nbsp;</p>

      <p><strong>Manage,</strong> qualificar (PF) portador do RG n&ordm; **** e inscrito no CPF sob n&ordm; **** Endere&ccedil;o completo ****** n&ordm; ***&nbsp; bairro, CEP***, (cidade e estado), de ora em diante</p>

      <p>simplesmente denominada &ldquo;Licenciado&rdquo; .</p>

      <p>&nbsp;</p>

      <p><strong>Manage</strong> empresa inscrita sob o CNPJ/MF n&ordm; 00.568.073/0001-84, inscri&ccedil;&atilde;o estadual: isenta com sede na Rua Paran&aacute;, n&ordm; 497, bairro Funcion&aacute;rios,</p>

      <p>CEP n&ordm; 37.713-047, na cidade de Po&ccedil;os de Caldas-MG,&nbsp; telefones: 0800-707 5900, 0800-704 1133, de ora em diante &nbsp;denominada simplesmente como</p>

      <p>&ldquo;<strong>MEDSYSTEM S/V&rdquo;</strong> ou &ldquo;Licenciante&rdquo;.</p>

      <p>&nbsp;</p>

      <p>Contratante e Medsystem S/V doravante denominadas em conjunto &ldquo;Partes&rdquo; e individualmente &ldquo;Parte&rdquo;.</p>

      <p>&nbsp;</p>

      <h1>Celebram as Partes o presente Contrato de Licen&ccedil;a de Uso e Presta&ccedil;&atilde;o de</h1>

      <h1>Servi&ccedil;o de Software (&ldquo;Contrato&rdquo;), que se reger&aacute; pelas cl&aacute;usulas e condi&ccedil;&otilde;es a</h1>

      <h1>seguir estipuladas:</h1>

      <p>&nbsp;</p>

      <p><strong>CONSIDERA&Ccedil;&Otilde;ES PRESCEDENTES:</strong></p>

      <p>&nbsp;</p>

      <p>S&atilde;o considera&ccedil;&otilde;es precedentes (as &ldquo;Condi&ccedil;&otilde;es&rdquo;), a serem cumpridas pelas&nbsp;Partes&nbsp;para a Presta&ccedil;&atilde;o dos Servi&ccedil;os:</p>

      <p>&nbsp;</p>

      <ol>
      	<li>A <strong>MEDSYSTEM S/V</strong> &eacute; empresa do seguimento da Tecnologia da Informa&ccedil;&atilde;o e Comunica&ccedil;&atilde;o (TIC) desenvolvedora de softwares destinados &agrave; gest&atilde;o nas</li>
      	<li>&aacute;reas da Medicina e Sa&uacute;de;</li>
      	<li>A <strong>MEDSYSTEM S/V</strong> por meio de seu site disponibiliza a exposi&ccedil;&atilde;o de seus softwares contendo dados importantes sobre suas caracter&iacute;sticas t&eacute;cnicas,</li>
      	<li>funcionais e operacionais disponibilizando amplo acesso via e-mail, chats, 0800, Espa&ccedil;o VIP, contando com pessoal especialista para apresenta&ccedil;&otilde;es remotas</li>
      	<li>pela utiliza&ccedil;&atilde;o dos recursos da TIC visando a aproxima&ccedil;&atilde;o com os clientes colaboradores possibilitando a retirada de d&uacute;vidas e prestando esclarecimentos referentes aos softwares, &agrave; proposta t&eacute;cnica comercial e ao contrato;&nbsp;</li>
      	<li>O <strong>CONTRATANTE </strong>se interessa na utiliza&ccedil;&atilde;o do software e presta&ccedil;&atilde;o de servi&ccedil;os decorrentes para instrumentalizar e incrementar sua atividade profissional lucrativa;</li>
      	<li>O <strong>CONTRATANTE </strong>leu e compreendeu os termos e condi&ccedil;&otilde;es comerciais e t&eacute;cnicas al&eacute;m dos requisitos de ordem tecnol&oacute;gica ligados ao ambiente</li>
      	<li>computacional no qual o <em>software</em> ser&aacute; instalado incluindo dentre outros, sistema operacional, rede, Internet conforme discriminado tanto na proposta t&eacute;cnica comercial como neste contrato tendo o <strong>CONTRATANTE</strong> plenas condi&ccedil;&otilde;es de entender o alcance e implica&ccedil;&otilde;es da contrata&ccedil;&atilde;o aceitando-a ou recusando-a</li>
      	<li>antes de prestar o seu aceite;</li>
      	<li>As<strong> Partes </strong>concordam e aceitam a contrata&ccedil;&atilde;o virtual na forma de <strong>Contrato Eletr&ocirc;nico</strong> cuja manifesta&ccedil;&atilde;o de vontade e cujo consentimento se materializam como se houvessem sido exteriorizadas por qualquer outro meio tradicional, sendo consideradas inequ&iacute;vocas, instrumentalizando e aperfei&ccedil;oando o v&iacute;nculo contratual produzindo efic&aacute;cia e validade jur&iacute;dicas nos termos da legisla&ccedil;&atilde;o brasileira em vigor nos termos dos artigos 422 e 425 do C&oacute;digo Civil;</li>
      	<li>A <strong>MEDSYSTEM S/V</strong> atrav&eacute;s dos recursos da TIC &eacute; dotada de meios que lhe permitem a rastreabilidade, aferi&ccedil;&atilde;o e arquivamento dos aceites que forem dados eletronicamente pelo <strong>LICENCIADO</strong> para fins de comprova&ccedil;&atilde;o do v&iacute;nculo contratual;</li>
      </ol>

      <h1><strong>Cl&aacute;usula Primeira &ndash; DO OBJETO</strong></h1>

      <p>&nbsp;</p>

      <ol>
      	<li>
      	<ol>
      		<li>O presente instrumento tem por objeto a <strong>Licen&ccedil;a de Uso e Presta&ccedil;&atilde;o de Servi&ccedil;o</strong> da <strong>MEDSYSTEM S/V</strong> ao <strong>LICENCIADO</strong> em car&aacute;ter <strong>oneroso</strong>, <strong>intransfer&iacute;vel</strong>, <strong>n&atilde;o exclusivo</strong> e <em>intuitu personae</em>, do (s) seguinte (s) Sistema (s) de Solu&ccedil;&otilde;es (software (s)):</li>
      	</ol>
      	</li>
      </ol>

      <p>&nbsp;</p>

      <p><strong>********SOFTWARE CONSTANTE NA PORPOSTA TECNICA/COMERCIAL************** </strong></p>

      <p>&nbsp;</p>

      <ol>
      	<li>O <strong>LICENCIADO </strong>poder&aacute; instalar, usar, acessar, exibir e executar o (s) referido software (s) em computadores, desde que o servidor e as esta&ccedil;&otilde;es de trabalho sejam para uso pr&oacute;prio e exclusivo. <strong>A Licen&ccedil;a de Uso n&atilde;o</strong> <strong>constitui, em hip&oacute;tese nenhuma, uma venda do software, mas sim um licenciamento de uso, para utiliza&ccedil;&atilde;o do LICENCIADO.</strong></li>
      </ol>

      <p>&nbsp;</p>

      <h2>Cl&aacute;usula SEGUNDA &ndash; DA VIG&Ecirc;NCIA</h2>

      <p>&nbsp;</p>

      <ol>
      	<li>
      	<ol>
      		<li>O presente contrato, bem como a Validade T&eacute;cnica do software ter&atilde;o vig&ecirc;ncia pe<strong>lo prazo de 12 (doze) meses</strong> a contar da data de seu aceite que equivaler&aacute; como se fosse sua assinatura, <strong>sendo automaticamente renovado a menos que uma Parte notifique a outra com anteced&ecirc;ncia m&iacute;nima de&nbsp;30 (trinta) dias</strong> em rela&ccedil;&atilde;o a qualquer per&iacute;odo de vig&ecirc;ncia.&nbsp;</li>
      		<li>Os prazos que disserem respeito &agrave; implanta&ccedil;&atilde;o, capacita&ccedil;&atilde;o e outros que se fizerem necess&aacute;rios para o cumprimento do objeto deste contrato, ser&atilde;o previamente discutidos entre as Partes, analisado caso a caso de acordo com a complexidade da presta&ccedil;&atilde;o dos servi&ccedil;os juntamente com a disponibilidade de pessoal pela <strong>MEDSYSTEM S/V </strong>para a realiza&ccedil;&atilde;o do mesmo e documentado pela proposta t&eacute;cnica e comercial;</li>
      		<li>Ao t&eacute;rmino de cada per&iacute;odo de 12 (doze) meses o <strong>LICENCIADO</strong> pode escolher em aceitar as atualiza&ccedil;&otilde;es anuais disponibilizadas pela <strong>MEDSYSTEM S/V</strong> atrav&eacute;s de Upgrades, ou simplesmente n&atilde;o desejar mais utilizar o sistema e consequentemente sua assist&ecirc;ncia.</li>
      	</ol>
      	</li>
      </ol>

      <p>Par&aacute;grafo &Uacute;nico: A rescis&atilde;o antecipada ensejar&aacute; as consequ&ecirc;ncias previstas neste Contrato.</p>

      <p>&nbsp;</p>

      <p><strong>cl&aacute;usula terceira &ndash; DA REMUNERA&Ccedil;&Atilde;O</strong></p>

      <p>&nbsp;</p>

      <ol>
      	<li>
      	<ol>
      		<li>A <strong>MEDSYSTEM S/V</strong>&nbsp;far&aacute; jus as remunera&ccedil;&otilde;es conforme discriminadas abaixo nos termos constantes na proposta t&eacute;cnica/comercial, lida e aceita pelo <strong>LICENCIADO</strong>:</li>
      	</ol>
      	</li>
      	<li>O valor da remunera&ccedil;&atilde;o pela <strong>Licen&ccedil;a de Uso do Sistema </strong>a <strong>MEDSYSTEM S/V</strong> &eacute; de R$*** (******) que ser&aacute; dividida em XXX (**) parcelas mensais e sucessivas de R$****** (***), a iniciar-se em **/**/*****.</li>
      	<li>O valor da remunera&ccedil;&atilde;o pela <strong>Presta&ccedil;&atilde;o de Servi&ccedil;os </strong>do (s) software (s), evolu&ccedil;&atilde;o, suporte t&eacute;cnico dar-se-&aacute; por meio de boleto banc&aacute;rio no valor mensal de R$*** (******) a iniciar-se em **/**/*****.
      	<ol>
      		<li>A efetiva&ccedil;&atilde;o da Remunera&ccedil;&atilde;o dar-se-&aacute; todo dia *** (**) de cada m&ecirc;s durante a vig&ecirc;ncia deste Contrato.</li>
      		<li>Os boletos banc&aacute;rios que alude a cl&aacute;usula 3.1, itens i e ii ser&atilde;o disponibilizados na forma eletr&ocirc;nica pela <strong>MEDSYSTEM S/V</strong> atualmente no<strong> Espa&ccedil;o Vip</strong> (www.vip.soitic.com), conforme data de vencimento escolhida pelo <strong>LICENCIADO.</strong></li>
      	</ol>
      	</li>
      </ol>

      <ol>
      	<li>A inadimpl&ecirc;ncia implicar&aacute; na suspens&atilde;o de todos os direitos do <strong>LICENCIADO</strong>, inclusive:</li>
      </ol>

      <ol>
      	<li>n&atilde;o libera&ccedil;&atilde;o da contrassenha mantida como forma de prote&ccedil;&atilde;o do (s) software(s) estabelecida na cl&aacute;usula 4.2, submeter o <strong>LICENCIADO </strong>&agrave;s infra&ccedil;&otilde;es e penalidades descritas no Cap&iacute;tulo V da Lei 9.609/98 (Lei de Software), sem preju&iacute;zo das penalidades previstas neste contrato;</li>
      	<li>bloqueio/suspens&atilde;o imediato dos servi&ccedil;os prestados pela MEDSYSTEM S/V, no que concerne ao software objeto do presente instrumento, bem como os demais servi&ccedil;os por ele habitualmente prestados ao <strong>LICENCIADO</strong>;</li>
      </ol>

      <ol>
      	<li>O atraso no pagamento da Remunera&ccedil;&atilde;o importar&aacute; na aplica&ccedil;&atilde;o de multa de mora de 2% (dois por cento) sobre o valor devido, bem como de juros morat&oacute;rios de 1% (um por cento) ao m&ecirc;s, sujeitos a Protesto e Inscri&ccedil;&atilde;o do nome do <strong>LICENCIADO</strong> nos &oacute;rg&atilde;os de prote&ccedil;&atilde;o ao cr&eacute;dito;</li>
      	<li>Havendo o pagamento, a <strong>MEDSYSTEM S/V</strong> tem um prazo de at&eacute; 03 (tr&ecirc;s) dias &uacute;teis para reativa&ccedil;&atilde;o dos direitos do <strong>LICENCIADO</strong>, bem como a libera&ccedil;&atilde;o do sistema, com a expedi&ccedil;&atilde;o da Carta de Anu&ecirc;ncia, responsabilizando-se o devedor interessado pelas custas provenientes do protesto do t&iacute;tulo conforme consta da Lei.</li>
      	<li>Eventualmente e em casos espec&iacute;ficos, pode a <strong>MEDSYSTEM S/V</strong>, a seu crit&eacute;rio, autorizar o pagamento de boletos em atraso por meio de dep&oacute;sito banc&aacute;rio. Nesses casos, o <strong>LICENCIADO, </strong>ap&oacute;s realizar o dep&oacute;sito<strong>, </strong>dever&aacute; enviar c&oacute;pia do comprovante de dep&oacute;sito banc&aacute;rio identificado com seu C&oacute;digo<strong> </strong>Cadastral a <strong>MEDSYSTEM S/V </strong>por e-mail, desde que leg&iacute;veis os dados<strong>, </strong>para que seja feita a baixa do referido boleto.</li>
      </ol>

      <ol>
      	<li><strong>MEDSYSTEM S/V </strong>n&atilde;o se responsabiliza pela baixa do boleto banc&aacute;rio se o <strong>LICENCIADO</strong> n&atilde;o cumprir com sua parte de enviar o comprovante de dep&oacute;sito identificado com seu c&oacute;digo cadastral a empresa <strong>MEDSYSTEM S/V, </strong>ficando essa &uacute;ltima isenta de qualquer responsabilidade com rela&ccedil;&atilde;o &agrave; baixa por n&atilde;o conhecer a identifica&ccedil;&atilde;o de quem efetuou o dep&oacute;sito.

      	<ol>
      		<li>O valor referente ao licenciamento e a presta&ccedil;&atilde;o de servi&ccedil;os do software ser&aacute; reajustada anualmente, no m&ecirc;s de mar&ccedil;o, pela varia&ccedil;&atilde;o do IPCA-IBGE ou pelo &iacute;ndice oficial que que melhor reflita a infla&ccedil;&atilde;o. No caso de extin&ccedil;&atilde;o do IPCA-IBGE ser&aacute; utilizado o &iacute;ndice que que vier a ser adotado a fim de substitu&iacute;-lo.&nbsp;</li>
      		<li>No valor das Remunera&ccedil;&otilde;es descritas na Cl&aacute;usula 3.1 itens i e ii&nbsp; n&atilde;o incluem valores relativos a capacita&ccedil;&otilde;es in loco e/ou capacita&ccedil;&otilde;es remotas, nem hora t&eacute;cnica, deslocamento, hospedagem e alimenta&ccedil;&atilde;o de funcion&aacute;rios da <strong>MEDSYSTEM S/V</strong> na realiza&ccedil;&atilde;o de servi&ccedil;os prestados ao <strong>LICENCIADO</strong>, n&atilde;o estando inclu&iacute;das tamb&eacute;m customiza&ccedil;&otilde;es e/ou adequa&ccedil;&otilde;es solicitadas pelo <strong>LICENCIADO,</strong> assim como personaliza&ccedil;&atilde;o ou configura&ccedil;&atilde;o de modelos e/ou impressos, laudos, rotinas, prescri&ccedil;&otilde;es, dentre outros, que ser&atilde;o cobrados em separado por negocia&ccedil;&atilde;o pr&eacute;via entre as Partes de acordo com a complexidade envolvida.</li>
      	</ol>
      	</li>
      </ol>

      <h2>Cl&aacute;usula QUARTA &ndash; DA RESERVA DE DIREITOS E DE PROPRIEDADE</h2>

      <p>&nbsp;</p>

      <ol>
      	<li>
      	<ol>
      		<li>O software &eacute; de propriedade da <strong>MEDSYSTEM S/V</strong> que reserva todos os direitos que n&atilde;o foram expressamente concedidos ao <strong>CONTRATANTE</strong> neste contrato. O software descrito neste contrato &eacute; protegido por leis de direitos autorais e outras leis e tratados de propriedade intelectual. A titularidade, direitos autorais e outros direitos de propriedade intelectual relativos ao Sistema de Solu&ccedil;&otilde;es s&atilde;o de propriedade da <strong>MEDSYSTEM S/V</strong>. O software deste contrato &eacute; licenciado e n&atilde;o vendido. A licen&ccedil;a de uso &eacute; intransfer&iacute;vel o que significa que o <strong>CONTRATANTE</strong> n&atilde;o tem permiss&atilde;o para vender, alugar, arrendar, emprestar, ceder, transferir, sublicenciar a ningu&eacute;m.</li>
      		<li>A partir do momento da aceita&ccedil;&atilde;o deste contrato, o software, como meio de prote&ccedil;&atilde;o e a cada ano, interromper&aacute; automaticamente, tendo como forma de libera&ccedil;&atilde;o CONTRA-SENHA a ser passada pela <strong>MEDSYSTEM S/V</strong>.</li>
      		<li>Todos os direitos patrimoniais, logotipos, marcas ins&iacute;gnias, sinais distintivos, documenta&ccedil;&atilde;o t&eacute;cnica associada e quaisquer outros materiais correlatos constituem, conforme o caso, direitos autorais, segredos comerciais, e/ou direitos de propriedade da <strong><em>MEDSYSTEM S/V</em></strong>, sendo tais direitos protegidos pela legisla&ccedil;&atilde;o nacional e internacional aplic&aacute;vel &agrave; propriedade intelectual.</li>
      		<li>Qualquer c&oacute;pia do sistema, no todo ou em parte, bem como a reprodu&ccedil;&atilde;o do mesmo pelo <strong>CONTRATANTE</strong>, sem pr&eacute;via e expressa autoriza&ccedil;&atilde;o da <strong><em>MEDSYSTEM S/V</em></strong>, ser&aacute; considerada c&oacute;pia n&atilde;o autorizada e, sua mera exist&ecirc;ncia ser&aacute; considerada como viola&ccedil;&atilde;o aos direitos de propriedade da <strong><em>MEDSYSTEM S/V</em></strong>, sujeitando-se o <strong>CONTRATANTE</strong> &agrave;s penalidades previstas no presente Contrato assim como na legisla&ccedil;&atilde;o em vigor.</li>
      		<li>A viola&ccedil;&atilde;o das regras de licenciamento de uso &eacute; causa de rescis&atilde;o contratual sujeitando o <strong>CONTRATANTE</strong> &agrave;s penalidades contratuais e legais inclusive repara&ccedil;&atilde;o por perdas e danos.</li>
      	</ol>
      	</li>
      </ol>

      <p>&nbsp;</p>

      <p><strong>Cl&aacute;usula QUINTA &ndash; DAs OBRIGA&Ccedil;&Otilde;ES dA MEDSYSTEM S/v</strong></p>

      <p>&nbsp;</p>

      <p>5.1. &nbsp;&nbsp;&nbsp;&nbsp; Sem preju&iacute;zo de outras obriga&ccedil;&otilde;es previstas neste Contrato, em lei e/ou inerentes ao Uso da Licen&ccedil;a e a Presta&ccedil;&atilde;o dos Servi&ccedil;os, a <strong>MEDSYSTEM S/V</strong>&nbsp;compromete-se, durante a vig&ecirc;ncia deste Contrato a:</p>

      <ol>
      	<li>solucionar eventuais falhas estruturais pr&oacute;prias do desenvolvimento do Sistema de Solu&ccedil;&otilde;es (software) disponibilizando todo o suporte t&eacute;cnico necess&aacute;rio ao <strong>LICENCIADO</strong> dentro do prazo de Validade T&eacute;cnica do sistema envidando seus melhores esfor&ccedil;os e a ado&ccedil;&atilde;o das melhores pr&aacute;ticas e t&eacute;cnicas para que a resolu&ccedil;&atilde;o seja implementada dentro do menor prazo poss&iacute;vel, observada a complexidade caso a caso.</li>
      	<li>preservar na assist&ecirc;ncia, sempre que poss&iacute;vel, os dados e informa&ccedil;&otilde;es adquiridas e arquivadas na utiliza&ccedil;&atilde;o do software pelo <strong>LICENCIADO</strong>;</li>
      	<li>manter o sistema de solu&ccedil;&otilde;es atualizado e desenvolver Upgrades anuais, bem como evoluir as linguagens de programa&ccedil;&atilde;o, ambientes operacionais e legisla&ccedil;&atilde;o, desde que haja necessidade, respeitadas e atendidas dentro do que for poss&iacute;vel, &agrave;s observa&ccedil;&otilde;es e cr&iacute;ticas do <strong>LICENCIADO</strong> e <strong>desde que seja respeitada e atendida a velocidade m&iacute;nima REAL de Internet de 2MB.</strong></li>
      	<li>disponibilizar, com a devida e total seguran&ccedil;a, todos os meios de comunica&ccedil;&atilde;o necess&aacute;rios, inclusive 0800 e acesso remoto, para a aproxima&ccedil;&atilde;o, solu&ccedil;&atilde;o de quest&otilde;es e transfer&ecirc;ncia de dados entre as partes contratantes, em dias &uacute;teis exceto feriados nacionais, de segunda a sexta-feira, no hor&aacute;rio comercial das 08 &agrave;s 18 horas, nos s&aacute;bados das 09 &agrave;s 12 horas, incluindo plant&atilde;o via celular fora das situa&ccedil;&otilde;es ordin&aacute;rias<a href="#_ftn1" name="_ftnref1">[1]</a>. Todos os hor&aacute;rios previstos neste contrato levar&atilde;o em considera&ccedil;&atilde;o o hor&aacute;rio de Bras&iacute;lia.</li>
      	<li>utilizar equipamentos, tecnologia e pessoas capacitadas e especialistas para solucionar e atender os requisitos deste instrumento com total efici&ecirc;ncia, agilidade, precis&atilde;o, transpar&ecirc;ncia e efetividade;</li>
      	<li>fornecer todas as informa&ccedil;&otilde;es necess&aacute;rias para a resolu&ccedil;&atilde;o de poss&iacute;veis problemas, para o bom funcionamento do Sistema de Solu&ccedil;&otilde;es, ressalvado o disposto nas demais cl&aacute;usulas deste instrumento;</li>
      	<li>restabelecer o perfeito funcionamento do Sistema de Solu&ccedil;&otilde;es nos casos em que o <strong>LICENCIADO</strong> teve seu funcionamento interrompido segundo a cl&aacute;usula 3.3, voltando o programa a funcionar normalmente quando da regulariza&ccedil;&atilde;o contratual mediante a <strong>MEDSYSTEM S/V</strong>.</li>
      </ol>

      <p>5.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O suporte t&eacute;cnico disponibilizado pela <strong>MEDSYSTEM S/V</strong> pressup&otilde;e o m&iacute;nimo de conhecimento do uso do computador por parte do <strong>LICENCIADO</strong> e de seus colaboradores, o que inclui o uso do computador e suas fun&ccedil;&otilde;es, o uso do sistema operacional sob o qual o sistema ir&aacute; trabalhar e do tema a que o software contratado se prop&otilde;e a atuar. Pressup&otilde;e ainda uma configura&ccedil;&atilde;o adequada dos computadores no que se refere &agrave; utiliza&ccedil;&atilde;o do software e do seu bom estado de funcionamento assim como dos demais dispositivos do ambiente computacional do <strong>LICENCIADO</strong>.</p>

      <p>5.3. &nbsp;&nbsp;&nbsp;&nbsp; Excluem-se integralmente da responsabilidade da <strong>MEDSYSTEM S/V</strong>:</p>

      <ol>
      	<li>resultados produzidos pelo software caso estes sejam afetados por algum tipo de programa externo ou aqueles normalmente conhecidos como &ldquo;v&iacute;rus&rdquo;, &ldquo;Cavalos de Tr&oacute;ia&rdquo;, &ldquo;worms&rdquo;, &ldquo;spywares&rdquo;, &ldquo;rootkit&rdquo;, dentre outros classificados como malwares ou pragas digitais;</li>
      	<li>falha de opera&ccedil;&atilde;o, opera&ccedil;&atilde;o por pessoas n&atilde;o autorizadas ou qualquer outra causa em que n&atilde;o exista culpa da <strong>MEDSYSTEM S/V;</strong></li>
      	<li>descumprimento dos prazos legais do <strong>LICENCIADO</strong> para a entrega de documentos fiscais ou pagamentos de impostos;</li>
      	<li>danos ou preju&iacute;zos decorrentes de decis&otilde;es administrativas, gerenciais ou comerciais tomadas com base nas informa&ccedil;&otilde;es fornecidas pelo sistema;</li>
      	<li>quest&otilde;es definidas como &ldquo;caso fortuito&rdquo; ou &ldquo;for&ccedil;a maior&rdquo;, contempladas no C&oacute;digo Civil Brasileiro;</li>
      	<li>manuten&ccedil;&atilde;o e configura&ccedil;&atilde;o de dispositivos hardwares/equipamentos perif&eacute;ricos (impressoras, computadores, monitores, scanners, pen drive, cart&atilde;o de mem&oacute;ria, disco r&iacute;gido, drives, roteadores, servidores, certificados digitais, dentre outros);</li>
      	<li>manuten&ccedil;&atilde;o e configura&ccedil;&atilde;o dos servi&ccedil;os de rede interna e servi&ccedil;os de Internet do LICENCIADO;</li>
      	<li>instala&ccedil;&atilde;o, configura&ccedil;&atilde;o e utiliza&ccedil;&atilde;o de antiv&iacute;rus ou demais aplicativos de seguran&ccedil;a nos computadores do <strong>LICENCIADO</strong>;</li>
      	<li>instala&ccedil;&atilde;o e configura&ccedil;&atilde;o de softwares de outras empresas;</li>
      	<li>falhas referentes &agrave; m&aacute; utiliza&ccedil;&atilde;o, utiliza&ccedil;&atilde;o ineficiente ou deficit&aacute;ria do software por parte do <strong>LICENCIADO </strong>ou de seus colaboradores;</li>
      	<li>perda de dados armazenados no sistema por falta de backup por parte do <strong>LICENCIADO</strong>;</li>
      	<li>a presta&ccedil;&atilde;o de servi&ccedil;o se verificada a inadimpl&ecirc;ncia e/ou qualquer descumprimento de obriga&ccedil;&atilde;o contratual pelo <strong>LICENCIADO</strong>.</li>
      	<li>Eventos previs&iacute;veis ou n&atilde;o que causem impossibilidade de utiliza&ccedil;&atilde;o do <em>software</em> pelo <strong>LICENCIADO</strong> como, por exemplo, dentre outros casos: falta de conex&atilde;o com a Internet ou qualidade desta conex&atilde;o, falha na rede de transmiss&atilde;o de energia el&eacute;trica ou qualquer outra causa ou evento que n&atilde;o seja associado diretamente &agrave; <strong><em>MEDSYSTEM S/V</em></strong>.</li>
      </ol>

      <p><strong>Cl&aacute;usula SEXTA &ndash; DAS OBRIGA&Ccedil;&Otilde;ES do LICENCIADO</strong></p>

      <p>&nbsp;</p>

      <p>6.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O <strong>LICENCIADO </strong>obriga-se &agrave;:</p>

      <ol>
      	<li>remunerar a <strong>MEDSYSTEM S/V</strong> conforme estipulado na Cl&aacute;usula Terceira deste instrumento ainda que diante de customiza&ccedil;&otilde;es e/ou adequa&ccedil;&otilde;es solicitadas pelo pr&oacute;prio <strong>LICENCIADO</strong> valendo o mesmo quanto &agrave; personaliza&ccedil;&atilde;o ou configura&ccedil;&atilde;o de modelos e/ou impressos, laudos, rotinas, prescri&ccedil;&otilde;es, dentre outros.</li>
      	<li>Permanecer com o software licenciado sempre na sua vers&atilde;o mais recente, executando todas as atualiza&ccedil;&otilde;es que ele pode sofrer disponibilizadas pela <strong>MEDSYSTEM S/V;</strong></li>
      	<li>utilizar o (s) sistema (s) contratado (s) de acordo com suas finalidades e exig&ecirc;ncias t&eacute;cnicas;</li>
      	<li>disponibilizar o meio adequado para a implanta&ccedil;&atilde;o e utiliza&ccedil;&atilde;o do (s) software (s), tais como: hardware, rede, sistema operacional, dentre outros, de acordo com as configura&ccedil;&otilde;es estabelecidas na proposta comercial;</li>
      	<li>exigir e manter em seu quadro de funcion&aacute;rios usu&aacute;rios capacitados no curso e-learning que &eacute; disponibilizado pela <strong>MEDSYSTEM S/V</strong>, com certifica&ccedil;&atilde;o para manuseio e utiliza&ccedil;&atilde;o do sistema(s) contratado(s), bem como de suas evolu&ccedil;&otilde;es tecnol&oacute;gicas, obrigando-se a supervisionar o desempenho de cada um dos usu&aacute;rios no referido curso e no cotidiano das tarefas que envolvam a utiliza&ccedil;&atilde;o do sistema. Assim o <strong>LICENCIADO</strong> se obriga a auditar o desempenho de cada funcion&aacute;rio seu que fa&ccedil;a uso do sistema obrigando-o a realizar o curso e-learning e a obten&ccedil;&atilde;o do certificado que requer que o usu&aacute;rio tenha <strong>nota igual ou superior a 70 (setenta) pontos</strong>. O <strong>LICENCIADO</strong> ser&aacute; instru&iacute;do pela <strong>MEDSYSTEM S/V</strong> sobre o procedimento de auditoria que e &eacute; realizada mediante acesso ao hist&oacute;rico individual de desempenho de cada usu&aacute;rio cadastrado no sistema;</li>
      	<li>responsabilizar-se legalmente pelos dados e informa&ccedil;&otilde;es armazenados no (s) sistema (s) contratado (s);</li>
      	<li>instalar a atualiza&ccedil;&atilde;o dentro do prazo de 30 (trinta) dias corridos agendando a implanta&ccedil;&atilde;o com a <strong>MEDSYSTEM S/V</strong> sob pena de infra&ccedil;&atilde;o contratual:
      	<ol>
      		<li>O licenciado que n&atilde;o renovar o Contrato n&atilde;o ter&aacute; direito a assist&ecirc;ncia de qualquer forma por parte da <strong>MEDSYSTEM S/V</strong>. Perder&aacute; todos os direitos contidos nesse contrato, n&atilde;o poder&aacute; fazer uso de qualquer vers&atilde;o do sistema e exime a <strong>MEDSYSTEM S/V</strong> de qualquer responsabilidade quanto ao sistema, assist&ecirc;ncia e dados armazenados pelo sistema.</li>
      	</ol>
      	</li>
      	<li>reembolsar a <strong>MEDSYSTEM S/V</strong> pelos custos de envio de materiais, servi&ccedil;os e assist&ecirc;ncia.</li>
      	<li>arcar com os preju&iacute;zos advindos de danos permanente e irrepar&aacute;vel de banco de dados quando estes advierem por sua pr&oacute;pria responsabilidade, como por exemplo n&atilde;o efetua&ccedil;&atilde;o de backups, danos f&iacute;sicos em unidades de armazenamento, v&iacute;rus, dentre outros;</li>
      	<li>expor todas as informa&ccedil;&otilde;es indispens&aacute;veis e atinentes &agrave; assist&ecirc;ncia da <strong>MEDSYSTEM S/V </strong>para que esta possa vir a solucionar eventuais corre&ccedil;&otilde;es no software contratado caso seja necess&aacute;rio e em caso de rescis&atilde;o formalizar o pedido por escrito valendo o uso de meios eletr&ocirc;nicos;</li>
      	<li>responsabilizar-se por qualquer infra&ccedil;&atilde;o legal, nos &acirc;mbitos civil, penal, autoral e todos os demais, que, eventualmente, venha a ser cometida com a utiliza&ccedil;&atilde;o do (s) software (s) contratado (s).</li>
      	<li>possuir e manter ativo durante toda a vig&ecirc;ncia do contrato o acesso &agrave; Internet devendo esta ter velocidade m&iacute;nima de 1Mb para a realiza&ccedil;&atilde;o de atualiza&ccedil;&otilde;es e assist&ecirc;ncia t&eacute;cnica;</li>
      	<li>manter as configura&ccedil;&otilde;es dos computadores integrantes de seu ambiente computacional, assim como pela manuten&ccedil;&atilde;o e custeio dos equipamentos j&aacute; existentes e os que vierem a ser integrados assim como manter o ambiente computacional protegido contra v&iacute;rus e outras pragas digitais.</li>
      	<li>manter c&oacute;pias e todos os recursos necess&aacute;rios &agrave; recupera&ccedil;&atilde;o de falhas, quebras ou demais eventos que possam tornar os dados inacess&iacute;veis. O uso do software objeto deste contrato assim como qualquer outro software tende a gerar volumes crescentes de informa&ccedil;&otilde;es importantes ao LICENCIADO. A guarda e o tratamento destas informa&ccedil;&otilde;es s&atilde;o de sua inteira responsabilidade competindo-lhe a realiza&ccedil;&atilde;o de c&oacute;pias de seguran&ccedil;a di&aacute;rias e locais de todo o seu banco de dados, al&eacute;m do armazenamento em outros dispositivos de armazenamento de dados e mesmo em outros locais.</li>
      	<li>acessar constantemente o Espa&ccedil;o Vip www.vip.soitic.com para os fins da Cl&aacute;usula 2.3 al&eacute;m de informar para a MEDSYSTEM S/V os seus endere&ccedil;os de correios eletr&ocirc;nicos para recebimento de e-mails e interatividades importantes para o bom andamento deste Contrato obrigando-se ainda a: <strong>a)</strong> abrir constantemente sua caixa de entrada de e-mails para verifica&ccedil;&atilde;o de mensagens referentes ao Contrato; <strong>b)</strong> manter tais endere&ccedil;os eletr&ocirc;nicos sempre atualizados perante a <strong>MEDSYSTEM S/V</strong>.</li>
      </ol>

      <p>&nbsp;</p>

      <h2>CL&Aacute;USULA S&Eacute;TIMA &ndash; DAS PENALIDADES CL&Aacute;USULA PENAL</h2>

      <p>&nbsp;</p>

      <p>7.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O descumprimento de quaisquer das obriga&ccedil;&otilde;es estabelecidas neste contrato implicar&aacute; na indeniza&ccedil;&atilde;o a t&iacute;tulo de cl&aacute;usula penal compensat&oacute;ria a ser suportada pela parte ofensora em favor da parte prejudicada <strong>equivalente a 03 (tr&ecirc;s) vezes o valor vigente da remunera&ccedil;&atilde;o mensal estabelecido na cl&aacute;usula 3.1 </strong>sem preju&iacute;zo da aplica&ccedil;&atilde;o das demais medidas contratuais e judiciais cab&iacute;veis<strong>.</strong></p>

      <p><strong>Cl&aacute;usula OITAVA &ndash; DA RESCIS&Atilde;O</strong></p>

      <p>&nbsp;</p>

      <p>8.1. &nbsp;&nbsp;&nbsp;&nbsp; O presente instrumento somente poder&aacute; ser rescindido ap&oacute;s o prazo de <strong>12 (Doze) meses</strong> sendo que antes de decorrido tal per&iacute;odo, a parte que rescindir o contrato ficar&aacute; obrigada ao pagamento a t&iacute;tulo indenizat&oacute;rio, do equivalente a 20% (vinte por cento) do que seria devido pelo restante do contrato de acordo com a mensalidade estabelecida na cl&aacute;usula 3.1, ii, <strong>arcando ainda com a cl&aacute;usula penal estabelecida na cl&aacute;usula 7.1 a favor da parte contr&aacute;ria</strong>. Em nenhuma hip&oacute;tese ser&atilde;o devolvidos quaisquer valores pagos que ser&atilde;o considerados como contrapresta&ccedil;&atilde;o pela utiliza&ccedil;&atilde;o do software durante sua vig&ecirc;ncia.</p>

      <p>8.2. &nbsp;&nbsp;&nbsp;&nbsp; Ap&oacute;s o prazo acima citado poder&aacute; este instrumento ser rescindido por qualquer das partes, a qualquer tempo, com especifica&ccedil;&atilde;o do motivo, sem qualquer &ocirc;nus, desde que n&atilde;o esteja inadimplente com suas obriga&ccedil;&otilde;es e notifique a outra parte previamente e por escrito, no prazo antecedente de 30 (trinta) dias, sendo que quando a notifica&ccedil;&atilde;o de rescis&atilde;o for feita pela <strong>MEDSYSTEM S/V</strong> esta continuar&aacute; a prestar os servi&ccedil;os por mais 30 dias e quando a notifica&ccedil;&atilde;o de rescis&atilde;o for feita pelo <strong>LICENCIADO</strong>, este dever&aacute; arcar com o pagamento de mais 01 (uma) mensalidade estabelecida na cl&aacute;usula 3.1, ii, pois continuar&aacute; utilizando os servi&ccedil;os por mais 30 dias.</p>

      <p>8.3. &nbsp;&nbsp;&nbsp;&nbsp; Rescindido o contrato, o <strong>LICENCIADO </strong>dever&aacute; converter todo o banco de dados advindo da utiliza&ccedil;&atilde;o do software, no prazo m&aacute;ximo de 30 (trinta) dias, sendo que ap&oacute;s este per&iacute;odo a MEDSYSTEM S/V estar&aacute; autorizada a interromper o funcionamento do software contratado. Assim, fica o usu&aacute;rio restrito apenas a revis&atilde;o do banco de dados existente, estando apto a acessar e imprimir dados, sem, contudo, realizar qualquer altera&ccedil;&atilde;o ou inclus&atilde;o de novos dados no sistema que ter&aacute; seu funcionamento interrompido;</p>

      <p>8.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ambas as partes poder&atilde;o rescindir o contrato, a qualquer tempo, comprovada a ocorr&ecirc;ncia de decreta&ccedil;&atilde;o de recupera&ccedil;&atilde;o judicial, fal&ecirc;ncia ou dissolu&ccedil;&atilde;o de algumas das partes. Nestas hip&oacute;teses, a rela&ccedil;&atilde;o jur&iacute;dica entre as partes restar&aacute; rompida por completo as desobrigando de arcar com quaisquer &ocirc;nus ou indeniza&ccedil;&otilde;es advindas deste instrumento.</p>

      <p>8.5. &nbsp;&nbsp;&nbsp;&nbsp; <strong>A MEDSYSTEM S/V</strong> reserva-se o direito de rescindir o presente Contrato, a seu crit&eacute;rio exclusivo, cancelando imediatamente a conta do<strong> CONTRATANTE</strong>:</p>

      <ol>
      	<li>Na eventualidade de se verificar que o <strong>CONTRATANTE</strong> pratique qualquer atitude que viole ou que seja de alguma forma contr&aacute;ria com as cl&aacute;usulas deste contrato, a qualquer tipo de legisla&ccedil;&atilde;o vigente, &agrave; &eacute;tica, norma ou preceito ligado &agrave; Inform&aacute;tica em Sa&uacute;de, ao Conselho Federal de Medicina ou outro &oacute;rg&atilde;o ou entidade ligado &agrave; Sa&uacute;de e Medicina;</li>
      	<li>Se a <strong>MEDSYSTEM S/V</strong> entender e/ou verificar que o <strong>CONTRATANTE</strong> esteja utilizando o software indevidamente ou de alguma forma pratique ato de degrada&ccedil;&atilde;o da imagem da <strong>MEDSYSTEM S/V</strong>.</li>
      </ol>

      <p>&nbsp;</p>

      <h2>CL&Aacute;USULA NONA &ndash; DAS DISPOSI&Ccedil;&Otilde;ES GERAIS</h2>

      <p>&nbsp;</p>

      <p>9.1. &nbsp;&nbsp;&nbsp;&nbsp; Os tributos e encargos fiscais que sejam devidos, diretos ou indiretamente, em virtude deste contrato ou de sua execu&ccedil;&atilde;o, ser&atilde;o de exclusiva responsabilidade de seu contribuinte, conforme definido na norma tribut&aacute;ria.</p>

      <p>9.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Qualquer omiss&atilde;o ou toler&acirc;ncia em exigir o cumprimento de quaisquer termos ou condi&ccedil;&otilde;es deste contrato, ou em exercer direitos dele decorrentes, n&atilde;o constituir&aacute; ren&uacute;ncia, precedente ou nova&ccedil;&atilde;o contratual a eles e n&atilde;o prejudicar&aacute; assim a faculdade de qualquer das partes exigi-los ou exerc&ecirc;-los a qualquer tempo.</p>

      <p>9.3. &nbsp;&nbsp;&nbsp;&nbsp; Caso o <strong>LICENCIADO</strong> j&aacute; possua outro sistema, a convers&atilde;o de banco de dados tamb&eacute;m ser&aacute; objeto de cobran&ccedil;a em separado. Por&eacute;m, somente ap&oacute;s an&aacute;lise do departamento respons&aacute;vel a<strong> MEDSYSTEM S/V</strong> informar&aacute; o <strong>LICENCIADO</strong> sobre a compatibilidade do banco de dados e fornecer&aacute; o pre&ccedil;o para a convers&atilde;o do banco de dados. A <strong>MEDSYSTEM S/V</strong> n&atilde;o presta servi&ccedil;os de digita&ccedil;&atilde;o de dados referentes &agrave; eventual impossibilidade de convers&atilde;o do banco de dados do sistema anterior do <strong>LICENCIADO </strong>sendo de seu encargo tal responsabilidade.</p>

      <p>9.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; O <strong>LICENCIADO</strong> declara expressamente que foi previamente orientado e esclarecido pela <strong>MEDSYSTEM S/V</strong> sobre todas as d&uacute;vidas ou questionamentos relacionados ao software que ser&aacute; implantado por cess&atilde;o de uso em Proposta T&eacute;cnica e Comercial que antecedeu a forma&ccedil;&atilde;o deste contrato constando as fun&ccedil;&otilde;es execut&aacute;veis, configura&ccedil;&otilde;es m&iacute;nimas exigidas bem como os valores de cada servi&ccedil;o. Assim, a proposta t&eacute;cnica e comercial &eacute; parte integrante deste contrato.</p>

      <p>9.5. &nbsp;&nbsp;&nbsp;&nbsp; A <strong>MEDSYSTEM S/V</strong> garante ao <strong>LICENCIADO</strong>, a libera&ccedil;&atilde;o total e definitiva de qualquer forma de bloqueio do sistema em caso de encerramento de suas atividades ou fal&ecirc;ncia, incluindo os c&oacute;digos fonte do sistema contratado.</p>

      <p>9.6. &nbsp;&nbsp;&nbsp;&nbsp; A <strong>MEDSYSTEM S/V</strong> poder&aacute; notificar a qualquer momento que o <strong>LICENCIADO</strong> efetue melhorias em sua infraestrutura de hardware, rede, sistema operacional, Internet e pessoal para um melhor e adequado desempenho do sistema contratado.</p>

      <p>9.7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Este contrato constitui o acordo integral entre as partes no que tange ao seu objeto, prevalecendo sobre quaisquer outros entendimentos ou acordos anteriores, t&aacute;citos ou expressos.</p>

      <p>9.8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; As partes por si, seus empregados e prepostos, obrigam-se a manter sigilo sobre quaisquer informa&ccedil;&otilde;es, dados, materiais, documentos, especifica&ccedil;&otilde;es t&eacute;cnicas ou comerciais, inova&ccedil;&otilde;es ou aperfei&ccedil;oamento, ou dados gerais, de que venha a ter acesso ou conhecimento em raz&atilde;o deste contrato, ou ainda que lhes tenham sido confiados em virtude deste, n&atilde;o podendo, sob qualquer pretexto ou desculpa, omiss&atilde;o, culpa ou dolo, revelar, reproduzir ou deles dar conhecimento a estranhos dessa contrata&ccedil;&atilde;o, salvo se houver consentimento expresso, em conjunto das mesmas, ou cuja divulga&ccedil;&atilde;o seja exigida por for&ccedil;a de solicita&ccedil;&atilde;o dos poderes p&uacute;blicos ou determina&ccedil;&atilde;o judicial.</p>

      <p>9.9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A responsabilidade das partes com rela&ccedil;&atilde;o &agrave; quebra de sigilo, ser&aacute; proporcional aos efeitos do preju&iacute;zo causado.</p>

      <p>9.10.&nbsp;&nbsp;&nbsp; A nulidade de qualquer uma das cl&aacute;usulas deste contrato n&atilde;o implicar&aacute; em nulidade das demais. Na hip&oacute;tese de qualquer das cl&aacute;usulas deste contrato vir a serem declaradas nulas, em face de decis&atilde;o judicial transitada em julgado, elas n&atilde;o afetar&atilde;o os demais termos e condi&ccedil;&otilde;es, os quais continuar&atilde;o vigorando entre as partes, produzindo seus efeitos, inclusive em rela&ccedil;&atilde;o a terceiros.</p>

      <p>9.11. &nbsp;&nbsp; O CONTRATANTE concorda que a MEDSYSTEM S/V lhe envie ou simplesmente disponibilize por meio do software ou outro meio, artigos, textos, not&iacute;cias, ou quaisquer outras formas de comunica&ccedil;&atilde;o, desenvolvidas pela pr&oacute;pria MEDSYSTEM S/V ou por parceiros desta, sempre que a MEDSYSTEM S/V julgar que tais comunica&ccedil;&otilde;es sejam de interesse do CONTRATANTE.O <strong>CONTRATANTE</strong> reconhece e admite que quaisquer modifica&ccedil;&otilde;es, sejam elas qualificadas como otimiza&ccedil;&otilde;es ou como corre&ccedil;&otilde;es realizadas no software, mesmo que informadas, solicitadas e eventualmente pagas por ele ficam incorporadas ao software e ser&atilde;o de propriedade da <strong>MEDSYSTEM S/V</strong>, sendo, portanto, objeto de licen&ccedil;a de uso, nos id&ecirc;nticos termos e sujeitando-se &agrave;s condi&ccedil;&otilde;es deste contrato, podendo inclusive ser disponibilizadas a outros usu&aacute;rios</p>

      <p>9.12.&nbsp;&nbsp;&nbsp; Caso o <strong>CONTRATANTE</strong> fa&ccedil;a uso de backups f&iacute;sicos e locais do banco de dados, a responsabilidade sobre eles &eacute; integral e exclusivamente sua, respondendo por si s&oacute; em caso de perdas ou danifica&ccedil;&atilde;o na unidade de armazenamento utilizada. A <strong>MEDSYSTEM S/V</strong> n&atilde;o realiza backup f&iacute;sico referente ao software deste contrato, mas, poder&aacute; disponibilizar o backup do banco de dados para que o <strong>CONTRATANTE</strong> possa fazer a convers&atilde;o.</p>

      <ol>
      	<li>A responsabilidade pela convers&atilde;o do banco de dados ser&aacute; exclusiva do <strong>CONTRATANTE</strong> sendo que a <strong>MEDSYSTEM S/V</strong> estar&aacute; isenta de qualquer responsabilidade neste procedimento nem estar&aacute; obrigada a fornecer qualquer tipo de assist&ecirc;ncia.</li>
      </ol>

      <p>9.13.&nbsp;&nbsp;&nbsp; Upgrade anual estabelecido n&atilde;o &eacute; objeto de cobran&ccedil;a por parte da <strong>MEDSYSTEM S/V</strong> sendo que ser&aacute; enviado boleto para o <strong>LICENCIADO</strong> referente apenas &agrave;s despesas administrativas, com materiais e seu envio.</p>

      <p>9.14.&nbsp;&nbsp;&nbsp; O ambiente computacional &eacute; de total responsabilidade do <strong>LICENCIADO</strong>. Quaisquer a&ccedil;&otilde;es e circunst&acirc;ncias ou ainda eventos ocorridos neste ambiente e que interfiram com o desempenho do Sistema de Solu&ccedil;&otilde;es (software) contratado tamb&eacute;m s&atilde;o de responsabilidade do <strong>LICENCIADO</strong>. A empresa <strong>MEDSYSTEM S/V</strong> poder&aacute; adequar ou corrigir essas eventuais interfer&ecirc;ncias com o sistema, local ou remotamente, sob remunera&ccedil;&atilde;o acordada previamente entre o <strong>LICENCIADO</strong> e <strong>MEDSYSTEM S/V</strong>. Citam-se como exemplos reimplanta&ccedil;&atilde;o do Sistema de Solu&ccedil;&otilde;es, troca de servidor e/ou sua formata&ccedil;&atilde;o, dentre outros.</p>

      <p>&nbsp;</p>

      <h2>CL&Aacute;USULA D&Eacute;CIMA &ndash; DO FORO</h2>

      <p>&nbsp;</p>

      <p>10.1. As Partes elegem o foro da Comarca de Po&ccedil;os de Caldas-MG como &uacute;nico competente para dirimir quaisquer d&uacute;vidas ou lit&iacute;gios derivados do presente contrato, com ren&uacute;ncia expressa a qualquer outro por mais privilegiado que seja ou venha a ser.</p>

      <p>&nbsp;</p>

      <p>Po&ccedil;os de Caldas, xx de xxxx de 2017.</p>

      <p>&nbsp;</p>

      <p><strong>___________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ________________________</strong></p>

      <p><strong>&nbsp;&nbsp;&nbsp;&nbsp; VIANA &amp; CIA LTDA EPP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</strong></p>

      <p>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&ldquo;MEDSYSTEM S/V&rdquo;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &ldquo;LICENCIADO&rdquo;</p>

      <p>&nbsp;</p>

      <hr />
      <p>&nbsp;</p>
      </div>



</div>

  </div>


  @endsection
  <!-- /.container-fluid-->
