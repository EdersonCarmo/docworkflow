@extends('layouts.soged')
{{-- @extends('includes.modaldoc') --}}

@section('content')

@php
$idpasta=0;
$_SESSION['pastaend'] = \Auth::user()->empresa_id.'p'.\Auth::id().'/';

function nomepasta($nome){
	switch ($nome) {
		case \Auth::user()->empresa_id.'a'.\Auth::id():
		return "Documentos aprovados";
		break;
		case \Auth::user()->empresa_id.'c'.\Auth::id():
		return "Compartilhados comigo";
		break;
		case \Auth::user()->empresa_id.'p'.\Auth::id():
		return "Meus documentos";
		break;
		case \Auth::user()->empresa_id.'t'.\Auth::id():
		return "Templates";
		break;
		default:
		return $nome;
	}
}

@endphp

<div class="container-fluid">

	<ol class="breadcrumb">
		@if(! empty($breadcrumb_lista))
		@foreach (array_reverse($breadcrumb_lista) as $item)
		<li class="breadcrumb-item"><a data="{{$item->id_pasta}}" href="{{url('pasta/'.$item->id_pasta)}}" class="droppable">{{ nomepasta($item->pasta) }}</a></li>
		@endforeach
		@endif
	</ol>

	<div class="card mb-3">

		<div class="card-header">
			<div>
				<i class="fa fa-folder"></i>
				<strong>{{ nomepasta($_SESSION['nomeatual']) }}</strong>
			</div>
		</div>

		<div class="card-body workspace-folder">
			<div class="container-fluid">
				<div class="row escopo-pastas">








					
					<!-- ↓ foreach PASTAS ↓ foreach PASTAS ↓ foreach PASTAS ↓ foreach PASTAS ↓ foreach PASTAS ↓ foreach PASTAS ↓ foreach PASTAS ↓ foreach PASTAS  -->
					@if($tipo != 't')
					<div>
						<h6 class="mb-0">Diretórios</h6>
						<hr class="mt-1">
					</div>

					<div class="row mb-3">
						@foreach ($pastas as $key)
						@if ($idpasta != $key->id_pasta)
						<div class="col-lg-2 text-center">
							<div class="arquivos" id="div_pasta_{{$key->id_pasta}}">

								<a href="{{ route('pasta.show', $key->id_pasta) }}" tipo="pasta" data="{{$key->id_pasta}}" permi="{{$key->id_permissao}}" class="propriedades droppable draggable" link="">
									<i class="fa fa-folder fa-5x file"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>

									<p class="text-center nome_arquivo" indice="{{$key->id_pasta}}" id="text_pasta_{{$key->id_pasta}}">{{ $key->pasta }}</p>
								</a>
								<input type="text" value="{{ $key->pasta }}" indice="{{$key->id_pasta}}" id="input_pasta_{{$key->id_pasta}}" class="form-control renomear_input text-center" name="nome_pasta"/>

							</div>
						</div>
						@php
						$idpasta = $key->id_pasta;
						@endphp
						@endif
						@endforeach
						
					</div>
					@endif

					<!-- ↑ foreach PASTAS ↑ foreach PASTAS ↑ foreach PASTAS ↑ foreach PASTAS ↑ foreach PASTAS ↑ foreach PASTAS ↑ foreach PASTAS ↑ foreach PASTAS -->

					
















					<!-- ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS ↓ foreach ARQUIVOS  -->
					@if($tipo != 't')
					<div>
						<h6 class="mb-0">Arquivos</h6>
						<hr class="mt-1">
					</div>

					<div class="row mb-3">

						@foreach ($arquivos as $key)
						@php
						$ext = explode(".", $key->local);

						$ext = end($ext);

						$ico = 'fa-file'; $rename=0;
						if($ext == 'zip' or $ext == 'rar') $ico = 'fa-file-archive';
						if($key->crud==4 or $key->crud==5 or $key->crud==6 or $key->crud==7 or $key->crud==12 or $key->crud==13 or $key->crud==14 or $key->crud==15) $rename=1;
						@endphp
						<div class="col-lg-2 text-center">
							<div class="arquivos" id="div_arquivo_{{$key->id_arquivo}}">

								@if ($ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'gif' or $ext == 'bmp')
								<a href="{{ env('APP_S3').$key->local }}" data-lightbox="{{ $key->arquivo }}" data-title="{{ $key->arquivo }}" class="propriedades draggable" tipo="arquivo" id="arquivo_{{$key->id_arquivo}}" permi="{{$key->id_permissao}}" data="{{$key->id_arquivo}}" data-whatever="{{env('APP_S3').$key->local}}" link="{{$key->local}}" crud="{{$key->crud}}">
									<img src="{{ env('APP_S3').$key->local }}" alt="" width="60" height="60">
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>
									<p class="text-center nome_arquivo" indice="{{$key->id_arquivo}}" id="text_arquivo_{{$key->id_arquivo}}">{{ $key->arquivo }}</p>
								</a>

								@elseif($ext == 'pdf')
								<a href="{{ env('APP_S3').$key->local }}" target="_blank" class="propriedades draggable" tipo="arquivo" id="arquivo_{{$key->id_arquivo}}" permi="{{$key->id_permissao}}" data="{{$key->id_arquivo}}" data-whatever="{{env('APP_S3').$key->local}}" link="{{$key->local}}" crud="{{$key->crud}}">
									<i class="fa fa-file-pdf fa-4x"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>
									<p class="text-center nome_arquivo" indice="{{$key->id_arquivo}}" id="text_arquivo_{{$key->id_arquivo}}">{{ $key->arquivo }}</p>
								</a>

								@elseif($ext == 'xls' or $ext == 'xlsx')
								<a href="{{ env('APP_DOCS').$key->local }}&embedded=true" target="_blank" class="propriedades draggable" tipo="arquivo" id="arquivo_{{$key->id_arquivo}}" permi="{{$key->id_permissao}}" data="{{$key->id_arquivo}}" data-whatever="{{env('APP_S3').$key->local}}" link="{{$key->local}}" crud="{{$key->crud}}">
									<i class="fa fa-file-excel fa-4x"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>
									<p class="text-center nome_arquivo" indice="{{$key->id_arquivo}}" id="text_arquivo_{{$key->id_arquivo}}">{{ $key->arquivo }}</p>
								</a>

								@elseif($ext == 'ppt' or $ext == 'pptx')
								<a href="{{ env('APP_DOCS').$key->local }}&embedded=true" target="_blank" class="propriedades draggable" tipo="arquivo" id="arquivo_{{$key->id_arquivo}}" permi="{{$key->id_permissao}}" data="{{$key->id_arquivo}}" data-whatever="{{env('APP_S3').$key->local}}" link="{{$key->local}}" crud="{{$key->crud}}">
									<i class="fa fa-file-powerpoint fa-4x"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>
									<p class="text-center nome_arquivo" indice="{{$key->id_arquivo}}" id="text_arquivo_{{$key->id_arquivo}}">{{ $key->arquivo }}</p>
								</a>

								@elseif($ext == 'doc' or $ext == 'docx' or $ext == 'odt' or $ext == 'ods' or $ext == 'odf' or $ext == 'pages' or $ext == 'txt')
								<a href="{{ env('APP_DOCS').$key->local }}&embedded=true" target="_blank" class="propriedades draggable" tipo="arquivo" target="_blank" id="arquivo_{{$key->id_arquivo}}" permi="{{$key->id_permissao}}" data="{{$key->id_arquivo}}" data-whatever="{{env('APP_S3').$key->local}}" link="{{$key->local}}" crud="{{$key->crud}}">
									<i class="fa fa-file-word fa-4x"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>
									<p class="text-center nome_arquivo" indice="{{$key->id_arquivo}}" id="text_arquivo_{{$key->id_arquivo}}">{{ $key->arquivo }}</p>
								</a>

								@else
								<a href="{{ env('APP_S3').$key->local }}" class="propriedades draggable" download tipo="arquivo" id="arquivo_{{$key->id_arquivo}}" permi="{{$key->id_permissao}}" data="{{$key->id_arquivo}}" data-whatever="{{env('APP_S3').$key->local}}" link="{{$key->local}}" crud="{{$key->crud}}">
									<i class="fa {{$ico}} fa-4x"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":""}}" title="Arquivo compartilhado"></i>
									<p class="text-center nome_arquivo" indice="{{$key->id_arquivo}}" id="text_arquivo_{{$key->id_arquivo}}">{{ $key->arquivo }}</p>
								</a>
								@endif

								
								<input type="text" value="{{ $key->arquivo }}" indice="{{$key->id_arquivo}}" id="input_arquivo_{{$key->id_arquivo}}" class="form-control renomear_input text-center" name="nome_arquivo"/>

							</div>
						</div>
						@endforeach
						
					</div>
					@endif
					<!-- ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS ↑ foreach ARQUIVOS -->
















					
					<!-- ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS ↓ foreach DOCUMENTOS -->
					@if($tipo != 't')
					<div>
						<h6 class="mb-0">Documentos</h6>
						<hr class="mt-1">
					</div>
					@endif

					<div class="row mb-3">
						@foreach ($documentos as $key)

						<div class="col-lg-2 text-center">
							<div class="arquivos" id="div_documento_{{ $key->id_documento }}">
								<a href="#" data-toggle="modal" data-target="#documentoModal" data-nome="{{$key->documento}}" data-conteudo="{{$key->texto}}" class="propriedades draggable" tipo="documento" id="documento_{{$key->id_documento}}" data="{{$key->id_documento}}" permi="{{$key->id_permissao}}" link="" encrypt="{{encrypt($_SESSION['pastaend'])}}" data-whatever="{{$key->documento}}" crud="{{$key->crud}}">
									<i class="fa fa-file-alt fa-4x"></i>
									<i class="{{ $key->sts == 2?"fa fa-users":"" }}" title="Arquivo compartilhado"></i>

									<p class="text-center nome_documento" indice="{{$key->id_documento}}" id="text_documento_{{$key->id_documento}}">{{ $key->documento }}</p>
								</a>

								<input type="text" value="{{ $key->documento }}" indice="{{$key->id_documento}}" id="input_documento_{{$key->id_documento}}" class="form-control renomear_input text-center" name="nome_documento"/>
							</div>
						</div>
						@endforeach

					</div>
					<!-- ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS ↑ foreach DOCUMENTOS -->





















					
					<!-- ↓ BTN MAIS ↓ BTN MAIS ↓ BTN MAIS ↓ BTN MAIS ↓ BTN MAIS ↓ BTN MAIS ↓ BTN MAIS ↓ BTN MAIS -->
					@isset($folder)
					<button class="btn btn-success btn-novo"><i class="fa fa-plus fa-2x"></i></button>

					<div class="menu-novo">
						<ul>
							@if($tipo != 't')
							<a href="#" data-toggle="modal" data-target="#selectTemModal" data-whatever="{{$atual}}"><li class="my-2"><i class="fa fa-file mr-4"></i>Novo Documento</li></a>
							<a href="#" class="nova_pasta_button" data-whatever="{{$atual}}"><li class="my-2"><i class="fa fa-folder mr-4"></i>Nova Pasta</li></a>
							@endif
							@if($tipo == 't')
							<a href="{{route('createTem',$atual)}}"><li class="my-2"><i class="fa fa-file-alt mr-4"></i>Novo Template</li></a>
							@endif
							<hr>
							@if($tipo != 't')
							<a href="#" data-toggle="modal" data-target="#uploadModal"><li class="my-2"><i class="fa fa-upload mr-4"></i>Enviar Arquivo</li></a>
							@endif
						</ul>
					</div>
					@endisset
					<!-- ↑ BTN MAIS ↑ BTN MAIS ↑ BTN MAIS ↑ BTN MAIS ↑ BTN MAIS ↑ BTN MAIS ↑ BTN MAIS ↑ BTN MAIS -->
					<!-- ↓ Propriedades ↓ Propriedades ↓ Propriedades ↓ Propriedades ↓ Propriedades ↓ Propriedades ↓ Propriedades ↓ Propriedades -->
					<div class="menu-propriedades">
						<ul>
							<a href="#" class="renomear_button">
								<li class="my-2"><i class="fa fa-edit mr-4"></i>Renomear</li>
							</a>
							<a href="{{route('show2', [2, encrypt($_SESSION['pastaend'])])}}" class="editar_button">
								<li class="my-2"><i class="fa fa-pencil-alt mr-4"></i>Editar</li>
							</a>
							<a href="#" class="download_button" download target="_blank">
								<li class="my-2"><i class="fa fa-arrow-alt-circle-down mr-4"></i>Download</li>
							</a>
							<a href="#" class="modal_button share_button" data-toggle="modal" target="#shareModal">
								<li class="my-2"><i class="fa fa-share mr-4"></i>Compartilhar</li>
							</a>
							<a href="#" class="modal_button permissao_button" data-toggle="modal" target="#permissoesModal">
								<li class="my-2"><i class="fa fa-key mr-4"></i>Visualizar Permissões</li>
							</a>
							<hr>
							<a href="#" class="excluir_button">
								<li class="my-2"><i class="fa fa-trash mr-4"></i>Excluir</li>
							</a>
						</ul>
					</div>
					<!-- ↑ Propriedades ↑ Propriedades ↑ Propriedades ↑ Propriedades ↑ Propriedades ↑ Propriedades ↑ Propriedades ↑ Propriedades -->
				</div>
			</div>
		</div>
		<div class="card-footer small text-muted"> Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
	</div>

</div>

<!-- Modal SELECIONA TEMPLATE  Modal SELECIONA TEMPLATE  Modal SELECIONA TEMPLATE  Modal SELECIONA TEMPLATE  Modal SELECIONA TEMPLATE -->
<div class="modal fade" id="selectTemModal" tabindex="-1" role="dialog" aria-labelledby="selectTemModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="selectTemModalLabel"><i class="fa fa-file mr-2"></i> Novo Documento</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			@isset($templates)
			<div class="modal-body">
				<form class="" action="{{route('createDocs')}}" method="post">
					{{ csrf_field() }}
					<input type="hidden" name="pasta" value="{{$atual}}">

					<div class="col-12 mb-3">
						<label class="">Clientes</label>
						<select multiple class="form-control" id="exampleSelect3" name="cliente_id[]" data-width="100%" >
							@foreach ($clientes as $item)
							<option value="{{$item->id_cliente}}">{{$item->cliente}}</option>
							@endforeach
						</select>
					</div>

					<div class="col-12 mb-3">
						<label class="" for="">Modelo de Template</label>
						<select class="form-control" name="id_documento" id="template_escolhido" onchange="escolherTemplate()">
							<option value="0">Selecione ...</option>
							@foreach ($templates as $key)
							<option value="{{$key->id_documento}}">{{$key->documento}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-success btn-sm mr-2" type="submit" id="btn_template" disabled>Gerar Documentos</button>
					<a class="btn btn-primary btn-sm mr-2" href="{{route('createDoc',$atual)}}">Novo em branco</a>
				</div>

			</form>
			@endisset
		</div>
	</div>
</div>
<!-- Modal SELECIONA TEMPLATE -->

<!-- Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO -->
<div class="modal fade" id="documentoModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="documentoModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="container mt-3">
				<form action="{{route("ImprimirMoldura")}}" method="POST" class="d-flex">
					{{ csrf_field() }}
					<select class="form-control" name="moldura" id="moldura" required>
						<option value="">Selecione ...</option>
						@foreach ($molduras as $key)
						<option value="{{$key->id_moldura}}">{{$key->moldura}}</option>
						@endforeach
					</select>
					<input hidden name="id_documento" id="id_documento_modal" value="">
					<button type="submit" class="ml-2 btn btn-success"><i class="fa fa-print"></i></button>
				</form>
			</div>
			<div class="modal-body">



			</div>
		</div>
	</div>
</div>
<!-- Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO Modal ABRIR DOCUMENTO -->

<!-- Modal MENU UPLOAD -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="uploadModalLabel">
					<i class="fa fa-cloud-upload-alt mr-2" aria-hidden="true"></i> Envio de arquivos
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="" action="{{route("arquivos")}}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="modal-body">
					<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
					@isset($_SESSION['pastaend'])
					<input type="hidden" name="pastaend" value="{{ $_SESSION['pastaend'] }}">
					@endisset
					@isset($_SESSION['idpastaatual'])
					<input type="hidden" name="pastaatual" value="{{ $_SESSION['idpastaatual'] }}">
					@endisset
					<input class="form-control" id="arquivo" type="file" name="arquivo[]" value="" required autofocus multiple>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary loader-button"><i class="fa fa-cloud-upload-alt" aria-hidden="true"></i> Enviar arquivo</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal MENU UPLOAD MENU UPLOAD MENU UPLOAD MENU UPLOAD MENU UPLOAD MENU UPLOAD MENU UPLOAD MENU UPLOAD MENU UPLOAD -->

<!-- Modal MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR -->
<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="shareModalLabel">
					Compartilhar
				</h5>

				<a href="#" id="gerar-link-publico" class="mb-3 text-muted"><small>Gerar link público<i class="ml-2 fa fa-link"></i></small></a>

			</div>
			<form action="{{route("compStore_permissao")}}" method="post">
				{{ csrf_field() }}
				<div class="modal-body">

					<div class="row" style="display:none" id="link-publico">
						<div class="col-12">
							<h6 class="mb-0">Compartilhamento de link</h6>
							<small id="link-publico-gerado"></small>
						</div>

						<div class="col-6">
							<div class="form-group">


							</div>
						</div>

						<div class="col-12">
							<hr>
						</div>

					</div>

					<div class="row">
						<div class="col-12">
							<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
							@isset($_SESSION['pastaend'])
							<input type="hidden" name="pastaend" value="{{ $_SESSION['pastaend'] }}">
							@endisset
							@isset($_SESSION['idpastaatual'])
							<input type="hidden" name="pastaatual" value="{{ $_SESSION['idpastaatual'] }}">
							@endisset

							<div class="form-group">
								<h6 for="exampleSelect2 mb-0">Pessoas</h6>
								<select multiple class="form-control" id="exampleSelect2" name="user_id[]" data-width="100%" >
									@foreach ($usuarios as $item)
									<option value="{{$item->id}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>

							<div id="compartilhar_parametros"></div>

							<div id="avancado-compartilhar-check" style="display:none">
								<div class="form-group compartilhar_permissoes_arquivo">
									<select class="form-control" id="compartilhar_permissoes" name="crud[]">
									</select>
								</div>

								<div class="compartilhar_permissoes_pasta">
									<div class="row">

										<div class="col-6">
											<div class="form-check">
												<small class="form-check-label">
													<input class="form-check-input" id="leitura_pasta" type="checkbox" value="1" name="crud[]" hidden>
													<input class="form-check-input" type="checkbox" value="1" name="crud[]" checked disabled>
													Ler
												</small>
											</div>
											<div class="form-check">
												<small class="form-check-label">
													<input class="form-check-input" type="checkbox" value="4" name="crud[]">
													Editar
												</small>
											</div>
											<div class="form-check">
												<small class="form-check-label">
													<input class="form-check-input" type="checkbox" value="2" name="crud[]">
													Criar
												</small>
											</div>
										</div>

										<div class="col-6">
											<div class="form-check">
												<small class="form-check-label">
													<input class="form-check-input" type="checkbox" value="16" name="crud[]">
													Download
												</small>
											</div>
											<div class="form-check">
												<small class="form-check-label">
													<input class="form-check-input" type="checkbox" value="32" name="crud[]">
													Upload
												</small>
											</div>
										</div>

									</div>
								</div>
								

								
							</div>
						</div>

					</div>
				</div>

				<div class="modal-footer justify-content-between w-100">

					<div>
						<a href="#" id="avancado-compartilhar" class="my-2 text-muted"><small>Avançado</small></a>
					</div>

					<div>
						<button type="submit" class="btn btn-primary">Compartilhar</button>
						<button type="button" class="btn btn-outline-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
					</div>

				</div>

			</form>

		</div>
	</div>
</div>
</div>
<!-- Modal MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR MENU COMPARTILHAR -->

<!-- Modal MENU PERMISSÔES MENU PERMISSÔES MENU PERMISSÔES MENU PERMISSÔES MENU PERMISSÔES MENU PERMISSÔES -->
<div class="modal fade" id="permissoesModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="shareModalLabel">
					Compartilhado com outras pessoas
				</h5>
			</div>
			<div class="modal-body">

				<div class="table-scroll-permissao">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="text-left">Pessoa</th>
								<th class="text-center">Visualizar</th>
								<th class="text-center">Editar</th>
								{{-- <th class="text-center">Excluir</th> --}}
								<th class="text-center pasta_permissoes">Criar</th>
								<th class="text-center">Download</th>
								<th class="text-center pasta_permissoes">Upload</th>
							</tr>
						</thead>
						<tbody id="table_body">
						</tbody>
					</table>
				</div>

			</div>

			<div class="modal-footer justify-content-between w-100">

				<div>
					<button type="submit" class="btn btn-primary">Salvar</button>
					<button type="button" class="btn btn-outline-primary" data-dismiss="modal" aria-label="Close">Cancelar</button>
				</div>

			</div>

		</div>
	</div>
</div>
</div>
<!-- Modal MENU PERMISSÔES -->

@if (session('success'))
<script type="text/javascript">
	window.swal({
		type: 'success',
		title: 'Sucesso',
		text: '{{ session("success") }}',
	});
</script>
@endif

@endsection
