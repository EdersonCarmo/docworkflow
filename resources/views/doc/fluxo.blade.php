
@extends('layouts.soged')

@section('content')

      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script>
      $( function() {
        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();
      } );
      </script>

    <div class="container-fluid">
      @component('includes.alert')
      @endcomponent

      <!-- BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB -->
          <ol class="breadcrumb bg-primary">
            <a class="text-white" href="#">
              <i class="fa fa-exchange-alt mr-2"></i>&nbsp; FLUXOS
            </a>
            <div class="col-md-1 text-right">
              <a class="text-white" href="{{route('documento.show',$idDoc)}}" title="Voltar">
                <i class="fa fa-arrow-circle-left fa-lg" aria-hidden="true"></i>
              </a>
            </div>
          </ol>
      <!-- BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB -->
      <!-- Blank div to give the page height to preview the fixed vs. static navbar-->
      <div class="row">

      <div class="col-md-7">
        <form class="form-control" action="{{route("fluxo.store")}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="documento_id" value="{{$idDoc}}">
          <input type="hidden" name="statusDOC" value="">
          <input type="hidden" name="statusNEW" value="">

          <div class="row">
            <div class="col-md-7">
              <label for="fluxo">Nome do fluxo</label>
              <input id="fluxo" class="form-control form-control-sm" type="text" name="fluxo" value="">
            </div>
            <div class="col-md-5" id="div_sel_usu">
              <label for="sel_usu" id="id_select1">Usuários</label><br>
              <select id="sel_usu" class="custom-select" name="user_id[]" multiple>
                <option value="none">Selecione ...</option>
                @foreach ($usuarios as $key)
                  <option value="{{$key->id}}" title="{{$key->id.$key->name}}">{{$key->name}}</option>
                @endforeach
              </select>
            </div>

            <div class="col-md-6" id="div_sel_cli">
              <label for="sel_cli" id="id_select2">Clientes</label><br>
              <select id="sel_cli" class="custom-select" name="cliente_id[]" multiple>
                <option value="none">Selecione ...</option>
                @foreach ($clientes as $key)
                  <option value="{{$key->id}}">{{$key->cliente}}</option>
                @endforeach
              </select>
            </div>

          </div>

          <div class="row">
            <div class="col-md-12" id="div_mail">
              <label for="fluxo">Endereço de e-mail</label>
              <input id="myEmail" class="form-control form-control-sm" type="text" name="email" value="">
            </div>
          </div>

<div style="height: 20px;"></div>

          <div class="row">
            <div class="container">
            <div class="">
              <div class="btn-group btn-group-toggle">
              <label id="rd_enc" class="btn btn-dark">
                <input class="rbAtivoInativo" name="tipo" type="radio" value="1">&nbsp;
                <i class="far fa-hand-point-right" aria-hidden="true"></i> Encaminhar &nbsp;
              </label>
              <label id="rd_mail" class="btn btn-dark">
                <input class="rbAtivoInativo" name="tipo" type="radio" value="2">&nbsp;
                <i class="far fa-envelope" aria-hidden="true"></i> E-Mail &nbsp;
              </label>
              <label id="rd_conf" class="btn btn-dark">
                <input class="rbAtivoInativo" name="tipo" type="radio" value="3">&nbsp;
                <i class="far fa-thumbs-up" aria-hidden="true"></i> Confirmação &nbsp;
              </label>
            </div>
          </div>
        </div>
        </div>

        <div style="height: 20px;"></div>

        <div class="" id="div_btn">
          <input  class="btn btn-primary" type="submit" name="" value="Criar Fluxo">
        </div>
        </form>

        <script type="text/javascript">
        $('#div_sel_usu').hide('fast');
        $('#div_sel_cli').hide('fast');
        $('#div_mail').hide('fast');
        $('#div_btn').hide('fast');

        $(document).ready(function () {
          $('#rd_enc').click(function () {
            $('#div_sel_cli').hide('fast');
            $('#div_mail').hide('fast');
            $('#div_sel_usu').show('fast');
            $('#sel_cli').val('none');
          });
          $('#rd_mail').click(function () {
            $('#div_sel_usu').hide('fast');
            $('#div_mail').show('fast');
            $('#sel_usu').val('none');
          });
          $('#rd_conf').click(function () {
            $('#div_sel_cli').hide('fast');
            $('#div_mail').hide('fast');
            $('#div_sel_usu').show('fast');
            $('#sel_cli').val('none');
          });
          $('#myEmail').click(function () {
          $('#div_btn').show('fast');
          });

          $('#sel_usu').change(function() {
            if ($('#sel_usu').val() != 'none')
              $('#div_btn').show('fast');
             else
              $('#div_btn').hide('fast');
          });
        });
        </script>

      </div>
<!-- DIREITA -->
      <div class="col-md-4 form-control" style="display: {{count($fluxos) == 0?"none":""}}">
        <form class="" action="{{route("fluxo.update",1)}}" method="post">
          {{ csrf_field() }}{{ method_field('PUT') }}
          <input type="hidden" name="documento_id" value="{{$idDoc}}">

          <div style="cursor:move;">
            <div id="sortable">
              @foreach ($fluxos as $key)
                @php
                  if($key->tipo == 1) $icone = "far fa-hand-point-right";
                  elseif($key->tipo == 2) $icone = "far fa-envelope";
                  elseif($key->tipo == 3) $icone = "far fa-thumbs-up";
                @endphp
                <div class="input-group input-group-sm">
                  <span class="input-group-addon" id="sizing-addon3" title="Mover item">
                    ░&nbsp; {{$key->ordem - 1}}° &nbsp;
                    <i class="{{$icone}}" aria-hidden="true"></i>
                  </span>

                  <input type="text" name="" value="&nbsp;{{$key->fluxo}}" disabled>
                  <input type="hidden" class="form-control" name="teste[]" aria-describedby="sizing-addon3" value="{{$key->ordem}}">

                  <a class="btn btn-link btn-danger" href="{{route('apagaFluxo',$key->id_fluxo)}}" onclick="return confirm('Confirma a exclusão?')" title="Excluir item">
                    <i class="far fa-trash-alt text-danger" aria-hidden="true"></i>
                  </a>

                </div>
              @endforeach


            </div>
              <br>
            <input class="btn btn-primary" type="submit" name="" value="Gravar ordenação">

            </form>
          </div>
        </div>
      </div>

    </div>

  @endsection
  <!-- /.container-fluid-->
