<!DOCTYPE html>
<html>
<head>
	<title>Imprimir Documento</title>

	<style type="text/css">

	@page{
		size:21cm 29.7cm;
		margin:0.75cm 0.75cm 1cm 1.5cm; 
	}

	@media screen{
		.overlay{
			background-color: rgba(0,0,0,0.8);
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100vh;
			z-index: 99999999;
		}
		.background{
			display: none;
		}

		.main{
			padding-right: 2em;
			font-family: sans-serif;
		}
	}

	@media print {

		.overlay{
			display: none;
		}

		.spacer{
			height: 5em;
		}

		.spacer-header{
			height: 6em;
		}

		table.report-container {
			page-break-after:always;
			z-index: 999;
			min-height: 100%;
		}
		thead.report-header {
			display:table-header-group;
		}
		tfoot.report-footer {
			display:table-footer-group;
		} 

		.main{
			padding-right: 2em;
			font-family: sans-serif;
		}

		#footer{
			position: fixed;
			bottom: 0;
			
		}

		.background{
			width: 100%;
			position: fixed;
			height: 100%;
			top: 0;
			left: 0;
			z-index: -1;
		}

		.img{
			width: 100%;
			height: 100%;
		}

	}
</style>

<script src="{{URL::asset('/vendor/jquery/jquery.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		window.onafterprint = function(e){
			$(window).off('mousemove', window.onafterprint);
			$('.overlay').hide();
		};

		window.print();

		setTimeout(function(){
			$(window).one('mousemove', window.onafterprint);
		}, 1);
	});
</script>
</head>
<body>

	<div class="overlay"></div>

	<div class="background">
		<img class="img" src="https://ecos-soged.s3.amazonaws.com/empresas/4/4p34/FundoSoitic.png">
	</div>

	<table class="report-container">
		<thead class="report-header">
			<tr>
				{{-- <td>
					<div class="header">
						<p style="margin: 0px"><span style="color: rgb(44, 130, 201);"><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Grupo SOITIC - Solu&ccedil;&otilde;es Inteligentes para Tecnologia da Informa&ccedil;&atilde;o e Comunica&ccedil;&atilde;o</span></strong></span></p>

						<p style="margin: 0px"><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><span style="color: rgb(44, 130, 201);">Paran&aacute;, 497 | Funcion&aacute;rios | Po&ccedil;os de Caldas | MG | CEP: 37713-047</span></span></span></p>

						<p style="margin: 0px"><span style="color: rgb(44, 130, 201); font-family: Arial, Helvetica, sans-serif; font-size: 18px;">0800 704 1133 | 0800 707 5900 | </span><span style="color: rgb(44, 130, 201); font-family: Arial, Helvetica, sans-serif; font-size: 11px;">35</span><span style="color: rgb(44, 130, 201); font-family: Arial, Helvetica, sans-serif; font-size: 18px;"> 3729 9900 | soitic.com</span></p>
					</div>
				</td> --}}

				<td class="spacer-header"></td>
			</tr>
		</thead>
		<tfoot class="report-footer">

			<tr>
				<td class="spacer"></td>
			</tr>
		</tfoot>
		<tbody class="report-content">
			<tr>
				<td>
					<div class="main">
						{!!$documento->texto!!}
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<div id="footer">
		<p style="margin: 0px"><span style="color: rgb(44, 130, 201);"><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size: 18px;">Grupo SOITIC - Solu&ccedil;&otilde;es Inteligentes para Tecnologia da Informa&ccedil;&atilde;o e Comunica&ccedil;&atilde;o</span></strong></span></p>

		<p style="margin: 0px"><span style="font-size: 18px;"><span style="font-family: Arial,Helvetica,sans-serif;"><span style="color: rgb(44, 130, 201);">Paran&aacute;, 497 | Funcion&aacute;rios | Po&ccedil;os de Caldas | MG | CEP: 37713-047</span></span></span></p>

		<p style="margin: 0px"><span style="color: rgb(44, 130, 201); font-family: Arial, Helvetica, sans-serif; font-size: 18px;">0800 704 1133 | 0800 707 5900 | </span><span style="color: rgb(44, 130, 201); font-family: Arial, Helvetica, sans-serif; font-size: 11px;">35</span><span style="color: rgb(44, 130, 201); font-family: Arial, Helvetica, sans-serif; font-size: 18px;"> 3729 9900 | soitic.com</span></p>

	</div>

</body>
</html>