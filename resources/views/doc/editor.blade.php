
@extends('layouts.soged')
@section('content')

<div class="container-fluid">

	@component('includes.alert')
	@endcomponent

	@if (isset($documento))
	<form class="" method="POST" action="{{route("documento.update",$documento->id_documento)}}">
		{{ method_field('PUT') }}
		<input type="hidden" name="id_documento" value="{{$documento->id_documento}}">
		<input type="hidden" name="template_id" value="{{$documento->template_id}}">
		@else
		<form class="" method="POST" action="{{route("documento.store")}}">
			<input type="hidden" name="pasta_id" value="{{$pasta}}">
			@endif
			@if (isset($tem))
			<input type="hidden" name="template_id" value="1">
			@endif
			{{ csrf_field() }}

			<!-- CABEÇALHO -->
			<div class="header p-3">
				
				<div class="d-flex align-items-center">
					@if (isset($tem))
					<i class="fa fa-file fa-2x text-white"></i>&nbsp;
					<input class="transparente ml-2" type="text" name="documento" value="{{ $documento->documento or 'Novo Template' }}" title="renomear" size="40">
					<input type="hidden" name="tem" value="{{ $documento->template_id or 1 }}">
					<a href="#" id="editar-nome-template"><i class="fa fa-edit text-white ml-3"></i></a>
					@else
					<i class="fa fa-file fa-lg text-white"></i>
					<div>
						<input class="transparente ml-2" type="text" name="documento" value="{{ $documento->documento or 'Novo Documento' }}" title="renomear" size="40">
						<a href="#" id="editar-nome-template"><i class="fa fa-edit text-white ml-3"></i></a>
					</div>
					@endif
				</div>
				

				@isset($templates)
				<div>
					<select class="form-control" name="template_id" required>
						<option value="">Selecione o tipo</option>
						@foreach ($templates as $key)
						<option value="{{$key->id_template}}">{{$key->template}}</option>
						@endforeach
					</select>
				</div>
				@endisset

				@if (isset($tem))
				<div class="text-right">
					<a class="text-white" href="#" data-toggle="modal" data-target="#tagsModal" title="Visualiza Tags existentes.">
						<i class="fa fa-question-circle fa-2x"></i>
					</a>
				</div>
				@endif

					{{-- @isset($documento->id_documento)
					<div class="text-right {{ isset($tipo)?"d-none":"" }}">
						<a class="text-white" href="{{route('fluxo.show',$documento->id_documento)}}" title="Fluxos do documento.">
							<i class="fa fa-exchange-alt mr-2"></i>
						</a>
					</div>
					@endisset --}}
				</div>
				<!-- CABEÇALHO -->

				<!-- TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO -->
				<textarea id="ckeditor" name="texto">
					{{ $texto->texto or old('texto') }}
					{{ $documento->texto or '' }}
					{{ $template->texto or ''  }}
				</textarea>
				<!-- TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO -->
				<!-- BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS -->
				<div>
					<br>
				</div>
				<div class="row">

					<div class="col-lg-3 {{ isset($tipo) && $tipo == 3?"d-none":"" }}">
						@if (isset($documento))
						<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
							<button type="submit" class="btn btn-primary">Salvar</button>

							{{-- <div class="btn-group" role="group">
								<button id="btnGroupDrop1" type="button" class="btn {{ $documento->template_id > 0? "btn-success":"btn-primary"}} dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								</button>
								<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
									<a class="btn btn-primary m-2" href="{{route('salvaPdf',[$documento->id_documento, $end])}}">
										<i class="fas fa-file-pdf"></i> Salva PDF
									</a>
									<a class="btn btn-primary m-2" href="{{url('/imprimir-documento/'.$documento->id_documento)}}">
										<i class="fa fa-print"></i> Imprimir
									</a>
									<a class="btn btn-primary m-2" href="#" data-toggle="modal" data-target="#molduraModal" data-id="{{ $documento->id_documento }}">
										<i class="fa fa-print"></i> Imprimir com moldura
									</a>
								</div>
							</div> --}}

						</div>

						@else
						<button class="form-control btn {{ isset($documento->template_id) > 0? "btn-success":"btn-primary"}}" type="submit">
							Criar {{isset($tem)?"Template":"Documento"}}
						</button>
						@endif
					</div>

					@isset($documento->fluxo)
					<div class="col-lg-6" style="display: {{ count($documento->fluxo) > 0?"":"none" }}">
						<div class="">
							<button class="form-control btn btn-success" type="submit" name="status" value="{{ $documento->status + 1 }}" onclick="return confirm('Confirma a finalização do documento?')">
								{{ isset($tipo) && $tipo == 3?"CONFIRMAR":"Finalizar" }} e enviar o Documento para o {{ $documento->status + 1 }}° passo</button>
							</div>
						</div>
						@endisset

					</div>
				</form>
				<!-- BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS BUTTONS -->

			</div>

			<script>
				CKEDITOR.replace( 'ckeditor',  {
					height: '700px'
				});



			</script>

			<!-- Modal MENU TAGS TEMPLATE -->
			<div class="modal fade bs-example-modal-lg" id="tagsModal" tabindex="-1" role="dialog" aria-labelledby="tagsModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="tagsModalLabel"><i class="fa fa-file"></i> TAGS </h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div  class="modal-body">
							<div class="">

								<table class="table table-condensed">
									<tr>
										<td>@nome_a</td> <td> → </td> <td>Nome da parte A</td>
										<td>@nome_b</td> <td> → </td> <td>Nome da parte B</td>
									</tr>
									<tr>
										<td>@cnpj_a</td> <td> → </td> <td>CNPJ da parte A</td>
										<td>@cnpj_b</td> <td> → </td> <td>CNPJ da parte B</td>
									</tr>
									<tr>
										<td>@razao_a</td> <td> → </td> <td>Razão da parte A</td>
										<td>@razao_b</td> <td> → </td> <td>Razão da parte B</td>
									</tr>
									<tr>
										<td>@ie_a</td> <td> → </td> <td>Inscrição Estadual da parte A</td>
										<td>@ie_b</td> <td> → </td> <td>Inscrição Estadual da parte B</td>
									</tr>
									<tr>
										<td>@uf_a</td> <td> → </td> <td>Estado da parte A</td>
										<td>@uf_b</td> <td> → </td> <td>Estado da parte B</td>
									</tr>
									<tr>
										<td>@cidade_a</td> <td> → </td> <td>Cidade da parte A</td>
										<td>@cidade_b</td> <td> → </td> <td>Cidade da parte B</td>
									</tr>
									<tr> <td>@bairro_a</td> <td> → </td> <td>Bairro da parte A</td>
										<td>@bairro_b</td> <td> → </td> <td>Bairro da parte B</td>
									</tr>
									<tr>
										<td>@rua_a</td> <td> → </td> <td>Logradouro da parte A</td>
										<td>@rua_b</td> <td> → </td> <td>Logradouro da parte B</td>
									</tr>
									<tr>
										<td>@numero_a</td> <td> → </td> <td>Número da parte A</td>
										<td>@numero_b</td> <td> → </td> <td>Número da parte B</td>
									</tr>
									<tr>
										<td>@cep_a</td> <td> → </td> <td>CEP da parte A</td>
										<td>@cep_b</td> <td> → </td> <td>CEP da parte B</td>
									</tr>
									<tr>
										<td>@comp_a</td> <td> → </td> <td>Complemento da parte A</td>
										<td>@comp_b</td> <td> → </td> <td>Complemento da parte B</td>
									</tr>

								</table>

							</div>
						</div>

					</div>
				</div>
			</div>
			<!-- Modal MENU TAGS TEMPLATE -->
			<!-- Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA -->
			<div class="modal fade" id="molduraModal" tabindex="-1" role="dialog" aria-labelledby="molduraModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="ModalLabel">
								<i class="fa fa-file-alt mr-2" aria-hidden="true"></i> Seleção de cabeçalhos e rodapés:
							</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form class="" action="{{ route("ImprimirMoldura") }}" method="post">
								{{ csrf_field() }}
								<input id="documento" type="hidden" name="id_documento" value="">

								<select class="form-control" name="id_moldura">
									<option value="0">Selecione...</option>
									@foreach ($molduras as $key)
									<option value="{{ $key->id_moldura }}">{{ $key->moldura }}</option>
									@endforeach
								</select>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary"><i class="fa fa-print" aria-hidden="true"></i> Imprimir</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA Modal MOLDURA -->

			<script type="text/javascript">
				function troca(){ var txt = document.getElementById("ckeditor");  }


				$('#tagsModal').on('show.bs.modal', function (event) {
					var button = $(event.relatedTarget)
					var recipient = button.data('whatever')
					var modal = $(this)

					modal.find('.modal-footer input').val(recipient)
				})

				$('#molduraModal').on('show.bs.modal', function (event) {
					var button 	= $(event.relatedTarget)
					var var1 		= button.data('id')
					var modal 	= $(this)
					modal.find('#documento').val(var1)
				})
			</script>

			@if (session('success'))
			<script type="text/javascript">
				window.swal({
					type: 'success',
					title: 'Sucesso',
					text: '{{ session("success") }}',
				});
			</script>
			@endif

			@endsection
