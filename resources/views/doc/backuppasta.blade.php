@extends('layouts.soged')
@extends('includes.modaldoc')

@section('content')

@php
$ss = session('pasta');
$id = 0;
$_SESSION['pastaend'] = \Auth::user()->empresa_id.'p'.\Auth::id().'/';
$newarq;
$idarq=0;
$idpasta=0;

function down($valor){
  $ret  = 'none';
  if($valor % 2 != 0) $ret = '';
  return $ret;
}
@endphp

<div class="container-fluid">

  <div class="card mb-3">

    <div class="card-header">

      <div>
        <i class="fa fa-folder"></i>
        <strong>Meus Arquivos</strong>
      </div>

    </div>


    <div class="card-body">
      <div class="container-fluid">
        <div class="row">

          <!-- ↓ foreach PASTAS  -->
          @foreach ($pastas as $key)
          @if ($idpasta != $key->id_pasta)
          <div class="col-lg-2 text-center">
            <div class="dropdown">

              <a href="{{ route('pasta.show', $key->id_pasta) }}">
                <i class="fa fa-folder-o fa-5x"></i>
                <i class="fa {{ $key->sts == 2?"fa-users":""}}" title="Arquivo compartilhado"></i>
              </a>

              <p class="text-center">{{ $key->pasta }}</p>

              <div class="dropdown-content">
                <a href="#" data-toggle="modal" data-target="#renamePastaModal" data-whatever="{{$key->id_pasta}}">
                  <i class="fa fa-pencil"></i>
                  Renomear
                </a>
                <a href="#" data-toggle="modal" data-target="#movePastaModal" data-whatever="{{$key->id_pasta}}" data-permissao="{{$key->id_permissao}}">
                  <i class="fa fa-arrows"></i>
                  Mover
                </a>
                <a href="{{route('compartilhar_permissao',$key->id_permissao)}}" style="display: {{$key->updown>3?"":"none"}}">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                  Compartilhar
                </a>
                <a href="{{route('permissao.show',$key->id_permissao)}}" style="display: {{$key->updown>3?"":"none"}}">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                  Permissões
                </a>
                <a href="{{ route('apagaPermissao', $key->pasta_id) }}" onClick="return confirm('Deseja realmente excluir?')">
                  <i class="fa fa-trash"></i>
                  Excluir
                </a>
              </div>
            </div>
          </div>
          @php
          $idpasta = $key->id_pasta;
          @endphp
          @endif
          @endforeach
          <!-- ↑ foreach PASTAS -->
          <!-- ↓ foreach ARQUIVOS -->
          @foreach ($arquivos as $key)
          @php
          $ext = substr($key->local, -3);
          $ico = 'fa-file-o'; $rename=0;
          if($ext == 'png' or $ext == 'jpg') $ico = 1;
          if($ext == 'pdf') $ico = 'fa-file-pdf-o';
          if($ext == 'ocx') $ico = 'fa-file-word-o';
          if($ext == 'zip' or $ext == 'rar') $ico = 'fa-file-zip-o';
          if($key->crud==4 or $key->crud==5 or $key->crud==6 or $key->crud==7 or $key->crud==12 or $key->crud==13 or $key->crud==14 or $key->crud==15) $rename=1;
          @endphp

          <div class="col-lg-2 text-center" style="display: {{ $key->crud == 0 ? "none":"" }}">
            <div class="dropdown">
              <a href="#" class="dropbtn" data-toggle="modal" data-target="#imgModal" data-whatever="{{env('APP_S3').$key->local}}">
                @if ($ext == 'png' or $ext == 'jpg')
                <img src="{{env('APP_S3').$key->local}}" alt="" width="60" height="60">
                @else
                <i class="fa {{$ico}} fa-4x"></i>
                @endif
                <i class="fa {{ $key->sts == 2?"fa-users":""}}" title="Arquivo compartilhado"></i>
              </a>

              <p class="text-center">{{ $key->arquivo }}</p>

              <div class="dropdown-content">
                <a href="{{ env('APP_S3').$key->local }}" style="display: {{ $key->updown % 2 != 0 ? "":"none" }}" download>
                  <i class="fa fa-download"></i>
                  Baixar
                </a>
                <a href="#" data-toggle="modal" data-target="#fundoTemModal" data-whatever="{{ $key->local }}" title="Define imagem como fundo de template" style="display: {{ $ico == 1 ? "":"none" }}" >
                  <i class="fa fa-picture-o"></i>
                  Definir fundo
                </a>
                <a href="#" data-toggle="modal" data-target="#renameArqModal" data-whatever="{{$key->id_arquivo}}" style="display: {{ $rename == 1 ? "":"none" }}" >
                  <i class="fa fa-pencil"></i>
                  Renomear
                </a>
                <a href="#" data-toggle="modal" data-target="#moveArqModal" data-whatever="{{$key->id_permissao}}">
                  <i class="fa fa-arrows"></i>
                  Mover
                </a>
                <a href="{{route('compartilhar_permissao',$key->id_permissao)}}" style="display: {{$key->updown>3?"":"none"}}">
                  <i class="fa fa-users" aria-hidden="true"></i>
                  Compartilhar
                </a>
                <a href="{{route('permissao.show',$key->id_permissao)}}" style="display: {{$key->updown>3?"":"none"}}">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                  Permissões
                </a>
                <a href="{{route('apaga_arquivo',$key->id_arquivo)}}" onclick="return confirm('Deseja realmente excluir?')" style="display: {{ $key->crud > 7? "":"none" }}">
                  <i class="fa fa-trash"></i>
                  Excluir
                </a>
              </div>
            </div>
          </div>
          @endforeach
          <!-- ↑ foreach ARQUIVOS -->
          <!-- ↓ foreach DOCUMENTOS -->
          @foreach ($documentos as $key)
          @php
          $ico = 'fa-file-text-o'; $rename=0;
          if($key->template_id > 0) $ico = 'fa-file-text';
          if($key->crud==4 or $key->crud==5 or $key->crud==6 or $key->crud==7 or $key->crud==12 or $key->crud==13 or $key->crud==14 or $key->crud==15) $rename=1;
          @endphp

          <div class="col-lg-2 text-center" style="display: {{ $key->crud == 0 ? "none":"" }}">
            <div class="dropdown">
              <a href="{{route('testep', [$key->id_documento, encrypt($_SESSION['pastaend'])])}}" class="dropbtn">
                <i class="fa {{$ico}} fa-4x"></i>
                <i class="fa {{ $key->sts == 2?"fa-users":""}}" title="Arquivo compartilhado"></i>
              </a>

              <p class="text-center">{{ $key->documento }}</p>

              <div class="dropdown-content">
                <a href="{{route('pdf',$key->id_documento)}}" style="display: {{ $key->updown % 2 != 0 ? "":"none" }}" download>
                  <i class="fa fa-download"></i>
                  Baixar PDF
                </a>
                <a href="#" data-toggle="modal" data-target="#renameDocModal" data-whatever="{{$key->id_documento}}" style="display: {{ $rename == 1 ? "":"none" }}" >
                  <i class="fa fa-pencil"></i>
                  Renomear
                </a>
                <a href="#" data-toggle="modal" data-target="#moveArqModal" data-whatever="{{$key->id_permissao}}">
                  <i class="fa fa-arrows"></i>
                  Mover
                </a>
                <a href="{{route('compartilhar_permissao',$key->id_permissao)}}" style="display: {{$key->updown>3?"":"none"}}">
                  <i class="fa fa-users" aria-hidden="true"></i>
                  Compartilhar
                </a>
                <a href="{{route('permissao.show',$key->id_permissao)}}" style="display: {{$key->updown>3?"":"none"}}">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                  Permissões
                </a>
                <a href="{{route('apaga_arquivo',$key->id_documento)}}" onclick="return confirm('Deseja realmente excluir?')" style="display: {{ $key->crud > 7? "":"none" }}">
                  <i class="fa fa-trash"></i>
                  Excluir
                </a>
              </div>
            </div>
          </div>
          @endforeach
          <!-- ↑ foreach DOCUMENTOS -->

          <!-- ↓ BTN MAIS -->
          @isset($folder)
          <div class="col-lg-2">
            <div class="col-lg-2 text-center">
              <div class="dropdown">
                <a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-plus-circle fa-4x" aria-hidden="true"></i>
                </a>
                <div class="dropdown-content">
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#newPastaModal" data-whatever="{{$atual}}">
                    <i class="fa fa-folder-o" aria-hidden="true"></i> Nova Pasta
                  </a>
                  <a class="dropdown-item" href="{{route('createTem',$atual)}}">
                    <i class="fa fa-file-text" aria-hidden="true"></i> Novo Template
                  </a>
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#selectTemModal" data-whatever="{{$atual}}">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i> Novo Documento
                  </a>
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#uploadModal">
                    <i class="fa fa-cloud-upload" aria-hidden="true"></i> Enviar arquivo
                  </a>
                </div>
              </div>
            </div>
          </div>
          @endisset
          <!-- ↑ BTN MAIS -->

        </div>
      </div>
    </div>
    <div class="card-footer small text-muted"> Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
  </div>

</div>
@php
if(isset($folder)){
  function mostrar($folder, $id){
    foreach ($folder as $key) {
      if($key->ref_pai == $id) echo $key->pasta.'<br>';
    }
  }
}
@endphp
@isset($folder)
@foreach ($folder as $key)
<option value="{{$key->pasta}}"></option>
@endforeach
@endisset
<br><br><br>

<!-- /.container-fluid-->
<script type="text/javascript">
  $('#renameArqModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('RENOMEAR ARQUIVO ' + recipient)
    //console.log(recipient);
    modal.find('.modal-footer input').val(recipient)
  })

  $('#renamePastaModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('.modal-footer input').val(recipient)
  })

  $('#renameDocModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('.modal-footer input').val(recipient)
  })

  $('#moveArqModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    //modal.find('.modal-title').text('MOVER ARQUIVO PARA' + recipient)
    modal.find('.modal-footer input').val(recipient)
  })

  $('#movePastaModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var recipient2 = button.data('permissao') // Extract info from data-* attributes
    var modal = $(this)
    //modal.find('.modal-title').text('MOVER PARA  ' + recipient)
    modal.find('.modal-footer input').val(recipient)
    modal.find('.modal-footer1 input').val(recipient2)
  })

  $('#newPastaModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('.modal-title').text('CRIAR PASTA ' + recipient)
    modal.find('.modal-footer input').val(recipient)
  })

  $('#permissaoModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    modal.find('.modal-title').text('EDITAR PERMISSÕES ' + recipient)
    modal.find('.modal-footer input').val(recipient)
  })

  $('#fundoTemModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this)
    //odal.find('.modal-title').text('EDITAR PERMISSÕES ' + recipient)
    modal.find('.modal-footer input').val(recipient)
  })

  $('#imgModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever')
    var modal = $(this)
    $("#imgModal img").attr("src", recipient);
    $("#imgModal").modal("show");
    //odal.find('.modal-title').text('EDITAR PERMISSÕES ' + recipient)
    //modal.find('.modal-body src').val(recipient)
  })
</script>
@endsection
