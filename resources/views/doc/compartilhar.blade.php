@extends('layouts.soged')

@section('content')
  @php
  $c = $r = $u = $d = $up = $dn = $p = '';
  if(isset($permissao)) {
    $updn = $permissao->updown;
    $crud = $permissao->crud;
    if($crud > 7) $d        = 'checked';
    if($crud % 2 != 0) $r   = 'checked';
    if($crud==2 or $crud==3 or $crud==6 or $crud==7 or $crud==10 or $crud==11 or $crud==14 or $crud==15) $c='checked';
    if($crud==4 or $crud==5 or $crud==6 or $crud==7 or $crud==12 or $crud==13 or $crud==14 or $crud==15) $u='checked';
    if($updn > 3) $p        = 'checked';
    if($updn % 2 != 0) $dn  = 'checked';
    if($updn==2 or $updn==3 or $updn==6 or $updn==7 or $updn==10 or $updn==11 or $updn==14 or $updn==15) $up='checked';
  }
@endphp
  <div class="container-fluid">

    <!-- MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT -->
    <div>
      @if (session('success'))
        <script>setTimeout("document.getElementById('alerta-1').style.display = 'none'", 5000);</script>
        <p id="alerta-1" class="alert alert-success">{{ session('success') }}</p>
      @endif
      @if (session('danger'))
        <script>setTimeout("document.getElementById('alerta-1').style.display = 'none'", 5000);</script>
        <p id="alerta-1" class="alert alert-danger">{{ session('danger') }}</p>
      @endif
    </div>
    <!-- MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT -->

    <!-- BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB -->
    <ol class="breadcrumb bg-primary">
      <a class="text-white" href="{{ route('pasta.index')}}">
        <i class="fa fa-tasks fa-lg text-white"></i>&nbsp;
        @php
          if($permissao->documento_id != NULL) $titulo = " Compartilhar Acesso ao Documento";
            else
            $permissao->arquivo_id == NULL ? $titulo = " Compartilhar Acesso a Pasta": $titulo = " Compartilhar Acesso ao Arquivo"
        @endphp
        {{ $titulo }}
      </a>
    </ol>
    <!-- BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB -->

    <!-- FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO -->
    @isset($permissao)
      <ol class="breadcrumb bg-primary" style="display: {{ isset($usuarios)?"":"none"}}">
        <div class="col-lg-3">
          <label class="text-white" for="exampleFormControlSelect2">
            Usuário(s) com acesso:
          </label>
        </div>
        <div class="col-lg-4">
          <select class="form-control" id="exampleFormControlSelect2">
            @foreach ($userComp as $key)
              <option value="">
                {{$key->name}}
              </option>
            @endforeach
          </select>
        </div>
      </ol>
    @endisset
    <!-- FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO -->

    <div class="card mb-3">
      <div class="card-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 ">

                <div class="row">
                  <div class="">
                    <div class="col-lg-12">
                    </div>
                  </div>
                </div>

                <div class="row custom-control">
                  <form action="{{route("compStore_permissao")}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="arquivo_id" value="{{$permissao->arquivo_id}}">
                    <input type="hidden" name="pasta_id" value="{{$permissao->pasta_id}}">
                    <input type="hidden" name="documento_id" value="{{$permissao->documento_id}}">

                    <label class="custom-control custom-checkbox">
                      <input name="r" value="1" type="checkbox" class="custom-control-input" {{ $r }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Visualizar</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                      <input name="c" value="2" type="checkbox" class="custom-control-input" {{ $c }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Criar</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                      <input name="u" value="4" type="checkbox" class="custom-control-input" {{ $u }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Alterar</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                      <input name="d" value="8" type="checkbox" class="custom-control-input" {{ $d }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Apagar</span>
                    </label>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 custom-control">
                  <label class="custom-control custom-checkbox">
                    <input name="dn" value="1" type="checkbox" class="custom-control-input" {{ $dn }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Download</span>
                  </label>
                  <label class="custom-control custom-checkbox">
                    <input name="up" value="2" type="checkbox" class="custom-control-input" {{ $up }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">UpLoad</span>
                  </label>
                  @isset($permissao)
                    <label class="custom-control custom-checkbox" style="display: {{$permissao->sts == 1?"none":""}}">
                      <input name="p" value="{{$permissao->sts == 1?0:4}}" type="checkbox" class="custom-control-input" {{ $p }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Alterar Permissões</span>
                    </label>
                  @endisset
                </div>
              </div>
<!-- SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS -->
              <div class="row">
                  <div class="col-lg-1">
                    <label  for="exampleFormControlSelect2" title="Selecione 1 ou mais">Usuários:</label>
                  </div>
                  <div class="col-lg-4">
                    <select name="user_id[]" class="form-control" id="exampleFormControlSelect2" multiple>
                      @foreach ($usuarios as $key)
                        <option value="{{$key->id}}">
                          {{$key->name}} {{$key->id}}
                        </option>
                      @endforeach
                    </select>
                  </div>
              </div>
<!-- SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS SELECT USERS -->
<!-- SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP -->
              <div class="row">
                  <div class="col-lg-1">
                    <label  for="exampleFormControlSelect2" title="Selecione 1 ou mais">Grupos:</label>
                  </div>
                  <div class="col-lg-4">
                    <select name="grupo_id[]" class="form-control" id="exampleFormControlSelect2" multiple>
                      @foreach ($grupos as $key)
                        <option value="{{$key->id_grupo}}">
                          {{$key->grupo}}
                        </option>
                      @endforeach
                    </select>
                  </div>
              </div>
              <!-- SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP SELECT GROUP -->

              <div class="row">
                <div class="custom-control">
                  <input class="btn btn-primary" type="submit" value="Compartilhar" onclick="return confirm('Deseja realmente compartilhar?')">
                </div>
              </div>

            </form>

          </div>
        </div>
        <div class="card-footer small text-muted"> Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
      </div>
    </div>

    <!-- /.container-fluid-->
  @endsection
