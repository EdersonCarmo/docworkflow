@extends('layouts.soged')

@section('content')
<div class="container-fluid">
  <div class="card mb-3">

    <div class="card-header">

      <div>
        <i class="fa fa-exchange-alt"></i>
        <strong>Fluxos</strong>
      </div>

    </div>

    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Documentos</th>
              <th>Fluxo</th>
              <th class="text-center">Status</th>
              <th class="text-center">Ordem</th>
              <th>Data</th>
              <th class="text-center">Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($fluxos as $key)
            @php
            if($key->tipo == 1) {$icone = "fa fa-hand-point-right"; $titulo = "Encaminhamento de Documento";}
            elseif($key->tipo == 2) {$icone = "fa fa-envelope"; $titulo = "Envio de E-mail";}
            elseif($key->tipo == 3) {$icone = "fa fa-thumbs-up"; $titulo = "Envia para aprovação";}
            @endphp
            <tr>
              <td>{{ $key->documento->documento }}</td>
              <td>{{ $key->fluxo }}</td>
              <td class="text-center"><i class="{{$icone}}" aria-hidden="true" title="{{$titulo}}"></i></td>
              <td class="text-center">{{ $key->ordem }}</td>
              <td>{{ date( 'd/m/Y h:m' , strtotime($key->created_at)) }}</td>
              <td class="actions">
                <a href="{{route('reabreDoc',$key->documento_id)}}" class="reabrir-doc" title="Cancela o fluxo e reabre o documento">
                  <i class="text-success fa fa-undo" aria-hidden="true"></i>
                </a>
                <a href="{{route('apagaFluxo',$key->id)}}" title="Excluir Usuário" class="excluir-registro">
                  <i class="text-danger fa fa-trash-alt" aria-hidden="true"></i>
                </a>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
  </div>
</div>
@endsection
