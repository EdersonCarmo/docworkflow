@extends('layouts.soged')

@section('content')
  @php
  $c = $r = $u = $d = $up = $dn = $p = '';
  if(isset($permissao)) {
    $updn = $permissao->updown;
    $crud = $permissao->crud;
    if($crud > 7) $d        = 'checked';
    if($crud % 2 != 0) $r   = 'checked';
    if($crud==2 or $crud==3 or $crud==6 or $crud==7 or $crud==10 or $crud==11 or $crud==14 or $crud==15) $c='checked';
    if($crud==4 or $crud==5 or $crud==6 or $crud==7 or $crud==12 or $crud==13 or $crud==14 or $crud==15) $u='checked';
    if($updn > 3) $p        = 'checked';
    if($updn % 2 != 0) $dn  = 'checked';
    if($updn==2 or $updn==3 or $updn==6 or $updn==7 or $updn==10 or $updn==11 or $updn==14 or $updn==15) $up='checked';
  }
@endphp
  <div class="container-fluid">

    <!-- MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT -->
    <div>
      @if (session('success'))
        <script>setTimeout("document.getElementById('alerta-1').style.display = 'none'", 5000);</script>
        <p id="alerta-1" class="alert alert-success">{{ session('success') }}</p>
      @endif
      @if (session('danger'))
        <script>setTimeout("document.getElementById('alerta-1').style.display = 'none'", 5000);</script>
        <p id="alerta-1" class="alert alert-danger">{{ session('danger') }}</p>
      @endif
    </div>
    <!-- MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT MESSAGE ALERT -->

    <!-- BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB -->
    <ol class="breadcrumb bg-primary">
      <a class="text-white" href="{{ route('pasta.index')}}">
        <i class="fa fa-tasks fa-lg text-white"></i>&nbsp;
        {{ isset($permissao)?" Permissões de Acesso":" Compartilhar Acesso"}}
      </a>
    </ol>
    <!-- BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB BREADCRUMB -->

    <!-- FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO -->
    @isset($permissao)
      <ol class="breadcrumb bg-primary" style="display: {{ isset($usuarios)?"":"none"}}">
        <div class="col-lg-3">
          <label class="text-white" for="exampleFormControlSelect2">
            Usuário(s) com acesso:
          </label>
        </div>
        <div class="col-lg-4">
          <select class="form-control" id="exampleFormControlSelect2" onchange="javascript: location.href = this.value">
            @foreach ($usuarios as $key)
              <option value="{{$key->id_permissao}}"{{ $permissao['user_id'] == $key->id?"selected":""}}>
                {{$key->name}} {{$key->id_permissao}}
              </option>
            @endforeach
          </select>
        </div>
      </ol>
    @endisset
    <!-- FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO -->

    <!-- FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO -->
    @if (!isset($permissao))
      <ol class="breadcrumb bg-primary" style="display: {{ isset($usuarios)?"":"none"}}">
        <div class="col-lg-1">
          <label class="text-white" for="exampleFormControlSelect2">
            Usuários:
          </label>
        </div>
        <div class="col-lg-3">
          <select class="form-control" id="exampleFormControlSelect2" onchange="javascript: location.href = this.value">
            @foreach ($usuarios as $key)
              <option value="{{$key->id_permissao}}">
                {{$key->name}} {{$key->id_permissao}}
              </option>
            @endforeach
          </select>
        </div>
      </ol>
    @endif
    <!-- FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO FORM SELECAO DE USUARIO -->

    <div class="card mb-3">
      <div class="card-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 ">

              @if (isset($permissao))
                <div class="row">
                  <div class="">
                    <div class="col-lg-12">
                      <strong>{{ $permissao->arquivo_id == NULL? "PASTA: " : "ARQUIVO: " }}</strong>
                      {{ $permissao->arquivo_id == NULL? $permissao->pasta->pasta : $permissao->arquivo->arquivo }} <br>

                      <strong>USUÁRIO: </strong>
                      {{ $permissao->usuario->name }}<br><br>
                    </div>
                  </div>
                </div>

                <div class="row custom-control">
                  <form action="{{route("permissao.update",$permissao->id_permissao)}}" method="post">
                    {{ csrf_field() }} {{ method_field('PUT') }}
                  @else
                    <form action="{{route("permissao.store")}}" method="post">
                      {{ csrf_field() }}
                    @endif

                    <label class="custom-control custom-checkbox">
                      <input name="r" value="1" type="checkbox" class="custom-control-input" {{ $r }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Visualizar</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                      <input name="c" value="2" type="checkbox" class="custom-control-input" {{ $c }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Criar</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                      <input name="u" value="4" type="checkbox" class="custom-control-input" {{ $u }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Alterar</span>
                    </label>
                    <label class="custom-control custom-checkbox">
                      <input name="d" value="8" type="checkbox" class="custom-control-input" {{ $d }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Apagar</span>
                    </label>

                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 custom-control">
                  <label class="custom-control custom-checkbox">
                    <input name="dn" value="1" type="checkbox" class="custom-control-input" {{ $dn }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Download</span>
                  </label>
                  <label class="custom-control custom-checkbox">
                    <input name="up" value="2" type="checkbox" class="custom-control-input" {{ $up }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">UpLoad</span>
                  </label>
                  @isset($permissao)
                    <label class="custom-control custom-checkbox" style="display: {{$permissao->sts == 1?"none":""}}">
                      <input name="p" value="4" type="checkbox" class="custom-control-input" {{ $p }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Alterar Permissões</span>
                    </label>
                  @endisset
                </div>
              </div>

              <div class="row">
                <div class="custom-control">
                  <input type="hidden" name="id_permissao" value="{{$permissao->id_permissao}}">
                  <input class="btn btn-primary" type="submit" value="{{ isset($permissao) ? "Alterar":" Criar "}}" onclick="return confirm('Deseja realmente alterar?')">
                </div>
              </div>

            </form>

          </div>
        </div>
        <div class="card-footer small text-muted"> Consultado por {{\Auth::user()->name}} em {{date('d/m/Y H:i')}}</div>
      </div>
    </div>

    <!-- /.container-fluid-->
  @endsection
