## DocWorkflow

Plataforma de gestão de arquivos baseado no google drive.
Possui um sistema de controle de fluxos, assim os documentos criados seguem a rotina determinada.
A plataforma é multi empresas, com ADM principal e ADM para as empresas.

---

## Projeto

Neste projeto utilizamos os servidores da AWS e servidor compartilhado para testes.
A plataforma também é um editor de documentos com inserções automaticas de dados.
