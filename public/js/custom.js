$(document).ready(function(){

	$('.loader-overlay').hide();

// MODAL MODAL MODAL MODAL MODAL MODAL MODAL MODAL MODAL MODAL
$('.permissao_button').click(function(e){
	var permissao = e.currentTarget.attributes['permi'].value;

	var tipo = e.currentTarget.attributes['tipo'].value;

	if(tipo == "pasta"){
		$('.pasta_permissoes').show();
	}
	else{
		$('.pasta_permissoes').hide();
	}

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	var binario = [];

	$.ajax({
		url:'/permissao_binario',
		type:'GET',
		success: function(result){
			binario = result;
		}
	});

	$.ajax({
		url:'/permissao/'+permissao,
		type:'GET',
		success: function(result){

			$('#table_body').find('tr').remove().end();

			$.each(result, function(key, value){
				var crud = result[key].crud ;

				var verdadeiro = {cor:'success',aria:'true',ico:'fa-check'};
				var falso = {cor:'danger',aria:'false',ico:'fa-times'};

				for (var i = binario.length - 1; i >= 0; i--) {
					if(binario[i].id_permissoes_binario == crud){

						var ler = ((binario[i].ler == 1) ? verdadeiro : falso);
						var alterar = ((binario[i].alterar == 1) ? verdadeiro : falso);
						var criar = ((binario[i].criar == 1) ? verdadeiro : falso);
						var apagar = ((binario[i].apagar == 1) ? verdadeiro : falso);
						var download = ((binario[i].download == 1) ? verdadeiro : falso);
						var upload = ((binario[i].upload == 1) ? verdadeiro : falso);

						if(tipo == "pasta"){
							$('#table_body').append(
								'<tr>'+
								'<td class="text-left">'+result[key].name+'</td>'+
								'<td class="text-center permissao_toggle text-'+ler.cor+'" aria-hidden="'+ler.aria+'">'+
								'<i class="fa '+ler.ico+'"></i>'+
								'</td>'+
								'<td class="text-center permissao_toggle text-'+alterar.cor+'" aria-hidden="'+alterar.aria+'">'+
								'<i class="fa '+alterar.ico+'"></i>'+
								'</td>'+
								'<td class="text-center permissao_toggle text-'+criar.cor+'" aria-hidden="'+criar.aria+'">'+
								'<i class="fa '+criar.ico+'"></i>'+
								'</td>'+
								'<td class="text-center permissao_toggle text-'+download.cor+'" aria-hidden="'+download.aria+'">'+
								'<i class="fa '+download.ico+'"></i>'+
								'</td>'+
								'<td class="text-center permissao_toggle text-'+upload.cor+'" aria-hidden="'+upload.aria+'">'+
								'<i class="fa '+upload.ico+'"></i>'+
								'</td>'+
								'</tr>');
						}
						else{
							$('#table_body').append(
								'<tr>'+
								'<td class="text-left">'+result[key].name+'</td>'+
								'<td class="text-center permissao_toggle text-'+ler.cor+'" aria-hidden="'+ler.aria+'">'+
								'<i class="fa '+ler.ico+'"></i>'+
								'</td>'+
								'<td class="text-center permissao_toggle text-'+alterar.cor+'" aria-hidden="'+alterar.aria+'">'+
								'<i class="fa '+alterar.ico+'"></i>'+
								'</td>'+
								'<td class="text-center permissao_toggle text-'+download.cor+'" aria-hidden="'+download.aria+'">'+
								'<i class="fa '+download.ico+'"></i>'+
								'</td>'+
								'</tr>');
						}
						
					}
				}
			});
		}
	});
});

// MODAL MODAL MODAL MODAL MODAL MODAL MODAL MODAL MODAL MODAL


$( ".draggable" ).draggable({ revert: "invalid", cursor: "move" });
$( ".droppable" ).droppable({
	drop: function( event, ui ) {

		var pasta_id_ref = $(this).attr('data');
		var objeto = ui.draggable.attr('data');

		var tipo = ui.draggable.attr('tipo');

		var id_arquivo = 0;
		var id_documento = 0;
		var id_pasta = 0;



		if(tipo == 'documento'){
			id_documento = objeto;
			$('#div_documento_'+objeto).parent().hide();
		}
		else if(tipo == 'arquivo'){
			id_arquivo = objeto;
			$('#div_arquivo_'+objeto).parent().hide();
		}
		else{
			id_pasta = objeto;
			$('#div_pasta_'+objeto).parent().hide();
		}

		$('.loader-overlay').css('display','flex');

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			url:'/permissao/moverpasta',
			type:'PUT',
			data: { id_arquivo:id_arquivo, pasta_id_ref:pasta_id_ref, id_documento:id_documento, id_pasta:id_pasta, acao:'MOVER'},
			success: function(result){
				$('.loader-overlay').hide();
			}
		});

	}
});

$('.loader-overlay').hide();

$('#notificacoes-click').click(function(e){
	e.preventDefault();

	$('#notificacoes').toggle(400);

	$('.overlay-notificacoes').toggle();
});

$('.overlay-notificacoes').click(function(){
	$('#notificacoes').hide();
	$('.overlay-notificacoes').hide();
});

$('#sair').click(function(e){
	e.preventDefault();
	window.swal({
		title: 'Deseja sair?',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Sim',
		cancelButtonText: 'Não'
	}).then((result) => {
		if (result.value) {
			$('#logout-form').submit();
		}
	});
});

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*			Excluir registro 				*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

$('.excluir-registro').click(function(e){

	e.preventDefault();

	window.swal({
		title: 'Deseja deletar o registro?',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Sim',
		cancelButtonText: 'Não'
	}).then((result) => {
		if (result.value) {
			$('.loader-overlay').css('display','flex');
			window.location = e.currentTarget.href;
		}
	});
});

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*				Reabrir doc 				*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

$('.reabrir-doc').click(function(e){

	e.preventDefault();

	window.swal({
		title: 'Atenção',
		text: 'Deseja realmente cancelar o fluxo e reabrir o documento?',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Sim',
		cancelButtonText: 'Não'
	}).then((result) => {
		if (result.value) {
			$('.loader-overlay').css('display','flex');
			window.location = e.currentTarget.href;
		}
	});
});


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*					PF / PJ 				*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

$('#rd_PF').click(function () {
	$('#PJ_razao').hide('fast');
	$('#PJ_ie').hide('fast');
	$('#PJ_cnpj').hide('fast');
	$('#PF_cpf').show('fast');
});
$('#rd_PJ').click(function () {
	$('#PJ_razao').show('fast');
	$('#PJ_ie').show('fast');
	$('#PJ_cnpj').show('fast');
	$('#PF_cpf').hide('fast');
});


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*			Máscaras para input				*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
var maskBehavior = function (val) {
	return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
options = {onKeyPress: function(val, e, field, options) {
	field.mask(maskBehavior.apply({}, arguments), options);
}


};

$('.celular').mask(maskBehavior, options);
$('.telefone').mask('(00) 0000-0000');
$('.cpf').mask('000.000.000-00', {reverse: true});
$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
$('.cep').mask('00000-000');

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*	Remover máscaras antes de salvar o form	*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
$('.salvar-cadastro').click(function(e){

	e.preventDefault();

	var counter = 0;

	$('.required').each(function() {

		if($(this).val() == '' && $(this).parent().css('display') == 'block'){
			$(this).css('box-shadow','0 0 8px 0 red');
			counter++;
		}
		else{
			$(this).css('box-shadow','none');
		}

	});

	if(counter !=0){
		window.swal({
			type: 'error',
			title: 'Ops...',
			text: 'Preencha os campos em destaque!',
		});
	}
	else{
		$('.loader-overlay').css('display','flex');

		$('.celular').unmask();
		$('.telefone').unmask();
		$('.cpf').unmask();
		$('.cnpj').unmask();
		$('.cep').unmask();
		$('.form').submit();
	}
});


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*				Validar CEP					*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

/*Quando o campo cep perde o foco.*/
$("#cep").blur(function() {

	/*Nova variável "cep" somente com dígitos.*/
	var cep = $(this).val().replace(/\D/g, '');

	/*Verifica se campo cep possui valor informado.*/
	if (cep != "") {

		/*Expressão regular para validar o CEP.*/
		var validacep = /^[0-9]{8}$/;

		/*Valida o formato do CEP.*/
		if(validacep.test(cep)) {

			/*Preenche os campos com "..." enquanto consulta webservice.*/
			$("#logradouro").val("...");
			$("#bairro").val("...");
			$("#cidade").val("...");
			$("#uf").val("...");

			/*Consulta o webservice viacep.com.br/*/
			$.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

				if (!("erro" in dados)) {
					/*Atualiza os campos com os valores da consulta.*/
					$("#logradouro").val(dados.logradouro);
					$("#bairro").val(dados.bairro);
					$("#cidade").val(dados.localidade);
					$("#uf").val(dados.uf);
				}
				else {
					/*CEP pesquisado não foi encontrado.*/
					limpa_formulário_cep();
				}
			});
		}
		else {
			/*cep é inválido.*/
			limpa_formulário_cep();
		}
	}
	else {
		/*cep sem valor, limpa formulário.*/
		limpa_formulário_cep();
	}
});

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*			Clique com o botão direito		*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
$('.workspace-folder').mousedown(function(event) {

	if(window.location.pathname == '/pasta' || window.location.pathname.substring(0,6)){

		switch (event.which) {
			case 1:
			break;
			case 3:
			$('.menu-novo').hide();
			$('.menu-propriedades').hide();
			$('.menu-novo').fadeIn(400);
			if(event.clientX > 1240){
				$('.menu-novo').css('left',event.clientX-470);
			}
			else{
				$('.menu-novo').css('left',event.clientX-262);
			}

			if(event.clientY > $(document).height()-200){
				$('.menu-novo').css('top',event.clientY-245);
			}
			else{
				$('.menu-novo').css('top',event.clientY-70);
			}
			break;
			default:
			break;
		}
		return true;

	}

});

$(".workspace-folder").on("contextmenu",function(e){
	return false;
});

$('body').click(function(e){

	$('.menu-novo').hide();
	$('.menu-propriedades').hide();
	$('.file').removeClass('clicked-file');
	$('.renomear_input').hide();
	$('.nome_arquivo').show();

});

$('.btn-novo').click(function(event){

	event.stopPropagation();

	if(event.clientX > 1240){
		$('.menu-novo').css('left',event.clientX-470);
	}
	else{
		$('.menu-novo').css('left',event.clientX-262);
	}

	if(event.clientY > $(document).height()-400){
		$('.menu-novo').css('top',event.clientY-245);
	}
	else{
		$('.menu-novo').css('top',event.clientY-70);
	}

	$('.menu-novo').fadeIn(400);
});

$('.propriedades').on('mousedown', function(event){

	event.stopPropagation();

	var data_item = event.currentTarget.attributes['data'].value;
	var tipo = event.currentTarget.attributes['tipo'].value;
	var permi = event.currentTarget.attributes['permi'].value;
	var link = event.currentTarget.attributes['link'].value;
	var crud = event.currentTarget.attributes['crud'].value;
	var binario = [];

	$.ajax({
		url:'/permissao_binario',
		type:'GET',
		success: function(result){
			binario = result;

			for (var i = binario.length - 1; i >= 0; i--) {
				if(binario[i].id_permissoes_binario == crud){

					if(tipo == "pasta"){


					}
					else{

						((binario[i].ler == 1) ? '' : '');
						((binario[i].alterar == 1) ? $('.editar_button, .renomear_button').show() : $('.editar_button, .renomear_button').hide());
						((binario[i].criar == 1) ? '' : '');
						((binario[i].apagar == 1) ? $('.excluir_button').show() : $('.excluir_button').hide());
						((binario[i].download == 1) ? $('.download_button').show() : $('.download_button').hide());
						((binario[i].upload == 1) ? '' : '');
					}

				}
			}
		}
	});


	

	if(event.clientX > 1240){
		$('.menu-propriedades').css('left',event.clientX-470);
	}
	else{
		$('.menu-propriedades').css('left',event.clientX-262);
	}

	if(event.clientY > $(document).height()-400){
		$('.menu-propriedades').css('top',event.clientY-245);
	}
	else{
		$('.menu-propriedades').css('top',event.clientY-70);
	}

	if(tipo != 'documento'){
		$('.menu-propriedades').find('.editar_button').hide();
	}
	else{
		var encrypt = event.currentTarget.attributes['encrypt'].value;
		$('.menu-propriedades').find('.editar_button').show();
		var link_documento = '/documento/show2/'+data_item+'/'+encrypt;
		$('.menu-propriedades').find('.editar_button').attr('href',link_documento);
	}

	if(tipo == 'arquivo'){
		var link_download = event.currentTarget.attributes['data-whatever'].value;
		$('.menu-propriedades').find('.download_button').show();
		$('.menu-propriedades').find('.download_button').attr('href',link_download);
	}
	else if(tipo == 'documento'){
		$('.menu-propriedades').find('.download_button').show();
		$('.menu-propriedades').find('.download_button').attr('href','/documento/download/'+data_item);
	}
	else{
		$('.menu-propriedades').find('.download_button').hide();
	}

	$('.menu-propriedades').find('.renomear_button').attr('data-target',data_item);

	$('.menu-propriedades').find('.excluir_button').attr('data-target',data_item);
	$('.menu-propriedades').find('.modal_button').attr('data-target',data_item);
	$('.menu-propriedades').find('.renomear_button').attr('tipo',tipo);
	$('.menu-propriedades').find('.excluir_button').attr('tipo',tipo);
	$('.menu-propriedades').find('.modal_button').attr('tipo',tipo);
	$('.menu-propriedades').find('.modal_button').attr('permi',permi);
	$('.menu-propriedades').find('.modal_button').attr('link',link);

	if(window.location.pathname == '/pasta' || window.location.pathname.substring(0,6)){

		switch (event.which) {
			case 3:
			$('.menu-propriedades').hide();
			$('.menu-novo').hide();
			$('.menu-propriedades').fadeIn(400);
			$('.file').removeClass('clicked-file');
			$(this).children(1).addClass('clicked-file');
			break;
			default:
			$('.menu-propriedades').hide();
			$(this).children(1).removeClass('clicked-file');
			break;
		}
		return false;

	}

});

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*			Abrir modal documentoModal		*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
$('#documentoModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var nomearquivo = button.data('nome');
	var conteudo = button.data('conteudo');
	var modal = $(this);
	$('#id_documento_modal').val(button[0].attributes['data'].value);
	modal.find('.modal-title').text(nomearquivo);
	modal.find('.modal-body').html(conteudo);
})

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*					Clique no input	 		*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
$('input').click(function(e){
	e.stopPropagation();
});

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*					Função renomear	 		*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
var extensao = '';
$('.renomear_button').click(function(e){

	e.stopPropagation();

	$('.menu-propriedades').hide();

	var input = $(this).attr('data-target');

	var tipo = $(this).attr('tipo');

	if(tipo == 'arquivo'){

		$('#input_arquivo_'+input).attr('tipo',tipo);

		$('#input_arquivo_'+input).show();

		var texto = $('#input_arquivo_'+input).val();

		var arr = texto.split(".");

		extensao = $(arr).get(-1);

		var texto_formatado = arr.slice();

		texto_formatado.pop();

		texto_formatado = texto_formatado.join().replace(',','.');

		$('#input_arquivo_'+input).focus().val("").val(texto_formatado);

		$('#text_arquivo_'+input).hide();

	}

	else if(tipo == 'documento'){

		$('#input_documento_'+input).attr('tipo',tipo);

		$('#input_documento_'+input).show();

		var texto = $('#input_documento_'+input).val();

		$('#input_documento_'+input).focus().val("").val(texto);

		$('#text_documento_'+input).hide();

	}

	else{
		$('#input_pasta_'+input).attr('tipo',tipo);

		$('#input_pasta_'+input).show();

		var texto = $('#input_pasta_'+input).val();

		$('#input_pasta_'+input).focus().val("").val(texto);

		$('#text_pasta_'+input).hide();
	}

});

$('.renomear_input').blur(function(){

	$('.loader-overlay').css('display','flex');

	if($(this).attr('tipo') == 'arquivo'){
		var indice = $(this).attr('indice');
		var novo_nome = $('#input_arquivo_'+indice).val();

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			url:'/arquivo/update',
			type:'PUT',
			data: { id_arquivo: indice, arquivo: novo_nome+'.'+extensao},
			success: function(result){
				$('#text_arquivo_'+indice).text(novo_nome+'.'+extensao);

				$('.loader-overlay').hide();
			}
		});
	}

	else if($(this).attr('tipo') == 'documento'){
		var indice = $(this).attr('indice');
		var novo_nome = $('#input_documento_'+indice).val();

		$('#text_documento_'+indice).show();

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			url:'/documento/update',
			type:'PUT',
			data: { id_documento: indice, documento: novo_nome, renomear_documento:1},
			success: function(result){
				$('#text_documento_'+indice).text(novo_nome);
				$('.loader-overlay').hide();
			}
		});
	}

	else{
		var indice = $(this).attr('indice');
		var novo_nome = $('#input_pasta_'+indice).val();

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			url:'/pasta/update',
			type:'PUT',
			data: { id_pasta: indice, pasta: novo_nome},
			success: function(result){
				$('#text_pasta_'+indice).text(novo_nome);

				$('.loader-overlay').hide();
			}
		});
	}


});

$('.renomear_input').on('keypress', function (e) {

	if(e.which === 13){

		$('.renomear_input').hide();
		$('.nome_arquivo').show();

		$('.loader-overlay').css('display','flex');

		if($(this).attr('tipo') == 'arquivo'){
			var indice = $(this).attr('indice');
			var novo_nome = $('#input_arquivo_'+indice).val();

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url:'/arquivo/update',
				type:'PUT',
				data: { id_arquivo: indice, arquivo: novo_nome+'.'+extensao},
				success: function(result){
					$('#text_arquivo_'+indice).text(novo_nome+'.'+extensao);

					$('.loader-overlay').hide();
				}
			});
		}

		else if($(this).attr('tipo') == 'documento'){
			var indice = $(this).attr('indice');
			var novo_nome = $('#input_documento_'+indice).val();

			$('#text_documento_'+indice).show();

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url:'/documento/update',
				type:'PUT',
				data: { id_documento: indice, documento: novo_nome, renomear_documento:1},
				success: function(result){
					$('#text_documento_'+indice).text(novo_nome);
					$('.loader-overlay').hide();
				}
			});
		}

		else{
			var indice = $(this).attr('indice');
			var novo_nome = $('#input_pasta_'+indice).val();

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url:'/pasta/update',
				type:'PUT',
				data: { id_pasta: indice, pasta: novo_nome},
				success: function(result){
					$('#text_pasta_'+indice).text(novo_nome);

					$('.loader-overlay').hide();
				}
			});
		}

	}
});


/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*				Função nova pasta	 		*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
$('.nova_pasta_button').click(function(e){

	$('.loader-overlay').css('display','flex');

	e.stopPropagation();

	$('.menu-novo').hide();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url:'/pasta',
		type:'POST',
		data: { pasta: 'Nova Pasta', ref_pai: $(this).attr('data-whatever')},
		success: function(result){

			location.reload();
		}
	});

});

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/*					Função excluir	 		*/
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
$('.excluir_button').click(function(e){

	e.stopPropagation();

	$('.menu-propriedades').hide();

	var id = $(this).attr('data-target');
	var tipo = $(this).attr('tipo');

	if(tipo == 'arquivo'){
		window.swal({
			title: 'Deseja deletar o arquivo?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}).then((result) => {
			if (result.value) {
				$('.loader-overlay').css('display','flex');

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url:'/arquivo/apaga/'+id,
					type:'GET',
					success: function(result){

						location.reload();
					}
				});
			}
		});
	}

	else if(tipo == 'documento'){
		window.swal({
			title: 'Deseja deletar o documento?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}).then((result) => {
			if (result.value) {
				$('.loader-overlay').css('display','flex');

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url:'/documento/apaga/'+id,
					type:'GET',
					success: function(result){

						location.reload();
					}
				});
			}
		});
	}

	else{
		window.swal({
			title: 'Deseja deletar a pasta?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Sim',
			cancelButtonText: 'Não'
		}).then((result) => {
			if (result.value) {
				$('.loader-overlay').css('display','flex');

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url:'/permissao/apagaPermissao/'+id,
					type:'GET',
					success: function(result){

						location.reload();
					}
				});
			}
		});
	}

});



$('#exampleSelect2').select2({ width: '100%' });
$('#exampleSelect3').select2({ width: '100%' });

$('#gerar-link-publico').click(function(e){
	e.preventDefault;

	var link = e.currentTarget.attributes['link'].value;
	$('#link-publico-gerado').text(link);
	$('#link-publico').show();
});

$('#avancado-compartilhar').click(function(e){
	e.preventDefault;
	$('#avancado-compartilhar-check').show();
	$(this).hide();
});

$('.modal_button').click(function(e){
	var tipo = $(this).attr('tipo');
	var target = $(this).attr('target');
	var id = $(this).attr('data-target');
	var link = $(this).attr('link');

	$('#compartilhar_permissoes').find('option').remove().end();
	$('#permissoes_link_permissoes').find('option').remove().end();

	if(tipo == "documento"){

		$('.compartilhar_permissoes_arquivo').show();
		$('.compartilhar_permissoes_pasta').hide();

		$('#compartilhar_permissoes').append(
			'<option value="1" selected="true">Pessoas podem visualizar</option>'+
			'<option value="5">Pessoas podem editar</option>'+
			'<option value="17">Pessoas podem baixar</option>'+
			'<option value="21">Pessoas podem baixar e editar</option>');

		$('#permissoes_link_permissoes').append(
			'<option value="1" selected="true">Pessoas podem visualizar</option>'+
			'<option value="5">Pessoas podem editar</option>'+
			'<option value="17">Pessoas podem baixar</option>'+
			'<option value="21">Pessoas podem baixar e editar</option>');

		$('#gerar-link-publico').hide();

		$('#compartilhar_parametros').append('<input id="id_documento" type="hidden" name="documento_id" value="'+id+'">');

		$('#leitura_pasta').removeAttr('checked');
	}
	else if(tipo == "arquivo"){

		$('.compartilhar_permissoes_arquivo').show();
		$('.compartilhar_permissoes_pasta').hide();

		$('#compartilhar_permissoes').append(
			'<option value="17" selected="true">Pessoas podem visualizar</option>');

		$('#permissoes_link_permissoes').append(
			'<option value="17" selected="true">Pessoas podem visualizar</option>');

		$('#gerar-link-publico').show();

		$('#compartilhar_parametros').append('<input id="id_arquivo" type="hidden" name="arquivo_id" value="'+id+'">');

		$('#leitura_pasta').removeAttr('checked');
	}
	else{

		$('.compartilhar_permissoes_arquivo').hide();
		$('.compartilhar_permissoes_pasta').show();

		$('#gerar-link-publico').hide();

		$('#compartilhar_parametros').append('<input id="id_pasta" type="hidden" name="pasta_id" value="'+id+'">');

		$('#leitura_pasta').attr('checked','true');

	}

	$('#link-publico-gerado').text('');
	$('#link-publico').hide();
	$('#gerar-link-publico').attr('link','https://ecos-soged.s3.amazonaws.com/'+link);

	$(target).modal();
});

$('#desativar-link-publico').click(function(e){

	e.stopPropagation();

	$(this).hide();

	$('#ativar-link-publico').show();

	$('#link-publico-permissao').hide();

});

$('#ativar-link-publico').click(function(e){
	e.stopPropagation();

	$(this).hide();

	$('#desativar-link-publico').show();

	$('#link-publico-permissao').css('display','flex');
});

$('.permissao_toggle').click(function(){

	if($(this).attr('aria-hidden') == 'true'){
		$(this).children().removeClass('fa-check').addClass('fa-minus');
		$(this).removeClass('text-success').addClass('text-danger');
		$(this).attr('aria-hidden','false')
	}
	else{
		$(this).children().removeClass('fa-minus').addClass('fa-check');
		$(this).removeClass('text-danger').addClass('text-success');
		$(this).attr('aria-hidden','true')
	}

});

$('#editar-nome-template').click(function(){

	$('input[name=documento]').focus();
	$('input[name=documento]').val('');
});

$('.loader-button').click(function(){
	$('.loader-overlay').css('display','flex');
});


});

function escolherTemplate(){

	if($('#template_escolhido').val() != '0'){
		$('#btn_template').removeAttr('disabled');
	}
	else{
		$('#btn_template').attr('disabled','true');
	}

}

function limpa_formulário_cep() {
	$("#rua").val("");
	$("#bairro").val("");
	$("#cidade").val("");
	$("#uf").val("");
}



// -------------------------------------------------------------------------
